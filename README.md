# R100 Slim V1

Mask generation for R100 Slim V1.

## Setup

Create a virtual environment:
```bash
python -m venv venv
```

activate environment:
```bash
.\venv\Scripts\activate
```

Install requirements:
```bash
pip install -r requirements.txt
```

## How to use:

### Mini FBS Mask:
Running the mini_fbs_mask.py will generate it's mask file in the folder "mask_files". To add this mask as one of the 2x2
masks in the submodule design, you must move this file into the "sub_device_masks" folder and ensure that the cells have
been flattened (done in klayout) as well as making sure the layers line up with those in the submodule mask.

The sub devices are added into the submodule in lines 1475 - 1566 in "modular_layout_builder_v1.py".

### Mini FBS Mask:
Running the "modular_layout_builder_v1.py" will generate the full submodule and test device mask in "mask_files" folder.
It will also generate data files for the frequency scheduling.

### Testing components:
Most components can be tested by finding the desired script in the "examples" folder and running it. They correspond to
the models library found in "models"
