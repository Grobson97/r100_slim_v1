import gdspy
from models.cpw_coupler_stub import CPWCouplerStub


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_coupler_cpw_stub = CPWCouplerStub(
        cpw_centre_width=16,
        cpw_gap=10,
        coupler_length=132.0,
        readout_connection_x=0.0,
        readout_connection_y=0.0,
    )

    test_cpw_coupler_stub_cell = test_coupler_cpw_stub.make_cell(
        cpw_center_layer=0,
        ground_layer=1,
        cell_name="CPW Stub",
    )

    main_cell.add(test_cpw_coupler_stub_cell)

    # print(test_lekid_coupler.get_readout_connection_x())
    # print(test_lekid_coupler.get_readout_connection_y())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
