import gdspy
from models.cpw_stub import CPWStub


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_cpw_stub = CPWStub(
        cpw_centre_width=16,
        cpw_gap=10,
        stub_length=60,
        stub_width=6,
        readout_connection_x=0.0,
        readout_connection_y=0.0,
    )

    test_cpw_stub_cell = test_cpw_stub.make_cell(
        cpw_center_layer=0,
        ground_layer=1,
        cell_name="CPW Stub",
    )

    main_cell.add(test_cpw_stub_cell)

    # print(test_lekid_coupler.get_readout_connection_x())
    # print(test_lekid_coupler.get_readout_connection_y())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
