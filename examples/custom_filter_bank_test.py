import gdspy
import numpy as np
from models.custom_filter_bank import CustomFilterBank

library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
# Create Lekid cell to add module cells to
main_cell = library.new_cell("Main")

resonator_length_array = np.linspace(200, 350, 6)
input_capacitor_array = np.linspace(2, 20, 6)
output_capacitor_array = np.linspace(20, 2, 6)


filter_bank = CustomFilterBank(
    resonator_length_array=resonator_length_array,
    input_capacitor_array=input_capacitor_array,
    output_capacitor_array=output_capacitor_array,
    feedline_width=2.0,
    filter_separation=0.375,
    input_connection_x=0,
    input_connection_y=0,
)

filter_bank_cell = filter_bank.make_cell(microstrip_layer=1, ground_layer=0)
main_cell.add(filter_bank_cell)

print(filter_bank.get_lekid_connections())

gdspy.LayoutViewer()
