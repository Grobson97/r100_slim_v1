import gdspy
from models.dual_pol_simple_omt import DualPolSimpleOMT


def main():
    library = gdspy.GdsLibrary("OMT Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    omt_instance = DualPolSimpleOMT(
        diameter=1610,
        choke_width=1195,
        outer_path_radius=1610 + 800,
        centre_x=0.0,
        centre_y=0.0,
    )

    omt_cell = omt_instance.make_cell(
        microstrip_layer=0,
        ground_layer=1,
        dielectric_layer=3,
        back_etch_layer=4,
    )
    main_cell.add(omt_cell)

    smiley_omt_cell = omt_instance.make_smiley_cell(
        microstrip_layer=0,
        ground_layer=1,
        dielectric_layer=3,
        back_etch_layer=4,
    )
    main_cell.add(gdspy.CellReference(smiley_omt_cell, origin=(5000, 0)))

    print(omt_instance.get_connections())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
