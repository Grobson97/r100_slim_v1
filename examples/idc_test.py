import gdspy
from models.idc import IDC


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_idc = IDC(
        finger_width=3.0,
        finger_gap=6.0,
        horizontal_finger_length=143.0,
        vertical_finger_length=1000.0,
        inductor_connection_x=100,
        inductor_connection_y=50,
    )

    test_idc_cell = test_idc.make_cell(
        idc_layer=1,
        ground_layer=2,
        dielectric_layer=3,
        nitride_step_down_layer=4,
        oxide_step_down_layer=5,
        e_beam_boundary_layer=6,
        cell_name="IDC",
    )

    main_cell.add(test_idc_cell)

    print(test_idc.get_coupler_connection_x())
    print(test_idc.get_coupler_connection_y())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
