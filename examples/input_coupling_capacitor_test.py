import gdspy
from models.input_coupling_capacitor import InputCouplingCapacitor


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_input_coupler = InputCouplingCapacitor(
        cap_in=12, feedline_connection_x=0, feedline_connection_y=0
    )

    test_input_coupler_cell = test_input_coupler.make_cell(
        microstrip_layer=1, ground_layer=0
    )
    main_cell.add(test_input_coupler_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
