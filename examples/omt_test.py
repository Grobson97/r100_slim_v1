import gdspy
from models.omt import OMT


def main():
    library = gdspy.GdsLibrary("OMT Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    omt_instance = OMT(
        diameter=1610,
        choke_width=1195,
        dual_polarisation=True,
        centre_x=0.0,
        centre_y=0.0,
    )

    omt_cell = omt_instance.make_cell(
        antenna_layer=1, ground_layer=0, dielectric_layer=2, back_etch_layer=3
    )
    main_cell.add(omt_cell)

    omt_cell = omt_instance.make_smiley_cell(
        antenna_layer=1,
        ground_layer=0,
        dielectric_layer=2,
        back_etch_layer=3,
        cell_name="Smiley",
    )
    main_cell.add(gdspy.CellReference(omt_cell, origin=(4500, 0)))

    print(omt_instance.get_feedline_connections())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
