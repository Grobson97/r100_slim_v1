import gdspy
from models.output_coupling_capacitor import OutputCouplingCapacitor


def main():
    library = gdspy.GdsLibrary("Resonator Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_cap_out = OutputCouplingCapacitor(cap_out=12)

    test_cap_out_cell = test_cap_out.make_cell(microstrip_layer=1, ground_layer=0)
    main_cell.add(test_cap_out_cell)

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
