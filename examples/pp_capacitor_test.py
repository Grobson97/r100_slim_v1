import gdspy
from models.pp_capacitor import PPCapacitor


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    pp_capacitor = PPCapacitor(
        capacitor_length=1500.0,
        inductor_connection_x=0.0,
        inductor_connection_y=0.0,
    )

    pp_capacitor_cell = pp_capacitor.make_cell(
        plate_layer=1,
        ground_layer=2,
        dielectric_layer=3,
        cell_name="PP Capacitor",
    )

    main_cell.add(pp_capacitor_cell)

    # print(test_idc.get_coupler_connection_x())
    # print(test_idc.get_coupler_connection_y())

    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
