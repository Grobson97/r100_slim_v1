import gdspy
from models.pp_lekid_coupler import PPLekidCoupler


def main():
    library = gdspy.GdsLibrary("Test Library", unit=1e-06, precision=1e-09)
    # Create Lekid cell to add module cells to
    main_cell = library.new_cell("Main")

    test_pp_lekid_coupler = PPLekidCoupler(
        plate_width=50.0,
        plate_height=30.0,
        plate_gap=5.0,
        ground_hole_width=150.0,
        coupler_track_width=8.0,
        length_to_capacitor=100.0,
        length_to_readout=40.0,
        lekid_capacitor_connection_x=0.0,
        lekid_capacitor_connection_y=0.0,
    )

    test_pp_lekid_coupler_cell = test_pp_lekid_coupler.make_cell(
        plate_layer=1,
        ground_layer=2,
        cell_name="PP Lekid Coupler",
    )

    main_cell.add(test_pp_lekid_coupler_cell)

    # print(test_lekid_coupler.get_readout_connection_x())
    # print(test_lekid_coupler.get_readout_connection_y())
    gdspy.LayoutViewer()


if __name__ == "__main__":
    main()
