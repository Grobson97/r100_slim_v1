import numpy as np
import gdspy

import util.spectrometer_builders
import util.filter_bank_builder_tools

from models.filter_bank import FilterBank

library = gdspy.GdsLibrary("OMT Library", unit=1e-06, precision=1e-09)
# Create Lekid cell to add module cells to
main_cell = library.new_cell("Main")

layers = {
    "e_beam_boundary": 0,
    "inductors": 1,
    "idc": 2,
    "readout_centre": 3,
    "dielectric": 4,
    "ground": 5,
    "wafer": 6,
    "dice": 7,
    "antennas": 8,
    "mm-feedline": 9,
    "back_etch": 10,
    "reduced_e-beam_boundary": 11,
    "nitride_step_down": 12,
    "oxide_step_down": 13,
    "capacitor_plates": 14,
    "aluminium": 15,
    "focal plane": 16,
    "pixel": 17,
    "dowel": 18,
    "pogo_pins": 19,
    "tabs": 20,
    "alignment": 21,
    "titanium_aluminium": 22,
    "fab_1_nitride_membrane_etch": 101,
    "fab_2_oxide_membrane_etch": 102,
    "fab_3_nb_wiring": 103,
    "fab_4_al_meanders": 104,
    "fab_5_microstrip_dielectric": 105,
    "fab_6_nb_ground": 106,
    "fab_7_drie_etch": 107,
    "fab_8_titanium_aluminium": 108,
}

# Define target frequencies for filter bank filters:
# NB: Final filter is not included in the mask.
# Define readout frequency band:
frequency_min = 120
frequency_max = 180
oversampling = 1.6
resolution = 100
filter_spacing = 0.25

feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0

number_of_channels = round(
    oversampling * resolution * np.log(frequency_max / frequency_min)
)
print(f"Number of channels = {number_of_channels}")
target_f0_array = util.filter_bank_builder_tools.create_target_f0_array(
    frequency_min, frequency_max, number_of_channels
)
# Create instance of filter bank using filter_bank module:
filter_bank = FilterBank(
    target_f0_array=target_f0_array,
    feedline_width=feedline_width,
    filter_separation=filter_spacing,
    input_connection_x=0,
    input_connection_y=0,
)

########################################################################################################################

# Section to make Trio spectrometer cells:

# Section to make 1st spectrometer Cell:

duo_lekid_f0_array = np.linspace(2.010e9, 2.790e9, 140)
print(f"Duo F0 spacing: {(duo_lekid_f0_array[1]-duo_lekid_f0_array[0]) * 1e-6} MHz")

spectrometer_f0_arrays = util.spectrometer_builders.split_and_group_f0_array(
    duo_lekid_f0_array, number_of_groups=7, number_of_spectrometers=2
)

duo_f0_array_1 = spectrometer_f0_arrays[0]
duo_f0_array_2 = spectrometer_f0_arrays[1]

# Mix arrays:
duo_f0_array_1 = util.spectrometer_builders.mix_spectrometer_f0_array(
    duo_f0_array_1, [0, 3, 6, 2, 5, 1, 4]
)
duo_f0_array_2 = util.spectrometer_builders.mix_spectrometer_f0_array(
    duo_f0_array_2, [0, 3, 6, 2, 5, 1, 4]
)

# Clip f0s at end of arrays to match the number of channels in FBS:
duo_f0_array_1 = duo_f0_array_1[:number_of_channels]
duo_f0_array_2 = duo_f0_array_2[:number_of_channels]

duo_angle = (
    360 / 18
)  # Rotation of each spectrometer relative to the line of symmetry between the two.
# Make radial spectrometer cell:
(
    duo_spectrometer_group_1_cell,
    duo_spectrometer_origin_a,
    duo_spectrometer_origin_b,
    duo_readout_input,
    duo_readout_output,
) = util.spectrometer_builders.make_radial_duo_spectrometer_cell(
    filter_bank_radius=27000,
    filter_bank_rotation_angle=duo_angle,
    lekid_f0_array_a=duo_f0_array_1,
    lekid_f0_array_b=duo_f0_array_2,
    full_band_lekid_f0s=[1.895e9, 1.9e9],
    dark_f0s=[1.795e9, 1.8e9, 1.805e9],
    filter_bank=filter_bank,
    cell_name="Spectrometer Group Cell 2",
    readout_width=readout_width,
    readout_gap=readout_gap,
    bridge_width=18,
    layers=layers,
    step_down=True,
)

duo_group_rotation = 0
main_cell.add(
    gdspy.CellReference(duo_spectrometer_group_1_cell, rotation=duo_group_rotation)
)

library.add(main_cell)
library.write_gds("..\\mask_files\\duo_spectrometer_test.gds")
