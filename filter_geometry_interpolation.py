import numpy as np

def get_resonator_length(target_f0, filter_resolution):
    """
    Returns the required resonator length for a given filter resolution (R100, R200, R300, R400, R500) based off of
    linear fit interpolation values from sonnet sims.
    :param target_f0: Target f0 in GHz.
    :param filter_resolution: Resolution of the desired filter.
    :return:
    """
    if filter_resolution == 100:
        m = -1.0441
        c = 13.675
    if filter_resolution == 200:
        m = -1.0378
        c = 13.672
    if filter_resolution == 300:
        m = -1.0344
        c = 13.669
    if filter_resolution == 400:
        m = -1.0313
        c = 13.664
    if filter_resolution == 500:
        m = -1.0289
        c = 13.661

    return (10 ** -(c / m)) * ((target_f0 * 1e9) ** (1 / m))

def get_input_capacitor(resonator_length, filter_resolution):
    """
    Returns the required input capacitor width (um) for a given filter resolution (R100, R200, R300, R400, R500) based off of
    linear fit interpolation values from sonnet sims.

    :param resonator_length: length of the corresponding resonator.
    :param filter_resolution: Resolution of the desired filter.
    :return:
    """
    if filter_resolution == 100:
        m = 0.1100
        c = -7.04
    if filter_resolution == 200:
        m = 0.0721
        c = -4.15
    if filter_resolution == 300:
        m = 0.0564
        c = -3.02
    if filter_resolution == 400:
        m = 0.0473
        c = -2.39
    if filter_resolution == 500:
        m = 0.0413
        c = -2.00

    return (m * resonator_length) + c

def get_output_capacitor(resonator_length, filter_resolution):
    """
    Returns the required output capacitor width (um) for a given filter resolution (R100, R200, R300, R400, R500) based off of
    linear fit interpolation values from sonnet sims.

    :param resonator_length: length of the corresponding resonator.
    :param filter_resolution: Resolution of the desired filter.
    :return:
    """
    if filter_resolution == 100:
        m = 0.0848
        c = -5.73
    if filter_resolution == 200:
        m = 0.0595
        c = -4.76
    if filter_resolution == 300:
        m = 0.0478
        c = -4.11
    if filter_resolution == 400:
        m = 0.0374
        c = -2.88
    if filter_resolution == 500:
        m = 0.0302
        c = -1.98

    return (m * resonator_length) + c

f0_array = np.array([130.0, 140.0, 150.0, 160.0, 170.0])
resolution_array = np.array([100, 200, 300, 400, 500])
resonator_length_array = np.empty_like(f0_array)
input_capacitor_array = np.empty_like(f0_array)
output_capacitor_array = np.empty_like(f0_array)

for count, f0 in enumerate(f0_array):
    l = get_resonator_length(f0, resolution_array[count])
    c_in = get_input_capacitor(l, resolution_array[count])
    c_out = get_output_capacitor(l, resolution_array[count])

    resonator_length_array[count] = l
    input_capacitor_array[count] = c_in
    output_capacitor_array[count] = c_out

print(resolution_array)
print(f0_array)
print(resonator_length_array)
print(input_capacitor_array)
print(output_capacitor_array)

print((resonator_length_array - 34.4) / 2)
