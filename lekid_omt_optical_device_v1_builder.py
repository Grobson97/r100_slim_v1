import gdspy
import numpy as np
import matplotlib.pyplot as plt
from math import radians
from math import degrees
import submodules.dice_lines
import util.filter_bank_builder_tools as tools
import util.spectrometer_builders
import util.general_gds_tools as general_gds_tools
from models.alignment_cross import AlignmentCross
from models.dual_pol_simple_omt import DualPolSimpleOMT
from models.edge_bond_pad import EdgeBondPad
from util.general_gds_tools import translate_coordinates
from models.filter_bank import FilterBank
from models.dual_pol_omt import DualPolOMT


########################################################################################################################
from models.lekid import Lekid
from models.pp_lekid import PPLekid

resolution = 200
filter_spacing = 0.375
number_of_pixels = 12
show_plots = False
chip_unique_id = f"idc_ppc_omt_simple_optical_v1"
fab_facility = "CDF"
library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = gdspy.Cell("Main")

# Create cell to add all devices to:
all_devices_cell = gdspy.Cell("All Devices")

# Define layer dictionary:
layers = {
    "e_beam_boundary": 0,
    "inductors": 1,
    "idc": 2,
    "readout_centre": 3,
    "dielectric": 4,
    "ground": 5,
    "wafer": 6,
    "dice": 7,
    "antennas": 8,
    "mm-feedline": 9,
    "back_etch": 10,
    "reduced_e-beam_boundary": 11,
    "nitride_step_down": 12,
    "oxide_step_down": 13,
    "capacitor_plates": 14,
    "aluminium": 15,
    "focal plane": 16,
    "pixel": 17,
    "dowel": 18,
    "pogo_pins": 19,
    "tabs": 20,
    "alignment": 21,
    "fab_1_nitride_membrane_etch": 101,
    "fab_2_oxide_membrane_etch": 102,
    "fab_3_nb_wiring": 103,
    "fab_4_al_meanders": 104,
    "fab_5_microstrip_dielectric": 105,
    "fab_6_nb_ground": 106,
    "fab_7_drie_etch": 107,
}

# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0
readout_bridge_separation = 500.0
optimal_bridge_thickness = 6
boundary_width = 300
device_width = 27000

ppc_f0_array = np.array([2.01, 2.02, 2.03, 2.04, 2.25, 2.26, 2.27, 2.28]) * 1e9
idc_f0_array = np.array([2.13, 2.14, 2.15, 2.16, 2.37, 2.38, 2.39, 2.40]) * 1e9

########################################################################################################################

# Section to add device etch boundaries.

device_boundary_points = [
    (-device_width / 2 - boundary_width, -device_width / 2 - boundary_width / 2),
    (device_width / 2 + boundary_width / 2, -device_width / 2 - boundary_width / 2),
    (device_width / 2 + boundary_width / 2, device_width / 2 + boundary_width / 2),
    (-device_width / 2 - boundary_width, device_width / 2 + boundary_width / 2),
]

device_boundary_path = gdspy.FlexPath(
    points=device_boundary_points, width=boundary_width, layer=layers["tabs"]
)

tabs_cell = gdspy.Cell("Test Device Tabs")
tab_cell = gdspy.Cell("Test Device Tab")
single_tab = gdspy.Polygon(
    points=[
        (0.0, -boundary_width / 2),
        (4920.0, -boundary_width / 2),
        (4920.0, boundary_width / 2),
        (0.0, boundary_width / 2),
        (0.0, -boundary_width / 2),
    ],
    layer=layers["tabs"],
)
single_tab.fillet(150)
tab_cell.add(single_tab)

tabs_cell.add(gdspy.CellArray(tab_cell, columns=5, rows=1, spacing=(5670, 0.0)))

main_cell.add(
    gdspy.CellReference(
        tabs_cell,
        origin=(
            -device_width / 2 - boundary_width / 2,
            -device_width / 2 - boundary_width,
        ),
        rotation=90,
    )
)
main_cell.add(device_boundary_path)

########################################################################################################################

# Section to add dowel hole and slot.

dowel_hole_centre = (0.0, 0.0)
dowel_hole = gdspy.Round(center=dowel_hole_centre, radius=407, layer=layers["dowel"])
dowel_ground_hole = gdspy.Round(
    center=dowel_hole_centre, radius=407, layer=layers["ground"]
)
dowel_dielectric_hole = gdspy.Round(
    center=dowel_hole_centre, radius=407, layer=layers["dielectric"]
)
main_cell.add([dowel_hole, dowel_ground_hole, dowel_dielectric_hole])

dowel_slot_centre = (9093.5, 0.0)
dowel_slot_cell = gdspy.Cell("Dowel Slot Test Device")
dowel_slot_points = [
    (0.0, -407.0),
    (1813.0, -407.0),
    (1813.0, 407.0),
    (0.0, 407.0),
    (0.0, -407.0),
]
dowel_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["dowel"])
dowel_ground_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["ground"])
dowel_dielectric_slot = gdspy.Polygon(
    points=dowel_slot_points, layer=layers["dielectric"]
)
dowel_slot.fillet(407)
dowel_ground_slot.fillet(407)
dowel_dielectric_slot.fillet(407)
dowel_slot_cell.add([dowel_slot, dowel_ground_slot, dowel_dielectric_slot])

main_cell.add(
    gdspy.CellReference(dowel_slot_cell, origin=dowel_slot_centre, rotation=0)
)

########################################################################################################################

# Section to add OMT's

omt_instance = DualPolSimpleOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    centre_x=0.0,
    centre_y=0.0,
)

omt_cell = omt_instance.make_cell(
    microstrip_layer=layers["mm-feedline"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
)

omt_1_origin = (-6000, 6000)
omt_2_origin = (6000, 6000)
omt_3_origin = (-6000, -6000)
omt_4_origin = (6000, -6000)

main_cell.add(
    [
        gdspy.CellReference(omt_cell, origin=omt_1_origin, rotation=-45),
        gdspy.CellReference(omt_cell, origin=omt_2_origin, rotation=-45),
        gdspy.CellReference(omt_cell, origin=omt_3_origin, rotation=-45),
        gdspy.CellReference(omt_cell, origin=omt_4_origin, rotation=-45),
    ]
)

omt_connections = omt_instance.get_connections()

########################################################################################################################

# Section to add PPC LEKIDs

x_distance_from_omt_center = 3700
y_distance_from_omt_center = 1000

ppc_lekid_input_positions = [
    (
        omt_1_origin[0] - x_distance_from_omt_center,
        omt_1_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_2_origin[0] - x_distance_from_omt_center,
        omt_2_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_2_origin[0] + x_distance_from_omt_center,
        omt_2_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_1_origin[0] + x_distance_from_omt_center,
        omt_1_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_3_origin[0] - x_distance_from_omt_center,
        omt_3_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_4_origin[0] - x_distance_from_omt_center,
        omt_4_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_4_origin[0] + x_distance_from_omt_center,
        omt_4_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_3_origin[0] + x_distance_from_omt_center,
        omt_3_origin[1] - y_distance_from_omt_center,
    ),
]
ppc_lekid_rotations = [0, 0, 180, 180, 0, 0, 180, 180]

ppc_lekid_readout_connections = []
ppc_name_list = []

for count, f0 in enumerate(ppc_f0_array):
    print(f"PPC{count}: {f0:.4E}")
    ppc_name_list.append(f"PPC{count}")

    ppc_lekid = PPLekid(
        target_f0=f0,
        signal_input_x=0.0,
        signal_input_y=0.0,
    )

    relative_ppc_lekid_readout_connection = ppc_lekid.get_readout_connections(
        readout_width=readout_width
    )

    # Rotate relative input connections about zero if count is odd
    relative_ppc_lekid_readout_connection = [
        tools.rotate_coordinates(
            relative_ppc_lekid_readout_connection[0][0],
            relative_ppc_lekid_readout_connection[0][1],
            angle=radians(ppc_lekid_rotations[count]),
        ),
        tools.rotate_coordinates(
            relative_ppc_lekid_readout_connection[1][0],
            relative_ppc_lekid_readout_connection[1][1],
            angle=radians(ppc_lekid_rotations[count]),
        ),
    ]

    ppc_lekid_readout_connections.append(
        [
            translate_coordinates(
                relative_ppc_lekid_readout_connection[0],
                ppc_lekid_input_positions[count],
            ),
            translate_coordinates(
                relative_ppc_lekid_readout_connection[1],
                ppc_lekid_input_positions[count],
            ),
        ]
    )

    ppc_lekid_cell = ppc_lekid.make_cell(
        inductor_layer=layers["inductors"],
        device_plate_layer=layers["capacitor_plates"],
        readout_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        readout_width=readout_width,
        readout_gap=readout_gap,
        cell_name=f"Lekid #PPC{count}",
    )

    main_cell.add(
        gdspy.CellReference(
            ppc_lekid_cell,
            origin=ppc_lekid_input_positions[count],
            rotation=ppc_lekid_rotations[count],
        )
    )

########################################################################################################################

# Section to add IDC LEKIDs

idc_lekid_input_positions = [
    (
        omt_1_origin[0] + x_distance_from_omt_center,
        omt_1_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_2_origin[0] + x_distance_from_omt_center,
        omt_2_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_2_origin[0] - x_distance_from_omt_center,
        omt_2_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_1_origin[0] - x_distance_from_omt_center,
        omt_1_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_3_origin[0] + x_distance_from_omt_center,
        omt_3_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_4_origin[0] + x_distance_from_omt_center,
        omt_4_origin[1] + y_distance_from_omt_center,
    ),
    (
        omt_4_origin[0] - x_distance_from_omt_center,
        omt_4_origin[1] - y_distance_from_omt_center,
    ),
    (
        omt_3_origin[0] - x_distance_from_omt_center,
        omt_3_origin[1] - y_distance_from_omt_center,
    ),
]
idc_lekid_rotations = [0, 0, 180, 180, 0, 0, 180, 180]

idc_lekid_readout_connections = []
idc_name_list = []

for count, f0 in enumerate(idc_f0_array):
    print(f"IDC{count}: {f0:.4E}")
    idc_name_list.append(f"IDC{count}")
    idc_lekid = Lekid(
        target_f0=f0,
        step_down=True,
        signal_input_x=0.0,
        signal_input_y=0.0,
    )

    relative_idc_lekid_readout_connection = idc_lekid.get_readout_connections(
        readout_width=readout_width
    )

    # Rotate relative input connections about zero if needed
    relative_idc_lekid_readout_connection = [
        tools.rotate_coordinates(
            relative_idc_lekid_readout_connection[0][0],
            relative_idc_lekid_readout_connection[0][1],
            angle=radians(idc_lekid_rotations[count]),
        ),
        tools.rotate_coordinates(
            relative_idc_lekid_readout_connection[1][0],
            relative_idc_lekid_readout_connection[1][1],
            angle=radians(idc_lekid_rotations[count]),
        ),
    ]

    idc_lekid_readout_connections.append(
        [
            translate_coordinates(
                relative_idc_lekid_readout_connection[0],
                idc_lekid_input_positions[count],
            ),
            translate_coordinates(
                relative_idc_lekid_readout_connection[1],
                idc_lekid_input_positions[count],
            ),
        ]
    )

    idc_lekid_cell = idc_lekid.make_cell(
        inductor_layer=layers["inductors"],
        idc_layer=layers["capacitor_plates"],
        readout_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        dielectric_layer=layers["dielectric"],
        nitride_step_down_layer=layers["nitride_step_down"],
        oxide_step_down_layer=layers["oxide_step_down"],
        e_beam_boundary_layer=layers["e_beam_boundary"],
        readout_width=readout_width,
        readout_gap=readout_gap,
        include_step_down=True,
        cell_name=f"Lekid #IDC{count}",
    )

    main_cell.add(
        gdspy.CellReference(
            idc_lekid_cell,
            origin=idc_lekid_input_positions[count],
            rotation=idc_lekid_rotations[count],
        )
    )
########################################################################################################################

# Section to add bond pads

bond_pad_top_origin = (0.0, device_width / 2 - 20)
bond_pad_bottom_origin = (0.0, -device_width / 2 + 20)


bond_pad = EdgeBondPad(
    pad_width=400,
    pad_gap=250,
    cpw_width=readout_width,
    cpw_gap=readout_gap,
    pad_length=600,
    transition_length=700,
    origin_x=0.0,
    origin_y=0.0,
)

bond_pad_cell = bond_pad.make_cell(
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    cell_name="Test Device Bond Pad",
)

main_cell.add(
    [
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_top_origin, rotation=0.0),
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_bottom_origin, rotation=180),
    ]
)

########################################################################################################################

# Create input readout line:

bond_pad_connection = (
    bond_pad.get_feedline_connection_x(),
    bond_pad.get_feedline_connection_y(),
)
bond_pad_top_connection = (
    bond_pad_top_origin[0] + bond_pad_connection[0],
    bond_pad_top_origin[1] + bond_pad_connection[1],
)

input_readout_points = [
    bond_pad_top_connection,
    (bond_pad_top_connection[0], bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] - 10300, bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] - 10800, bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] - 10800, ppc_lekid_readout_connections[0][0][1]),
    (ppc_lekid_readout_connections[0][0][0], ppc_lekid_readout_connections[0][0][1]),
]

input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=input_readout_points[:3],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=0.0,
    final_bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout input transition",
)

input_readout = general_gds_tools.build_cpw_with_bridges(
    path_points=input_readout_points[2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout input",
)

main_cell.add(input_transition)
main_cell.add(input_readout)

########################################################################################################################

# Section to connect LEKIDs

# First Row
idc_section_connections = idc_lekid_readout_connections[:3]
ppc_section_connections = ppc_lekid_readout_connections[:3]
for count in range(3):
    start_connection = ppc_section_connections[count][1]
    end_connection = idc_section_connections[count][0]
    if count == 1:
        start_connection = idc_section_connections[count - 1][1]
        end_connection = ppc_section_connections[count][0]
    if count == 2:
        start_connection = ppc_section_connections[count - 1][1]
        end_connection = idc_section_connections[count - 1][0]

    section_path_points = [
        start_connection,
        (start_connection[0] + 800, start_connection[1]),
        (end_connection[0] - 800, end_connection[1]),
        end_connection,
    ]
    section_1_connection = general_gds_tools.build_cpw_with_bridges(
        path_points=section_path_points,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=optimal_bridge_thickness,
        bridge_separation=readout_bridge_separation,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=f"Section connection 1 {count}",
    )
    main_cell.add(section_1_connection)

# Second row:
idc_section_connections = idc_lekid_readout_connections[2:5]
ppc_section_connections = ppc_lekid_readout_connections[2:5]
for count in range(3):
    start_connection = ppc_section_connections[count][1]
    end_connection = idc_section_connections[count][0]
    if count == 1:
        start_connection = idc_section_connections[count - 1][1]
        end_connection = ppc_section_connections[count][0]
    if count == 2:
        start_connection = ppc_section_connections[count - 1][1]
        end_connection = idc_section_connections[count - 1][0]

    section_path_points = [
        start_connection,
        (start_connection[0] - 800, start_connection[1]),
        (end_connection[0] + 800, end_connection[1]),
        end_connection,
    ]
    section_2_connection = general_gds_tools.build_cpw_with_bridges(
        path_points=section_path_points,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=optimal_bridge_thickness,
        bridge_separation=readout_bridge_separation,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=f"Section connection 2 {count}",
    )
    main_cell.add(section_2_connection)

# Third row:
idc_section_connections = idc_lekid_readout_connections[4:7]
ppc_section_connections = ppc_lekid_readout_connections[4:7]
for count in range(3):
    start_connection = ppc_section_connections[count][1]
    end_connection = idc_section_connections[count][0]
    if count == 1:
        start_connection = idc_section_connections[count - 1][1]
        end_connection = ppc_section_connections[count][0]
    if count == 2:
        start_connection = ppc_section_connections[count - 1][1]
        end_connection = idc_section_connections[count - 1][0]

    section_path_points = [
        start_connection,
        (start_connection[0] + 800, start_connection[1]),
        (end_connection[0] - 800, end_connection[1]),
        end_connection,
    ]
    section_3_connection = general_gds_tools.build_cpw_with_bridges(
        path_points=section_path_points,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=optimal_bridge_thickness,
        bridge_separation=readout_bridge_separation,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=f"Section connection 3 {count}",
    )
    main_cell.add(section_3_connection)

# Fourth row:
idc_section_connections = idc_lekid_readout_connections[6:]
ppc_section_connections = ppc_lekid_readout_connections[6:]
for count in range(3):
    if count == 0:
        start_connection = ppc_section_connections[count][1]
        end_connection = idc_section_connections[count][0]
    if count == 1:
        start_connection = idc_section_connections[count - 1][1]
        end_connection = ppc_section_connections[count][0]
    if count == 2:
        start_connection = ppc_section_connections[count - 1][1]
        end_connection = idc_section_connections[count - 1][0]

    section_path_points = [
        start_connection,
        (start_connection[0] - 800, start_connection[1]),
        (end_connection[0] + 800, end_connection[1]),
        end_connection,
    ]
    section_4_connection = general_gds_tools.build_cpw_with_bridges(
        path_points=section_path_points,
        cpw_centre_width=readout_width,
        cpw_gap=readout_gap,
        bend_radius=200,
        bridge_thickness=optimal_bridge_thickness,
        bridge_separation=readout_bridge_separation,
        cpw_centre_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        cell_name=f"Section connection 4 {count}",
    )
    main_cell.add(section_4_connection)

########################################################################################################################

# Connect rows together

row_connection_1_points = [
    idc_lekid_readout_connections[1][1],
    (10800, idc_lekid_readout_connections[1][1][1]),
    (10800, ppc_lekid_readout_connections[2][0][1]),
    ppc_lekid_readout_connections[2][0],
]
row_connection_1 = general_gds_tools.build_cpw_with_bridges(
    path_points=row_connection_1_points,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name=f"Row connection 1",
)
main_cell.add(row_connection_1)

row_connection_2_points = [
    idc_lekid_readout_connections[3][1],
    (-10800, idc_lekid_readout_connections[3][1][1]),
    (-10800, ppc_lekid_readout_connections[4][0][1]),
    ppc_lekid_readout_connections[4][0],
]
row_connection_2 = general_gds_tools.build_cpw_with_bridges(
    path_points=row_connection_2_points,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name=f"Row connection 2",
)
main_cell.add(row_connection_2)

row_connection_3_points = [
    idc_lekid_readout_connections[5][1],
    (10800, idc_lekid_readout_connections[5][1][1]),
    (10800, ppc_lekid_readout_connections[6][0][1]),
    ppc_lekid_readout_connections[6][0],
]
row_connection_3 = general_gds_tools.build_cpw_with_bridges(
    path_points=row_connection_3_points,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name=f"Row connection 3",
)
main_cell.add(row_connection_3)


########################################################################################################################

# Create output readout line:

rotated_bond_pad_connection_x, rotated_bond_pad_connection_y = tools.rotate_coordinates(
    x=bond_pad_connection[0], y=bond_pad_connection[1], angle=radians(180)
)

bond_pad_bottom_connection = (
    bond_pad_bottom_origin[0] + rotated_bond_pad_connection_x,
    bond_pad_bottom_origin[1] + rotated_bond_pad_connection_y,
)

output_readout_points = [
    bond_pad_bottom_connection,
    (bond_pad_bottom_connection[0], bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] - 10300, bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] - 10800, bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] - 10800, idc_lekid_readout_connections[-1][1][1]),
    (idc_lekid_readout_connections[-1][1][0], idc_lekid_readout_connections[-1][1][1]),
]

output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=output_readout_points[:3],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=0.0,
    final_bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout output transition",
)

output_readout = general_gds_tools.build_cpw_with_bridges(
    path_points=output_readout_points[2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout output",
)

main_cell.add(output_transition)
main_cell.add(output_readout)

########################################################################################################################

# Connect OMT outputs to LEKIDs

mm_connection_path_cell = gdspy.Cell("mm_connection_path")
full_mm_connection_path_cell = gdspy.Cell("mm connections")

rotated_omt_connections = []

for omt_connection in omt_connections:
    omt_rotated_x, omt_rotated_y = tools.rotate_coordinates(
        omt_connection[0], omt_connection[1], angle=radians(-45)
    )
    rotated_omt_connections.append((omt_rotated_x, omt_rotated_y))

mm_connection_path_points = [
    (0.0, 0.0),
    (200 * np.cos(radians(45)), 200 * np.sin(radians(45))),
    (200 * np.cos(radians(45)) + 200, 200 * np.sin(radians(45))),
    (
        x_distance_from_omt_center - rotated_omt_connections[0][0],
        y_distance_from_omt_center - omt_connections[0][1] - 200,
    ),
    (
        x_distance_from_omt_center - rotated_omt_connections[0][0],
        y_distance_from_omt_center - rotated_omt_connections[0][1] - 200,
    ),
    (
        x_distance_from_omt_center - rotated_omt_connections[0][0],
        y_distance_from_omt_center - rotated_omt_connections[0][1],
    ),
]

mm_connection_path = gdspy.FlexPath(
    points=mm_connection_path_points,
    width=feedline_width,
    bend_radius=200,
    corners="circular bend",
    layer=layers["mm-feedline"],
)

mm_connection_path_cell.add(mm_connection_path)

full_mm_connection_path_cell.add(
    [
        gdspy.CellReference(
            mm_connection_path_cell, origin=rotated_omt_connections[0], rotation=0
        ),
        gdspy.CellReference(
            mm_connection_path_cell,
            origin=rotated_omt_connections[1],
            rotation=0,
            x_reflection=True,
        ),
        gdspy.CellReference(
            mm_connection_path_cell, origin=rotated_omt_connections[2], rotation=180
        ),
        gdspy.CellReference(
            mm_connection_path_cell,
            origin=rotated_omt_connections[3],
            rotation=180,
            x_reflection=True,
        ),
    ]
)
main_cell.add(
    [
        gdspy.CellReference(
            full_mm_connection_path_cell, origin=omt_1_origin, rotation=0
        ),
        gdspy.CellReference(
            full_mm_connection_path_cell, origin=omt_2_origin, rotation=0
        ),
        gdspy.CellReference(
            full_mm_connection_path_cell, origin=omt_3_origin, rotation=0
        ),
        gdspy.CellReference(
            full_mm_connection_path_cell, origin=omt_4_origin, rotation=0
        ),
    ]
)

########################################################################################################################

# Add ID text and logo:

device_id_text = gdspy.Text(
    text=chip_unique_id,
    size=400,
    position=(2000, -12000),
    angle=0,
    layer=layers["ground"],
)

# Cardiff
cardiff_logo_cell = gdspy.Cell("Test Device Cardiff Logo")
cardiff_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\cardiff_logo.gds", layer=0
)
cardiff_logo_polygon_set = gdspy.PolygonSet(
    cardiff_logo_polygons, layer=layers["ground"]
)
cardiff_logo_cell.add(cardiff_logo_polygon_set)

main_cell.add(device_id_text)
main_cell.add(gdspy.CellReference(cardiff_logo_cell, origin=(3500, 11500)))

########################################################################################################################

resonator_index_array = [0, 4, 1, 5, 2, 6, 3, 7, 8, 12, 9, 13, 10, 14, 11, 15]

resonator_index_array[::2]
plt.figure(figsize=(8, 6))
plt.plot(
    resonator_index_array[::2],
    ppc_f0_array * 1e-9,
    marker="s",
    linestyle="none",
    label="PPC",
)
plt.plot(
    resonator_index_array[1::2],
    idc_f0_array * 1e-9,
    marker="o",
    linestyle="none",
    label="IDC",
)
for count, ppc_f0 in enumerate(ppc_f0_array * 1e-9):
    plt.text(
        x=resonator_index_array[::2][count] - 0.4,
        y=ppc_f0 + 0.006,
        s=ppc_name_list[count],
    )
for count, idc_f0 in enumerate(idc_f0_array * 1e-9):
    plt.text(
        x=resonator_index_array[1::2][count] - 0.4,
        y=idc_f0 + 0.006,
        s=idc_name_list[count],
    )
plt.legend()
plt.xlabel("Resonator Index")
plt.ylabel("F0 (GHz)")
plt.show()

# main_cell.flatten()
library.add(main_cell)
library.write_gds("mask_files\\" + chip_unique_id + "_" + fab_facility + "_mask.gds")
