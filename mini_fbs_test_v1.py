import gdspy
import numpy as np
import matplotlib.pyplot as plt
from math import radians
from math import degrees
import submodules.dice_lines
import util.filter_bank_builder_tools as tools
import util.spectrometer_builders
import util.general_gds_tools as general_gds_tools
from models.alignment_cross import AlignmentCross
from models.custom_filter_bank import CustomFilterBank
from models.dual_pol_simple_omt import DualPolSimpleOMT
from models.edge_bond_pad import EdgeBondPad
from models.hybrid_180 import Hybrid180
from util.general_gds_tools import translate_coordinates
from models.filter_bank import FilterBank
from models.dual_pol_omt import DualPolOMT


########################################################################################################################
from models.lekid import Lekid
from models.pp_lekid import PPLekid

resolution = 200
filter_spacing = 0.25
number_of_pixels = 12
show_plots = False
chip_unique_id = f"mini_fbs_test_v1"
fab_facility = "ANL"
library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = gdspy.Cell("Main")

# Create cell to add all devices to:
all_devices_cell = gdspy.Cell("All Devices")

# Define layer dictionary:
layers = {
    "e_beam_boundary": 0,
    "inductors": 1,
    "idc": 2,
    "readout_centre": 3,
    "dielectric": 4,
    "ground": 5,
    "wafer": 6,
    "dice": 7,
    "antennas": 8,
    "mm-feedline": 9,
    "back_etch": 10,
    "reduced_e-beam_boundary": 11,
    "nitride_step_down": 12,
    "oxide_step_down": 13,
    "capacitor_plates": 14,
    "aluminium": 15,
    "focal plane": 16,
    "pixel": 17,
    "dowel": 18,
    "pogo_pins": 19,
    "tabs": 20,
    "alignment": 21,
    "titanium_aluminium": 22,
    "fab_1_nitride_membrane_etch": 101,
    "fab_2_oxide_membrane_etch": 102,
    "fab_3_nb_wiring": 103,
    "fab_4_al_meanders": 104,
    "fab_5_microstrip_dielectric": 105,
    "fab_6_nb_ground": 106,
    "fab_7_drie_etch": 107,
    "fab_8_titanium_aluminium": 108,
}

# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0
readout_bridge_separation = 500.0
optimal_bridge_thickness = 6
boundary_width = 300
device_width = 27000

########################################################################################################################

# Section to add device etch boundaries.

device_boundary_points = [
    (-device_width / 2 - boundary_width, -device_width / 2 - boundary_width / 2),
    (device_width / 2 + boundary_width / 2, -device_width / 2 - boundary_width / 2),
    (device_width / 2 + boundary_width / 2, device_width / 2 + boundary_width / 2),
    (-device_width / 2 - boundary_width, device_width / 2 + boundary_width / 2),
]

device_boundary_path = gdspy.FlexPath(
    points=device_boundary_points, width=boundary_width, layer=layers["tabs"]
)

tabs_cell = gdspy.Cell("Test Device Tabs")
tab_cell = gdspy.Cell("Test Device Tab")
single_tab = gdspy.Polygon(
    points=[
        (0.0, -boundary_width / 2),
        (4920.0, -boundary_width / 2),
        (4920.0, boundary_width / 2),
        (0.0, boundary_width / 2),
        (0.0, -boundary_width / 2),
    ],
    layer=layers["tabs"],
)
single_tab.fillet(150)
tab_cell.add(single_tab)

tabs_cell.add(gdspy.CellArray(tab_cell, columns=5, rows=1, spacing=(5670, 0.0)))

main_cell.add(
    gdspy.CellReference(
        tabs_cell,
        origin=(
            -device_width / 2 - boundary_width / 2,
            -device_width / 2 - boundary_width,
        ),
        rotation=90,
    )
)
main_cell.add(device_boundary_path)

########################################################################################################################

# Section to add dowel hole and slot.

dowel_hole_centre = (0.0, 0.0)
dowel_hole = gdspy.Round(center=dowel_hole_centre, radius=407, layer=layers["dowel"])
dowel_ground_hole = gdspy.Round(
    center=dowel_hole_centre, radius=407, layer=layers["ground"]
)
dowel_dielectric_hole = gdspy.Round(
    center=dowel_hole_centre, radius=407, layer=layers["dielectric"]
)
main_cell.add([dowel_hole, dowel_ground_hole, dowel_dielectric_hole])

dowel_slot_centre = (9093.5, 0.0)
dowel_slot_cell = gdspy.Cell("Dowel Slot Test Device")
dowel_slot_points = [
    (0.0, -407.0),
    (1813.0, -407.0),
    (1813.0, 407.0),
    (0.0, 407.0),
    (0.0, -407.0),
]
dowel_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["dowel"])
dowel_ground_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["ground"])
dowel_dielectric_slot = gdspy.Polygon(
    points=dowel_slot_points, layer=layers["dielectric"]
)
dowel_slot.fillet(407)
dowel_ground_slot.fillet(407)
dowel_dielectric_slot.fillet(407)
dowel_slot_cell.add([dowel_slot, dowel_ground_slot, dowel_dielectric_slot])

main_cell.add(
    gdspy.CellReference(dowel_slot_cell, origin=dowel_slot_centre, rotation=0)
)

########################################################################################################################

# Section to add bond pads

bond_pad_top_origin = (0.0, device_width / 2 - 20)
bond_pad_bottom_origin = (0.0, -device_width / 2 + 20)


bond_pad = EdgeBondPad(
    pad_width=400,
    pad_gap=250,
    cpw_width=readout_width,
    cpw_gap=readout_gap,
    pad_length=600,
    transition_length=700,
    origin_x=0.0,
    origin_y=0.0,
)

bond_pad_cell = bond_pad.make_cell(
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    cell_name="Test Device Bond Pad",
)

top_bond_pad_connection = (
    bond_pad.get_feedline_connection_x() + bond_pad_top_origin[0],
    bond_pad.get_feedline_connection_y() + bond_pad_top_origin[1],
)

bottom_bond_pad_connection = (
    bond_pad.get_feedline_connection_x() + bond_pad_bottom_origin[0],
    bond_pad.get_feedline_connection_y() + bond_pad_bottom_origin[1],
)

main_cell.add(
    [
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_top_origin, rotation=0.0),
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_bottom_origin, rotation=180),
    ]
)

########################################################################################################################

# Section to add OMT's

# Create OMT with hybrid and cross overs:
full_omt_instance = DualPolOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    centre_x=0.0,
    centre_y=0.0,
)

full_omt_cell = full_omt_instance.make_cell(
    microstrip_layer=layers["mm-feedline"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
    step_over_layer=layers["ground"],
    lossy_metal_layer=layers["aluminium"],
)

omt_1_origin = (-6000, 6000)
omt_2_origin = (6000, 6000)
omt_3_origin = (-6000, -6000)
omt_4_origin = (6000, -6000)

main_cell.add(
    [
        gdspy.CellReference(full_omt_cell, origin=omt_1_origin, rotation=-90),
        gdspy.CellReference(full_omt_cell, origin=omt_3_origin, rotation=0),
    ]
)

full_omt_cell_connections = full_omt_instance.get_connections()

########################################################################################################################

# Add hybrid to pixel 2:

pixel_3_omt_hybrid_no_step_cell = gdspy.Cell("Single Pol OMT Hybrid No step")

single_pol_omt_instance = DualPolSimpleOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    dual_polarisation=False,
    centre_x=0.0,
    centre_y=0.0,
)
single_pol_omt_cell = single_pol_omt_instance.make_cell(
    microstrip_layer=layers["mm-feedline"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
)
single_pol_omt_connections = single_pol_omt_instance.get_connections()

hybrid = Hybrid180(
    resonator_length=560,
    loop_width=2.0,
    input_width=2.0,
    difference_port_width=2.5,
)
hybrid_cell = hybrid.make_cell(
    microstrip_layer=layers["mm-feedline"], cell_name="Hybrid 180 Test"
)
single_pol_hybrid_origin = (-2700, 0.0)

# Get hybrid connections and connect hybrid to omt:
hybrid_difference_port_connection = hybrid.get_difference_port_connection()
hybrid_difference_port_connection = tools.rotate_coordinates(
    hybrid_difference_port_connection[0],
    hybrid_difference_port_connection[1],
    angle=radians(180),
)
hybrid_difference_port_connection = (
    hybrid_difference_port_connection[0] + single_pol_hybrid_origin[0],
    hybrid_difference_port_connection[1] + single_pol_hybrid_origin[1],
)

hybrid_input_a = hybrid.get_input_connection_A()
hybrid_input_a = tools.rotate_coordinates(
    hybrid_input_a[0], hybrid_input_a[1], angle=radians(180)
)
hybrid_input_a = (
    hybrid_input_a[0] + single_pol_hybrid_origin[0],
    hybrid_input_a[1] + single_pol_hybrid_origin[1],
)

hybrid_input_b = hybrid.get_input_connection_B()
hybrid_input_b = tools.rotate_coordinates(
    hybrid_input_b[0], hybrid_input_b[1], angle=radians(180)
)
hybrid_input_b = (
    hybrid_input_b[0] + single_pol_hybrid_origin[0],
    hybrid_input_b[1] + single_pol_hybrid_origin[1],
)
single_pol_omt_to_hybrid_points = [
    single_pol_omt_connections[0],
    (single_pol_omt_connections[0][0], single_pol_omt_connections[0][1] + 100),
    (single_pol_omt_connections[0][0] - 1200, single_pol_omt_connections[0][1] + 100),
    (hybrid_input_b[0], hybrid_input_b[1] + 300),
    hybrid_input_b,
]
single_pol_omt_to_hybrid_path_1 = gdspy.FlexPath(
    points=single_pol_omt_to_hybrid_points,
    width=2.0,
    corners="circular bend",
    bend_radius=50,
    layer=layers["mm-feedline"],
)
single_pol_omt_to_hybrid_points = [
    single_pol_omt_connections[1],
    (single_pol_omt_connections[1][0], single_pol_omt_connections[1][1] - 100),
    (single_pol_omt_connections[1][0] - 1200, single_pol_omt_connections[1][1] - 100),
    (hybrid_input_a[0], hybrid_input_a[1] - 300),
    hybrid_input_a,
]
single_pol_omt_to_hybrid_path_2 = gdspy.FlexPath(
    points=single_pol_omt_to_hybrid_points,
    width=2.0,
    corners="circular bend",
    bend_radius=50,
    layer=layers["mm-feedline"],
)

# Add lossy line to hybrid sum port:
hybrid_sum_path_points = [
    single_pol_hybrid_origin,
    (single_pol_hybrid_origin[0] + 100, single_pol_hybrid_origin[1]),
]
x = single_pol_hybrid_origin[0] + 100
for n in range(4):
    hybrid_sum_path_points.append((x, single_pol_hybrid_origin[1] + 700))
    hybrid_sum_path_points.append((x + 50, single_pol_hybrid_origin[1] + 700))
    hybrid_sum_path_points.append((x + 50, single_pol_hybrid_origin[1] - 700))
    hybrid_sum_path_points.append((x + 100, single_pol_hybrid_origin[1] - 700))
    hybrid_sum_path_points.append((x + 100, single_pol_hybrid_origin[1]))
    x += 100

hybrid_sum_path = gdspy.FlexPath(
    points=hybrid_sum_path_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["aluminium"],
)

# add components to omt cell
pixel_3_omt_hybrid_no_step_cell.add(
    [
        gdspy.CellReference(single_pol_omt_cell, origin=(0.0, 0.0)),
        gdspy.CellReference(
            hybrid_cell,
            origin=single_pol_hybrid_origin,
            rotation=180,
            x_reflection=False,
        ),
        single_pol_omt_to_hybrid_path_1,
        single_pol_omt_to_hybrid_path_2,
        hybrid_sum_path,
    ]
)

# Add single pol omt to main cell.
main_cell.add(
    [
        gdspy.CellReference(pixel_3_omt_hybrid_no_step_cell, origin=omt_2_origin, rotation=0),
    ]
)

########################################################################################################################

# Add hybrid to pixel 2:

pixel_4_omt_hybrid_no_step_cell = gdspy.Cell("P4 Dual Pol OMT Hybrid No step")

dual_pol_omt_instance = DualPolSimpleOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    dual_polarisation=True,
    centre_x=0.0,
    centre_y=0.0,
)
dual_pol_omt_cell = dual_pol_omt_instance.make_cell(
    microstrip_layer=layers["mm-feedline"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
    cell_name="P4 Dual pol simple omt"
)
dual_pol_omt_connections = dual_pol_omt_instance.get_connections()

dual_pol_hybrid_origin = (0.0, 3500)

# Get hybrid connections and connect hybrid to omt:
p4_hybrid_difference_port_connection = hybrid.get_difference_port_connection()
p4_hybrid_difference_port_connection = tools.rotate_coordinates(
    p4_hybrid_difference_port_connection[0],
    p4_hybrid_difference_port_connection[1],
    angle=radians(90),
)
p4_hybrid_difference_port_connection = (
    p4_hybrid_difference_port_connection[0] + dual_pol_hybrid_origin[0],
    p4_hybrid_difference_port_connection[1] + dual_pol_hybrid_origin[1],
)

p4_hybrid_input_a = hybrid.get_input_connection_A()
p4_hybrid_input_a = tools.rotate_coordinates(
    p4_hybrid_input_a[0], p4_hybrid_input_a[1], angle=radians(90)
)
p4_hybrid_input_a = (
    p4_hybrid_input_a[0] + dual_pol_hybrid_origin[0],
    p4_hybrid_input_a[1] + dual_pol_hybrid_origin[1],
)

p4_hybrid_input_b = hybrid.get_input_connection_B()
p4_hybrid_input_b = tools.rotate_coordinates(
    p4_hybrid_input_b[0], p4_hybrid_input_b[1], angle=radians(90)
)
p4_hybrid_input_b = (
    p4_hybrid_input_b[0] + dual_pol_hybrid_origin[0],
    p4_hybrid_input_b[1] + dual_pol_hybrid_origin[1],
)
dual_pol_omt_to_hybrid_points = [
    dual_pol_omt_connections[1],
    (dual_pol_omt_connections[1][0] + 100, dual_pol_omt_connections[1][1]),
    (dual_pol_omt_connections[1][0] + 100, dual_pol_omt_connections[1][1] + 1200),
    (p4_hybrid_input_b[0] + 300, p4_hybrid_input_b[1]),
    p4_hybrid_input_b,
]
dual_pol_omt_to_hybrid_path_1 = gdspy.FlexPath(
    points=dual_pol_omt_to_hybrid_points,
    width=2.0,
    corners="circular bend",
    bend_radius=50,
    layer=layers["mm-feedline"],
)
dual_pol_omt_to_hybrid_points = [
    dual_pol_omt_connections[3],
    (dual_pol_omt_connections[3][0] - 100, dual_pol_omt_connections[3][1]),
    (dual_pol_omt_connections[3][0] - 100, dual_pol_omt_connections[3][1] + 1200),
    (p4_hybrid_input_a[0] - 300, p4_hybrid_input_a[1]),
    p4_hybrid_input_a,
]
dual_pol_omt_to_hybrid_path_2 = gdspy.FlexPath(
    points=dual_pol_omt_to_hybrid_points,
    width=2.0,
    corners="circular bend",
    bend_radius=50,
    layer=layers["mm-feedline"],
)

# Add lossy line to hybrid sum port:
p4_hybrid_sum_path_points = [
    dual_pol_hybrid_origin,
    (dual_pol_hybrid_origin[0], dual_pol_hybrid_origin[1] - 100),
]
y = dual_pol_hybrid_origin[1] - 100
for n in range(4):
    p4_hybrid_sum_path_points.append((dual_pol_hybrid_origin[0] + 700, y))
    p4_hybrid_sum_path_points.append((dual_pol_hybrid_origin[0] + 700, y - 50))
    p4_hybrid_sum_path_points.append((dual_pol_hybrid_origin[0] - 700, y - 50))
    p4_hybrid_sum_path_points.append((dual_pol_hybrid_origin[0] - 700, y - 100))
    p4_hybrid_sum_path_points.append((dual_pol_hybrid_origin[0], y - 100))
    y -= 100

p4_hybrid_sum_path = gdspy.FlexPath(
    points=p4_hybrid_sum_path_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["aluminium"],
)

# Add lossy line to hybrid sum port:
p4_probe_termination_points = [
    dual_pol_omt_connections[0],
    (dual_pol_omt_connections[0][0], dual_pol_omt_connections[0][1] + 100),
]
y = dual_pol_omt_connections[0][1] + 100
for n in range(4):
    p4_probe_termination_points.append((dual_pol_omt_connections[0][0] + 700, y))
    p4_probe_termination_points.append((dual_pol_omt_connections[0][0] + 700, y + 50))
    p4_probe_termination_points.append((dual_pol_omt_connections[0][0] - 700, y + 50))
    p4_probe_termination_points.append((dual_pol_omt_connections[0][0] - 700, y + 100))
    p4_probe_termination_points.append((dual_pol_omt_connections[0][0], y + 100))
    y += 100

p4_probe_termination_path = gdspy.FlexPath(
    points=p4_probe_termination_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["aluminium"],
)

# add components to omt cell
pixel_4_omt_hybrid_no_step_cell.add(
    [
        gdspy.CellReference(dual_pol_omt_cell, origin=(0.0, 0.0)),
        gdspy.CellReference(
            hybrid_cell,
            origin=dual_pol_hybrid_origin,
            rotation=90,
            x_reflection=False,
        ),
        dual_pol_omt_to_hybrid_path_1,
        dual_pol_omt_to_hybrid_path_2,
        p4_hybrid_sum_path,
        p4_probe_termination_path
    ]
)

# Add single pol omt to main cell.
main_cell.add(
    [
        gdspy.CellReference(pixel_4_omt_hybrid_no_step_cell, origin=omt_4_origin, rotation=0),
    ]
)


########################################################################################################################

# Section to add simple mini submodule filter-banks to pixel 1.
r100_filter_bank_cell_a = gdspy.Cell("R100 Filter Bank Cell #A")
r100_filter_bank_cell_b = gdspy.Cell("R100 Filter Bank Cell #B")

r100_fbs_1_spacing = 0.35

# Define readout frequency band:
r100_frequency_min = 145
r100_frequency_max = 155
oversampling = 1.6

r100_number_of_channels = round(
    oversampling * 100 * np.log(r100_frequency_max / r100_frequency_min)
)
print(f"R100 Number of channels = {r100_number_of_channels}")
r100_filter_f0_array_1 = tools.create_target_f0_array(
    r100_frequency_min, r100_frequency_max, r100_number_of_channels
)
print(f"R100 Filter Channels: {r100_filter_f0_array_1}")

r100_lekid_f0_array = np.linspace(2.2e9, 2.7e9, r100_number_of_channels)
index = np.linspace(0, r100_number_of_channels, r100_number_of_channels)

top_f0s = r100_lekid_f0_array[1::2]
bottom_f0s = r100_lekid_f0_array[::2]

top_mixing_pattern = [0, 2, 4, 1, 3]
bottom_mixing_pattern = [0, 2, 4, 1, 3, 5]

r100_lekid_f0_array_a = np.insert(
    bottom_f0s[bottom_mixing_pattern],
    np.arange(len(bottom_f0s) - 1) + 1,
    top_f0s[top_mixing_pattern],
)
r100_lekid_f0_array_b = r100_lekid_f0_array_a + 10e6
r100_full_band_f0_a = 2.15e9
r100_full_band_f0_b = 2.16e9

plt.figure(figsize=(8, 6))
plt.plot(r100_lekid_f0_array_a[1::2] * 1e-9, linestyle="--", marker="o", label="FBS A: Top")
plt.plot(r100_lekid_f0_array_a[::2] * 1e-9, linestyle="--", marker="o", label="FBS A: Bottom")
plt.plot(r100_lekid_f0_array_b[1::2] * 1e-9, linestyle="--", marker="s", label="FBS B: Top")
plt.plot(r100_lekid_f0_array_b[::2] * 1e-9, linestyle="--", marker="s", label="FBS B: Bottom")
plt.ylabel("Frequency (GHz)", fontsize=16)
plt.xlabel("Detector position along FBS side", fontsize=16)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.legend()
plt.show()

r100_filter_bank_1 = FilterBank(
    target_f0_array=r100_filter_f0_array_1,
    feedline_width=feedline_width,
    filter_separation=r100_fbs_1_spacing,
    input_connection_x=0,
    input_connection_y=0,
)

(
    r100_filter_bank_cell_a,
    r100_fbs_connection_1_a,
    r100_fbs_connection_2_a,
    r100_fbs_connection_3_a,
    r100_fbs_connection_4_a,
) = util.spectrometer_builders.make_single_simple_spectrometer_cell(
    lekid_f0_array=r100_lekid_f0_array_a,
    full_band_lekid_f0=r100_full_band_f0_a,
    filter_bank=r100_filter_bank_1,
    spectrometer_cell=r100_filter_bank_cell_a,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
    parallel_plate=False,
    step_down=True,
)

(
    r100_filter_bank_cell_b,
    r100_fbs_connection_1_b,
    r100_fbs_connection_2_b,
    r100_fbs_connection_3_b,
    r100_fbs_connection_4_b,
) = util.spectrometer_builders.make_single_simple_spectrometer_cell(
    lekid_f0_array=r100_lekid_f0_array_b,
    full_band_lekid_f0=r100_full_band_f0_b,
    filter_bank=r100_filter_bank_1,
    spectrometer_cell=r100_filter_bank_cell_b,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
    parallel_plate=False,
    step_down=True,
)

r100_fbs_origin_a = (omt_1_origin[0] + 3500, omt_1_origin[1] + 1000)
r100_fbs_origin_b = (omt_1_origin[0] - 1000, omt_1_origin[1] - 3500)

main_cell.add(
    gdspy.CellReference(r100_filter_bank_cell_a, origin=r100_fbs_origin_a, rotation=0)
)
main_cell.add(
    gdspy.CellReference(r100_filter_bank_cell_b, origin=r100_fbs_origin_b, rotation=-90)
)


########################################################################################################################

# Section to add simple mini submodule filter-banks to pixel 1.
changing_r_fbs_cell_a = gdspy.Cell("Rising R Filter Bank Cell #+R")
changing_r_fbs_cell_b = gdspy.Cell("Decreasing R Filter Bank Cell #-R")

changing_r_fbs_spacing = 0.3

changing_r_fbs_resolutions = np.array([100, 200, 300, 400, 500])
changing_r_fbs_lekid_f0_array_a = np.linspace(
    1.97e9, 2.0e9, changing_r_fbs_resolutions.size
)
changing_r_fbs_lekid_f0_array_b = np.linspace(
    1.82e9, 1.85e9, changing_r_fbs_resolutions.size
)

changing_r_full_band_f0_a = 1.96e9
changing_r_full_band_f0_b = 1.81e9

# Rising R with frequency: (R100:130GHz, R200:140GHz, R300:150GHz, R400:160GHz, R500:170GHz)
resonator_length_array_a = np.flip([283.71640206, 271.56363745, 257.04229093, 242.78539149, 230.29322058])
input_capacitor_array_a = np.flip([24.16880423, 15.42973826, 11.47718521,  9.09374902,  7.51111001])
output_capacitor_array_a = np.flip([18.32915089, 11.39803643,  8.17662151,  6.20017364,  4.97485526])

# Decreasing R with frequency: (R100:170GHz, R200:160GHz, R300:150GHz, R400:140GHz, R500:130GHz)
resonator_length_array_b = [219.43189782, 238.7766873,  257.04229093, 276.34680024, 298.89199572]
input_capacitor_array_b = [17.09750876, 13.06579915, 11.47718521, 10.68120365, 10.34423942]
output_capacitor_array_b = [12.87782493,  9.44721289,  8.17662151,  7.45537033,  7.04653827]

changing_r_fbs_a = CustomFilterBank(
    resonator_length_array=resonator_length_array_a,
    input_capacitor_array=input_capacitor_array_a,
    output_capacitor_array=output_capacitor_array_a,
    feedline_width=feedline_width,
    filter_separation=changing_r_fbs_spacing,
    input_connection_x=0,
    input_connection_y=0,
)
changing_r_fbs_b = CustomFilterBank(
    resonator_length_array=resonator_length_array_b,
    input_capacitor_array=input_capacitor_array_b,
    output_capacitor_array=output_capacitor_array_b,
    feedline_width=feedline_width,
    filter_separation=changing_r_fbs_spacing,
    input_connection_x=0,
    input_connection_y=0,
)

(
    changing_r_fbs_cell_a,
    changing_r_fbs_connection_1a,
    changing_r_fbs_connection_2a,
    changing_r_fbs_connection_3a,
    changing_r_fbs_connection_4a,
) = util.spectrometer_builders.make_single_simple_spectrometer_cell(
    lekid_f0_array=changing_r_fbs_lekid_f0_array_a,
    full_band_lekid_f0=changing_r_full_band_f0_a,
    filter_bank=changing_r_fbs_a,
    spectrometer_cell=changing_r_fbs_cell_a,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
    parallel_plate=True,
    step_down=False,
)

(
    changing_r_fbs_cell_b,
    changing_r_fbs_connection_1b,
    changing_r_fbs_connection_2b,
    changing_r_fbs_connection_3b,
    changing_r_fbs_connection_4b,
) = util.spectrometer_builders.make_single_simple_spectrometer_cell(
    lekid_f0_array=changing_r_fbs_lekid_f0_array_b,
    full_band_lekid_f0=changing_r_full_band_f0_b,
    filter_bank=changing_r_fbs_b,
    spectrometer_cell=changing_r_fbs_cell_b,
    readout_width=readout_width,
    readout_gap=readout_gap,
    layers=layers,
    parallel_plate=True,
    step_down=False,
)

changing_r_origin_a = (omt_2_origin[0] - 4000, omt_2_origin[1] + 1000)
changing_r_origin_b = (omt_3_origin[0] - 1000, omt_3_origin[1] + 4000)

main_cell.add(
    gdspy.CellReference(changing_r_fbs_cell_a, origin=changing_r_origin_a, rotation=180)
)
main_cell.add(
    gdspy.CellReference(changing_r_fbs_cell_b, origin=changing_r_origin_b, rotation=90)
)


########################################################################################################################

# Section connecting pixel 1 to filterbanks:

pixel_1_path_points_a = [
    (
        full_omt_cell_connections[1][0] + omt_1_origin[0],
        full_omt_cell_connections[1][1] + omt_1_origin[1],
    ),
    (
        full_omt_cell_connections[1][0] + omt_1_origin[0],
        full_omt_cell_connections[1][1] + omt_1_origin[1] - 50,
    ),
    (
        full_omt_cell_connections[1][0] + omt_1_origin[0] + 250,
        full_omt_cell_connections[1][1] + omt_1_origin[1] - 50,
    ),
    (
        full_omt_cell_connections[1][0] + omt_1_origin[0] + 250,
        r100_fbs_origin_a[1],
    ),
    r100_fbs_origin_a,
]
pixel_1_path_a = gdspy.FlexPath(
    points=pixel_1_path_points_a,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

pixel_1_output = tools.rotate_coordinates(full_omt_cell_connections[0][0], full_omt_cell_connections[0][1], angle=radians(180))
pixel_1_path_points_b = [
    (
        pixel_1_output[0] + omt_1_origin[0],
        pixel_1_output[1] + omt_1_origin[1],
    ),
    (
        r100_fbs_origin_b[0],
        pixel_1_output[1] + omt_1_origin[1],
    ),
    r100_fbs_origin_b,
]
pixel_1_path_b = gdspy.FlexPath(
    points=pixel_1_path_points_b,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

main_cell.add([
    pixel_1_path_a,
    pixel_1_path_b
])

########################################################################################################################

# Connect FBS to pixel 2:

pixel_2_fbs_path_points = [
    (
        hybrid_difference_port_connection[0] + omt_2_origin[0],
        hybrid_difference_port_connection[1] + omt_2_origin[1],
    ),
    (
        hybrid_difference_port_connection[0] + omt_2_origin[0],
        changing_r_origin_a[1],
    ),
    changing_r_origin_a,
]
pixel_2_fbs_path = gdspy.FlexPath(
    points=pixel_2_fbs_path_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

main_cell.add(pixel_2_fbs_path)

########################################################################################################################

# Section connecting pixel 3 to filterbanks:

pixel_3_fbs_path_points_a = [
    (
        full_omt_cell_connections[0][0] + omt_3_origin[0],
        full_omt_cell_connections[0][1] + omt_3_origin[1],
    ),
    (
        full_omt_cell_connections[0][0] + omt_3_origin[0] + 50,
        full_omt_cell_connections[0][1] + omt_3_origin[1],
    ),
    (
        full_omt_cell_connections[0][0] + omt_3_origin[0] + 50,
        full_omt_cell_connections[0][1] + omt_3_origin[1] + 250,
    ),
    (
        changing_r_origin_b[0],
        full_omt_cell_connections[0][1] + omt_3_origin[1] + 250,
    ),
    changing_r_origin_b,
]
pixel_3_fbs_path = gdspy.FlexPath(
    points=pixel_3_fbs_path_points_a,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

main_cell.add([
    pixel_3_fbs_path
])

########################################################################################################################

# Add directly coupled KID to pixel 3 hybrid and cross over.
pixel_3_direct_kid_f0 = 1.72e9
pixel_3_direct_kid = PPLekid(
    target_f0=pixel_3_direct_kid_f0,
    signal_input_x=0,
    signal_input_y=0,
)

pixel_3_direct_kid_cell = pixel_3_direct_kid.make_cell(
    inductor_layer=layers["inductors"],
    device_plate_layer=layers["capacitor_plates"],
    readout_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    readout_width=readout_width,
    readout_gap=readout_gap,
    cell_name="Pixel 3 Direct Kid #P3 DC",
)

pixel_3_direct_kid_readout_connections = pixel_3_direct_kid.get_readout_connections(readout_width=readout_width)
pixel_3_direct_kid_readout_length = pixel_3_direct_kid_readout_connections[0][1]
pixel_3_direct_kid_origin = (
    bond_pad_bottom_origin[0] - pixel_3_direct_kid_readout_length,
    omt_3_origin[1],
)

# Connect to hybrid:
pixel_3_direct_kid_input_points = [
    (
        full_omt_cell_connections[1][0] + omt_3_origin[0],
        full_omt_cell_connections[1][1] + omt_3_origin[1],
    ),
    (
        full_omt_cell_connections[1][0] + omt_3_origin[0],
        full_omt_cell_connections[1][1] + omt_3_origin[1] - 50,
    ),
    (
        full_omt_cell_connections[1][0] + omt_3_origin[0] + 150,
        full_omt_cell_connections[1][1] + omt_3_origin[1] - 50,
    ),
    (
        full_omt_cell_connections[1][0] + omt_3_origin[0] + 150,
        pixel_3_direct_kid_origin[1],
    ),
    pixel_3_direct_kid_origin,
]
pixel_3_direct_kid_input_path = gdspy.FlexPath(
    points=pixel_3_direct_kid_input_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

main_cell.add(
    [
        gdspy.CellReference(
            pixel_3_direct_kid_cell, origin=pixel_3_direct_kid_origin, rotation=-90
        ),
        pixel_3_direct_kid_input_path,
    ]
)

########################################################################################################################

# Add directly coupled KID to pixel 4 hybrid.

pixel_4_direct_hybrid_kid_f0 = 1.7e9
pixel_4_direct_hybrid_kid = PPLekid(
    target_f0=pixel_4_direct_hybrid_kid_f0,
    signal_input_x=0,
    signal_input_y=0,
)

pixel_4_direct_hybrid_kid_cell = pixel_4_direct_hybrid_kid.make_cell(
    inductor_layer=layers["inductors"],
    device_plate_layer=layers["capacitor_plates"],
    readout_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    readout_width=readout_width,
    readout_gap=readout_gap,
    cell_name="Pixel 4 Direct hybrid  #P4 DC1",
)

pixel_4_direct_hybrid_kid_readout_connections = pixel_4_direct_hybrid_kid.get_readout_connections(readout_width=readout_width)
pixel_4_direct_hybrid_kid_readout_length = pixel_4_direct_hybrid_kid_readout_connections[0][1]
pixel_4_direct_hybrid_kid_origin = (
    changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500 + pixel_4_direct_hybrid_kid_readout_length,
    p4_hybrid_difference_port_connection[1] + omt_4_origin[1] + 500
)

# Connect to hybrid:
pixel_4_direct_hybrid_kid_input_points = [
    (
        p4_hybrid_difference_port_connection[0] + omt_4_origin[0],
        p4_hybrid_difference_port_connection[1] + omt_4_origin[1],
    ),
    (
        p4_hybrid_difference_port_connection[0] + omt_4_origin[0] + 50,
        p4_hybrid_difference_port_connection[1] + omt_4_origin[1],
    ),
    (
        p4_hybrid_difference_port_connection[0] + omt_4_origin[0] + 50,
        p4_hybrid_difference_port_connection[1] + omt_4_origin[1] + 500,
    ),
    pixel_4_direct_hybrid_kid_origin,
]
pixel_4_direct_hybrid_kid_input_path = gdspy.FlexPath(
    points=pixel_4_direct_hybrid_kid_input_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

# Add KID directly to probe:
pixel_4_direct_kid_f0 = 1.71e9
pixel_4_direct_kid = PPLekid(
    target_f0=pixel_4_direct_kid_f0,
    signal_input_x=0,
    signal_input_y=0,
)

pixel_4_direct_kid_cell = pixel_4_direct_kid.make_cell(
    inductor_layer=layers["inductors"],
    device_plate_layer=layers["capacitor_plates"],
    readout_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    readout_width=readout_width,
    readout_gap=readout_gap,
    cell_name="Pixel 4 Direct hybrid  #P4 DC2",
)

pixel_4_direct_kid_readout_connections = pixel_4_direct_kid.get_readout_connections(readout_width=readout_width)
pixel_4_direct_kid_readout_length = pixel_4_direct_kid_readout_connections[0][1]
pixel_4_direct_kid_origin = (
    bottom_bond_pad_connection[0] + pixel_4_direct_kid_readout_length,
    dual_pol_omt_connections[2][1] + omt_4_origin[1] - 500
)

# Connect to probe:
pixel_4_direct_kid_input_points = [
    (
        dual_pol_omt_connections[2][0] + omt_4_origin[0],
        dual_pol_omt_connections[2][1] + omt_4_origin[1],
    ),
    (
        dual_pol_omt_connections[2][0] + omt_4_origin[0],
        dual_pol_omt_connections[2][1] + omt_4_origin[1] - 500,
    ),
    pixel_4_direct_kid_origin,
]
pixel_4_direct_kid_input_path = gdspy.FlexPath(
    points=pixel_4_direct_kid_input_points,
    width=feedline_width,
    corners="circular bend",
    bend_radius=25,
    layer=layers["mm-feedline"],
)

main_cell.add(
    [
        gdspy.CellReference(
            pixel_4_direct_hybrid_kid_cell, origin=pixel_4_direct_hybrid_kid_origin, rotation=90
        ),
        gdspy.CellReference(
            pixel_4_direct_kid_cell, origin=pixel_4_direct_kid_origin, rotation=90
        ),
        pixel_4_direct_hybrid_kid_input_path,
        pixel_4_direct_kid_input_path
    ]
)

########################################################################################################################

# Connect Readouts:

# **********************************************************************************************************************

# Input:
changing_r_fbs_connection_4a = tools.rotate_coordinates(changing_r_fbs_connection_4a[0], changing_r_fbs_connection_4a[1], angle=radians(180))
readout_input_points = [
    (
        top_bond_pad_connection[0],
        top_bond_pad_connection[1],
    ),
    (
        top_bond_pad_connection[0],
        top_bond_pad_connection[1] - 500,
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        top_bond_pad_connection[1] - 500,
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        top_bond_pad_connection[1] - 2500,
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        changing_r_fbs_connection_4a[1] + changing_r_origin_a[1],
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0],
        changing_r_fbs_connection_4a[1] + changing_r_origin_a[1],
    ),
]

readout_input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=readout_input_points[:4],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=0.1,
    final_bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout input transition",
)

readout_input = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_input_points[3:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout input",
)


# **********************************************************************************************************************
# -R to R100 A FBS readout

changing_r_fbs_connection_3a = tools.rotate_coordinates(changing_r_fbs_connection_3a[0], changing_r_fbs_connection_3a[1], angle=radians(180))
readout_segment_points_1 = [
    (
        changing_r_fbs_connection_3a[0] + changing_r_origin_a[0],
        changing_r_fbs_connection_3a[1] + changing_r_origin_a[1],
    ),
    (
        top_bond_pad_connection[0],
        changing_r_fbs_connection_3a[1] + changing_r_origin_a[1],
    ),
    (
        top_bond_pad_connection[0],
        r100_fbs_connection_2_a[1] + changing_r_origin_a[1],
    ),
    (
        r100_fbs_connection_2_a[0] + r100_fbs_origin_a[0],
        r100_fbs_connection_2_a[1] + r100_fbs_origin_a[1],
    ),
]

readout_segment_1 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_1,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 1",
)

# **********************************************************************************************************************
# R100 A to R100 B FBS readout

r100_fbs_connection_4_b = tools.rotate_coordinates(r100_fbs_connection_4_b[0], r100_fbs_connection_4_b[1], angle=radians(-90))
readout_segment_points_2 = [
    (
        r100_fbs_connection_1_a[0] + r100_fbs_origin_a[0],
        r100_fbs_connection_1_a[1] + r100_fbs_origin_a[1],
    ),
    (
        omt_1_origin[0],
        r100_fbs_connection_1_a[1] + r100_fbs_origin_a[1],
    ),
    (
        r100_fbs_connection_4_b[0] + r100_fbs_origin_b[0],
        omt_1_origin[1],
    ),
    (
        r100_fbs_connection_4_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_4_b[1] + r100_fbs_origin_b[1],
    ),
]

readout_segment_2 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_2,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 2",
)

# **********************************************************************************************************************
# R100 B to +R B FBS readout

r100_fbs_connection_3_b = tools.rotate_coordinates(r100_fbs_connection_3_b[0], r100_fbs_connection_3_b[1], angle=radians(-90))
changing_r_fbs_connection_1b = tools.rotate_coordinates(changing_r_fbs_connection_1b[0], changing_r_fbs_connection_1b[1], angle=radians(90))
readout_segment_points_3 = [
    (
        r100_fbs_connection_3_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_3_b[1] + r100_fbs_origin_b[1],
    ),
    (
        r100_fbs_connection_3_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_3_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        changing_r_fbs_connection_1b[0] + changing_r_origin_b[0],
        r100_fbs_connection_3_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        changing_r_fbs_connection_1b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_1b[1] + changing_r_origin_b[1],
    ),
]

readout_segment_3 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_3,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 3",
)

# **********************************************************************************************************************
# +R B to +R B FBS readout

changing_r_fbs_connection_2b = tools.rotate_coordinates(changing_r_fbs_connection_2b[0], changing_r_fbs_connection_2b[1], angle=radians(90))
changing_r_fbs_connection_3b = tools.rotate_coordinates(changing_r_fbs_connection_3b[0], changing_r_fbs_connection_3b[1], angle=radians(90))
readout_segment_points_4 = [
    (
        changing_r_fbs_connection_2b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_2b[1] + changing_r_origin_b[1],
    ),
    (
        changing_r_fbs_connection_2b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_2b[1] + changing_r_origin_b[1] + 500,
    ),
    (
        changing_r_fbs_connection_3b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_2b[1] + changing_r_origin_b[1] + 500,
    ),
    (
        changing_r_fbs_connection_3b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_3b[1] + changing_r_origin_b[1],
    ),
]

readout_segment_4 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_4,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 4",
)

# **********************************************************************************************************************
# +R B to R100 B FBS readout

r100_fbs_connection_2_b = tools.rotate_coordinates(r100_fbs_connection_2_b[0], r100_fbs_connection_2_b[1], angle=radians(-90))
changing_r_fbs_connection_4b = tools.rotate_coordinates(changing_r_fbs_connection_4b[0], changing_r_fbs_connection_4b[1], angle=radians(90))
readout_segment_points_5 = [
    (
        changing_r_fbs_connection_4b[0] + changing_r_origin_b[0],
        changing_r_fbs_connection_4b[1] + changing_r_origin_b[1],
    ),
    (
        changing_r_fbs_connection_4b[0] + changing_r_origin_b[0],
        r100_fbs_connection_2_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        r100_fbs_connection_2_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_2_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        r100_fbs_connection_2_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_2_b[1] + r100_fbs_origin_b[1],
    ),
]

readout_segment_5 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_5,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 5",
)

# **********************************************************************************************************************
# R100 B to R100 A FBS readout

r100_fbs_connection_1_b = tools.rotate_coordinates(r100_fbs_connection_1_b[0], r100_fbs_connection_1_b[1], angle=radians(-90))
readout_segment_points_6 = [
    (
        r100_fbs_connection_1_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_1_b[1] + r100_fbs_origin_b[1],
    ),
    (
        r100_fbs_connection_1_b[0] + r100_fbs_origin_b[0],
        r100_fbs_connection_4_a[1] + r100_fbs_origin_a[1],
    ),
    (
        r100_fbs_connection_4_a[0] + r100_fbs_origin_a[0],
        r100_fbs_connection_4_a[1] + r100_fbs_origin_a[1],
    ),
]

readout_segment_6 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_6,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 6",
)

# **********************************************************************************************************************
# R100 A to -R FBS readout

changing_r_fbs_connection_4a = tools.rotate_coordinates(changing_r_fbs_connection_4a[0], changing_r_fbs_connection_4a[1], angle=radians(180))
readout_segment_points_7 = [
    (
        r100_fbs_connection_3_a[0] + r100_fbs_origin_a[0],
        r100_fbs_connection_3_a[1] + r100_fbs_origin_a[1],
    ),
    (
        top_bond_pad_connection[0],
        r100_fbs_connection_3_a[1] + r100_fbs_origin_a[1],
    ),
    (
        top_bond_pad_connection[0],
        changing_r_fbs_connection_4a[1] + changing_r_origin_a[1],
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0],
        changing_r_fbs_connection_4a[1] + changing_r_origin_a[1],
    ),
]

readout_segment_7 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_7,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 7",
)

# **********************************************************************************************************************
# -R A to pixel 4 Direct KID readout

changing_r_fbs_connection_1a = tools.rotate_coordinates(changing_r_fbs_connection_1a[0], changing_r_fbs_connection_1a[1], angle=radians(180))

readout_segment_points_8 = [
    (
        changing_r_fbs_connection_1a[0] + changing_r_origin_a[0],
        changing_r_fbs_connection_1a[1] + changing_r_origin_a[1],
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        changing_r_fbs_connection_1a[1] + changing_r_origin_a[1],
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        pixel_4_direct_hybrid_kid_origin[1] + pixel_4_direct_hybrid_kid_readout_connections[1][0],
    ),
]

readout_segment_8 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_8,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 8",
)

# **********************************************************************************************************************
# pixel 4 Direct hybrid KID to pixel 3 direct kid readout

readout_segment_points_9 = [
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        pixel_4_direct_hybrid_kid_origin[1] + pixel_4_direct_hybrid_kid_readout_connections[0][0],
    ),
    (
        changing_r_fbs_connection_4a[0] + changing_r_origin_a[0] + 500,
        r100_fbs_connection_2_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        bottom_bond_pad_connection[0],
        r100_fbs_connection_2_b[1] + r100_fbs_origin_b[1] - 3800,
    ),
    (
        bottom_bond_pad_connection[0],
        pixel_3_direct_kid_origin[1] + pixel_3_direct_kid_readout_connections[0][0],
    ),
]

readout_segment_9 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_9,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 9",
)

# **********************************************************************************************************************
# pixel 3 Direct KID to pixel 4 direct kid readout

readout_segment_points_10 = [
    (
        bottom_bond_pad_connection[0],
        pixel_3_direct_kid_origin[1] + pixel_3_direct_kid_readout_connections[0][0],
    ),
    (
        bottom_bond_pad_connection[0],
        pixel_4_direct_kid_origin[1] + pixel_4_direct_kid_readout_connections[1][0],
    ),
]

readout_segment_10 = general_gds_tools.build_cpw_with_bridges(
    path_points=readout_segment_points_10,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout Segment 10",
)

# **********************************************************************************************************************

# Output:
bottom_bond_pad_connection = tools.rotate_coordinates(top_bond_pad_connection[0], top_bond_pad_connection[1], angle=radians(180))
readout_output_points = [
    (
        bottom_bond_pad_connection[0],
        pixel_4_direct_kid_origin[1] + pixel_4_direct_kid_readout_connections[0][0],
    ),
    bottom_bond_pad_connection,
]

readout_output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=readout_output_points,
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout output transition",
)

# Readout built fromm the following:
main_cell.add(readout_input_transition)
main_cell.add(readout_input)
main_cell.add(readout_segment_1)
main_cell.add(readout_segment_2)
main_cell.add(readout_segment_3)
main_cell.add(readout_segment_4)
main_cell.add(readout_segment_5)
main_cell.add(readout_segment_6)
main_cell.add(readout_segment_7)
main_cell.add(readout_segment_8)
main_cell.add(readout_segment_9)
main_cell.add(readout_segment_10)
main_cell.add(readout_output_transition)

########################################################################################################################
# Section to copy and expand oxide step down layer to form nitride stepdown layer.

nitride_step_down_polygon_set = (
    util.general_gds_tools.offset_polygons_on_layer(
        layer_to_reduce=layers["oxide_step_down"],
        output_layer=layers["nitride_step_down"],
        cell=main_cell,
        offset=10.0,
    )
)
main_cell.add(nitride_step_down_polygon_set)
########################################################################################################################

# Add logos and text

# Section to add unique id and logos
chip_id_text = gdspy.Text(
    text=chip_unique_id,
    size=600,
    position=(-13000, -13000),
    angle=0,
    layer=layers["ground"],
)

main_cell.add(chip_id_text)

########################################################################################################################

# Section to add logos:
logos_cell = gdspy.Cell("Logos")

# ANL
anl_logo_cell = gdspy.Cell("ANL Logo")
anl_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\anl_logo.gds", layer=0
)
anl_logo_polygon_set = gdspy.PolygonSet(anl_logo_polygons, layer=layers["ground"])
anl_logo_cell.add(anl_logo_polygon_set)

# UC
uc_logo_cell = gdspy.Cell("UC Logo")
uc_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\uc_logo.gds", layer=0
)
uc_logo_polygon_set = gdspy.PolygonSet(uc_logo_polygons, layer=layers["ground"])
uc_logo_cell.add(uc_logo_polygon_set)

# Cardiff
cardiff_logo_cell = gdspy.Cell("Cardiff Logo")
cardiff_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\cardiff_logo.gds", layer=0
)
cardiff_logo_polygon_set = gdspy.PolygonSet(
    cardiff_logo_polygons, layer=layers["ground"]
)
cardiff_logo_cell.add(cardiff_logo_polygon_set)

logos_cell.add(
    [
        gdspy.CellReference(cardiff_logo_cell, origin=(0.0, 0.0)),
        gdspy.CellReference(uc_logo_cell, origin=(5000, -800)),
        gdspy.CellReference(anl_logo_cell, origin=(5000, 800)),
    ]
)

main_cell.add(
    gdspy.CellReference(logos_cell, origin=(5500, 11500), rotation=0)
)

########################################################################################################################
cmap = plt.get_cmap("tab10")
plt.figure(figsize=(10, 6))
plt.plot()
plt.vlines(
    r100_lekid_f0_array_a*1e-9, ymax=0.0, ymin=-10, label="R100 - A", color=cmap(1), linewidth=2
)
plt.vlines(
    r100_full_band_f0_a*1e-9, ymax=0.0, ymin=-10, label="R100 - A Full Band", color=cmap(1), linewidth=2, linestyles=":"
)
plt.vlines(
    r100_lekid_f0_array_b*1e-9, ymax=0.0, ymin=-10, label="R100 - B", color=cmap(2), linewidth=2
)
plt.vlines(
    r100_full_band_f0_b*1e-9, ymax=0.0, ymin=-10, label="R100 - B Full Band", color=cmap(2), linewidth=2, linestyles=":"
)
plt.vlines(
    changing_r_fbs_lekid_f0_array_a*1e-9, ymax=0.0, ymin=-10, label="+R", color=cmap(3), linewidth=2
)
plt.vlines(
    changing_r_full_band_f0_a*1e-9, ymax=0.0, ymin=-10, label="+R Full Band", color=cmap(3), linewidth=2, linestyles=":"
)
plt.vlines(
    changing_r_fbs_lekid_f0_array_b*1e-9, ymax=0.0, ymin=-10, label="-R", color=cmap(4), linewidth=2
)
plt.vlines(
    changing_r_full_band_f0_b*1e-9, ymax=0.0, ymin=-10, label="-R Full Band", color=cmap(4), linewidth=2, linestyles=":"
)
plt.vlines(
    pixel_3_direct_kid_f0*1e-9, ymax=0.0, ymin=-10, label="Hybrid-Step Direct", color=cmap(5), linewidth=2
)
plt.vlines(
    pixel_4_direct_hybrid_kid_f0 * 1e-9, ymax=0.0, ymin=-10, label="Hybrid-No Step Direct", color=cmap(6), linewidth=2
)
plt.vlines(
    pixel_4_direct_kid_f0 * 1e-9, ymax=0.0, ymin=-10, label="Direct", color=cmap(7), linewidth=2
)
plt.xlabel("Resonator F0 Schedule (GHz)", fontsize=16)
plt.ylabel("Arbitrary Amplitude", fontsize=16)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.legend()
plt.show()

library.add(main_cell)
library.write_gds("mask_files\\" + chip_unique_id + "_" + fab_facility + "_mask.gds")
