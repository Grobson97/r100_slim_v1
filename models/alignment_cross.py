import gdspy
import numpy as np


class AlignmentCross:
    def __init__(
        self, centre=(0.0, 0.0), cross_size=(300.0, 5.0), box_size=(1000.0, 20.0)
    ) -> None:
        """
        Creates a new of an alignment cross, typically used for the MLA.

        :param centre: Coordinates for the centre of the cross.
        :param cross_size: Size of the cross defined as (Width, Thickness) in um.
        :param box_size: Size of the box defined as (Outer edge width, Thickness) in um.
        """

        self.centre = centre
        self.cross_size = cross_size
        self.box_size = box_size

    def make_cell(
        self,
        layer: int,
        cell_name="Alignment",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given BongPad instance.

        :param layer: GDSII layer for the alignment mark.
        :param cell_name: Name to be given to the Cell.
        """

        box_width = self.box_size[0]
        box_thickness = self.box_size[1]

        alignment_cell = gdspy.Cell(cell_name)

        outer_edge = gdspy.Rectangle(
            (self.centre[0] - box_width / 2, self.centre[1] + box_width / 2),
            (self.centre[0] + box_width / 2, self.centre[1] - box_width / 2),
            layer=layer,
        )
        inner_edge = gdspy.Rectangle(
            (
                self.centre[0] - box_width / 2 + box_thickness,
                self.centre[1] + box_width / 2 - box_thickness,
            ),
            (
                self.centre[0] + box_width / 2 - box_thickness,
                self.centre[1] - box_width / 2 + box_thickness,
            ),
            layer=layer,
        )

        cross1 = gdspy.Rectangle(
            (
                self.centre[0] - self.cross_size[0] / 2,
                self.centre[1] + self.cross_size[1] / 2,
            ),
            (
                self.centre[0] + self.cross_size[0] / 2,
                self.centre[1] - self.cross_size[1] / 2,
            ),
            layer=layer,
        )
        cross2 = gdspy.Rectangle(
            (
                self.centre[0] - self.cross_size[1] / 2,
                self.centre[1] + self.cross_size[0] / 2,
            ),
            (
                self.centre[0] + self.cross_size[1] / 2,
                self.centre[1] - self.cross_size[0] / 2,
            ),
            layer=layer,
        )

        alignment_cell.add(
            [gdspy.boolean(outer_edge, inner_edge, "not", layer=layer), cross1, cross2]
        )
        return alignment_cell
