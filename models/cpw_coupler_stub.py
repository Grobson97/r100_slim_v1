import gdspy
from gdspy.library import Cell
import numpy as np


def is_even(number: int):
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


class CPWCouplerStub:
    def __init__(
        self,
        cpw_centre_width: float,
        cpw_gap: float,
        coupler_length: float,
        readout_connection_x=0.0,
        readout_connection_y=0.0,
    ) -> None:
        """
        Creates new instance of a cpw stub to mimic the stub coupler of a LEKID, including a section of cpw with a
        bridge.

        :param cpw_centre_width: Width of CPW centre line.
        :param cpw_gap: gap width of cpw.
        :param coupler_length: Length of coupler fingers
        :param readout_connection_x: X coordinate of center of readout connection. Default= 0.0.
        :param readout_connection_y: Y coordinate of center of readout connection. Default= 0.0.
        """

        self.cpw_centre_width = cpw_centre_width
        self.cpw_gap = cpw_gap
        self.coupler_length = coupler_length
        self.readout_connection_x = readout_connection_x
        self.readout_connection_y = readout_connection_y

    def make_cell(
        self,
        cpw_center_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="CPW Coupler Stub",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given cpw stub instance. The geometry is
        oriented such that the stub is perpendicular to the x axis
        and the coupler hangs down from the cpw.

        :param int cpw_center_layer: GDSII layer for the readout feedline.
        :param ground_layer: GDSII layer for ground plane.
        :param dielectric_layer: GDSII layer for dielectric plane.
        :param cell_name: Name to be given to the cell.
        """

        cpw_centre_width = self.cpw_centre_width
        cpw_gap = self.cpw_gap
        coupler_length = self.coupler_length
        readout_connection_x = self.readout_connection_x
        readout_connection_y = self.readout_connection_y

        finger_width = 3.0
        finger_gap = 8.0
        stub_width = 6.0
        stub_length = 74.0
        rail_width = 6.0

        # Create cell to add coupler geometry to
        coupler_stub_cell = gdspy.Cell(cell_name)

        stub = gdspy.Rectangle(
            (
                readout_connection_x + 80.0 - stub_width / 2,
                readout_connection_y - cpw_centre_width / 2,
            ),
            (
                readout_connection_x + 80 + stub_width / 2,
                readout_connection_y - cpw_centre_width / 2 - stub_length,
            ),
            layer=cpw_center_layer,
        )

        readout_path_points = [
            (
                readout_connection_x,
                readout_connection_y,
            ),
            (
                readout_connection_x + 140,
                readout_connection_y,
            ),
        ]

        readout_centre_path = gdspy.FlexPath(
            points=readout_path_points,
            width=cpw_centre_width,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=cpw_center_layer,
        )

        readout_gap_path = gdspy.FlexPath(
            points=readout_path_points,
            width=cpw_centre_width + 2 * cpw_gap,
            offset=0.0,
            corners="circular bend",
            bend_radius=200,
            max_points=1999,
            layer=ground_layer,
        )

        bridge = gdspy.Rectangle(
            (
                readout_connection_x + 20,
                readout_connection_y + cpw_gap + cpw_centre_width / 2,
            ),
            (
                readout_connection_x + 26,
                readout_connection_y - cpw_gap - cpw_centre_width / 2,
            ),
            layer=10,
        )

        readout_gap_path = gdspy.boolean(
            operand1=readout_gap_path,
            operand2=bridge,
            operation="not",
            layer=ground_layer,
        )

        coupler_stub_cell.add([stub, readout_centre_path, readout_gap_path])

        ################################################################################################################

        # Section to build coupler fingers and rails.

        idc_coupler_finger_cell = gdspy.Cell(cell_name + "IDC Coupler Finger")
        idc_coupler_finger = gdspy.Rectangle(
            (-finger_width / 2, 0.0),
            (finger_width / 2, coupler_length),
            layer=cpw_center_layer,
        )
        idc_coupler_finger_cell.add(idc_coupler_finger)

        readout_coupler_finger_cell = gdspy.Cell(cell_name + "Readout Coupler Finger")
        readout_coupler_finger = gdspy.Rectangle(
            (-finger_width / 2, 0.0),
            (finger_width / 2, coupler_length),
            layer=cpw_center_layer,
        )
        readout_coupler_finger_cell.add(readout_coupler_finger)

        for n in range(4):
            coupler_finger_reference = gdspy.CellReference(
                idc_coupler_finger_cell,
                origin=(
                    (
                        readout_connection_x
                        + 80.0
                        + stub_width / 2
                        - finger_width / 2
                        - n * (finger_width + finger_gap)
                    ),
                    (
                        readout_connection_y
                        - cpw_centre_width / 2
                        - stub_length
                        - rail_width
                        - coupler_length
                    ),
                ),
            )
            if is_even(n):
                coupler_finger_reference = gdspy.CellReference(
                    readout_coupler_finger_cell,
                    origin=(
                        (
                            readout_connection_x
                            + 80.0
                            + stub_width / 2
                            - finger_width / 2
                            - n * (finger_width + finger_gap)
                        ),
                        (
                            readout_connection_y
                            - cpw_centre_width / 2
                            - stub_length
                            - rail_width
                            - coupler_length
                            - finger_gap
                        ),
                    ),
                )

            coupler_stub_cell.add(coupler_finger_reference)

        ########################################################################################################################

        # Add rails:

        top_rail_1x = (
            readout_connection_x
            + 80.0
            + stub_width / 2
            - 4 * finger_width
            - 3 * finger_gap
        )
        top_rail_1y = readout_connection_y - cpw_centre_width / 2 - stub_length

        top_rail_2x = readout_connection_x + 80.0 + stub_width / 2
        top_rail_2y = (
            readout_connection_y - cpw_centre_width / 2 - stub_length - rail_width
        )

        top_rail = gdspy.Rectangle(
            (top_rail_1x, top_rail_1y),
            (top_rail_2x, top_rail_2y),
            layer=cpw_center_layer,
        )
        bottom_rail = gdspy.Rectangle(
            (top_rail_1x, top_rail_1y - rail_width - coupler_length - finger_gap),
            (top_rail_2x, top_rail_2y - rail_width - coupler_length - finger_gap),
            layer=cpw_center_layer,
        )

        ground_hole = gdspy.Rectangle(
            (
                readout_connection_x + 17,
                readout_connection_y - cpw_centre_width / 2 - cpw_gap - 36,
            ),
            (
                readout_connection_x + 114,
                readout_connection_y
                - cpw_centre_width / 2
                - cpw_gap
                - 112
                - coupler_length,
            ),
            layer=ground_layer,
        )

        dielectric_hole = gdspy.Rectangle(
            (
                readout_connection_x + 27,
                readout_connection_y - cpw_centre_width / 2 - cpw_gap - 46,
            ),
            (
                readout_connection_x + 104,
                readout_connection_y
                - cpw_centre_width / 2
                - cpw_gap
                - 102
                - coupler_length,
            ),
            layer=dielectric_layer,
        )

        coupler_stub_cell.add([top_rail, bottom_rail, ground_hole, dielectric_hole])

        return coupler_stub_cell

    def get_readout_output_connection_x(self) -> float:
        """
        Function to return the x-coordinate of readout output line. in the default orientation this is the right hand
        connection (port).
        """

        return self.readout_connection_x + 140.0

    def get_readout_output_connection_y(self) -> float:
        """
        Function to return the y-coordinate of readout output line. in the default orientation this is the right hand
        connection (port).
        """

        return self.readout_connection_y
