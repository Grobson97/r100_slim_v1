import gdspy
from models.lekid_coupler import LekidCoupler
from models.idc import IDC
from models.inductor import Inductor


class CustomLEKID:
    def __init__(
        self,
        coupler_length: float,
        idc_length: float,
        idc_finger_number: int,
        signal_input_x=0.0,
        signal_input_y=0.0,
    ) -> None:
        """
        Creates new instance of a custom single Lekid with an interdigital capacitor
        using the modules for the elbow coupler, inductor and idc.

        :param float coupler_length: Length of the coupler fingers.
        :param float idc_length: Length of the idc fingers.
        :param int idc_finger_number: Total number of IDC fingers.
        :param readout_connection_x: X-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        :param readout_connection_y: Y-coordinate of the connection to whatever
        is delivering the signal to the Lekid inductor.
        """

        self.coupler_length = coupler_length
        self.idc_length = idc_length
        self.idc_finger_number = idc_finger_number

        self.signal_input_x = signal_input_x
        self.signal_input_y = signal_input_y

        self.inductor = Inductor(
            track_width=2.0,
            separation=12.0,
            inductor_width=514.0,
            idc_end_length=52.0,
            midpoint_end_length=62,
            bend_radius=6.0,
            u_bend_height=56.0,
            signal_input_connection_x=signal_input_x,
            signal_input_connection_y=signal_input_y,
            tolerance=0.01,
        )

        self.idc_fingers = IdcFingers(
            finger_number=idc_finger_number,
            finger_width=4.0,
            finger_gap=6.0,
            finger_length=self.idc_length,
        )

        self.idc = IDC(
            terminal_gap=8.0,
            terminal_width=8.0,
            terminal_finger_separation=16,
            idc_fingers=self.idc_fingers,
            inductor_connection_x=self.inductor.get_idc_connection_x(),
            inductor_connection_y=self.inductor.get_idc_connection_y(),
        )

        self.elbow_coupler = LekidCoupler(
            coupler_length=self.coupler_length,
            finger_width=4.0,
            finger_gap=6.0,
            readout_track_width=10.0,
            idc_track_width=8.0,
            length_to_idc=52,
            length_to_readout=80,
            idc_connection_x=self.idc.get_coupler_connection_x(),
            idc_connection_y=self.idc.get_coupler_connection_y(),
        )

    def make_cell(
        self,
        back_etch_layer: int,
        inductor_layer: int,
        idc_layer: int,
        readout_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        cell_name="Lekid",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Lekid geometry.
        The geometry is oriented such that coordinates of the connection to the
        readout line has the largest y value and the connection to the signal input
        the lowest.

        :param: int back_etch_layer: GDSII layer for back etch geometry.
        :param: int inductor_layer: GDSII layer for inductor geometry.
        :param: int idc_layer: GDSII layer for IDC geometry.
        :param: int readout_layer: GDSII layer for readout line geometry.
        :param: int ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        :param: int dielectric_layer: GDSII layer for the dielectric layer.
        :param: str cell_name: Name to be used to reference the cell. If creating
        multiple differing LEKIDS give cell names as LEKID_N where N is a unique
        number.
        """

        # Create Lekid cell to add module cells to.
        lekid_cell = gdspy.Cell(cell_name)

        if cell_name != "Lekid":
            cell_ID = cell_name.split("_")[1]
        if cell_name == "Lekid":
            cell_ID = "0"

        # Make gdspy cells:
        inductor_cell = self.inductor.make_cell(
            inductor_layer, cell_name="Inductor_" + cell_ID
        )
        idc_cell = self.idc.make_cell(
            back_etch_layer=back_etch_layer,
            idc_layer=idc_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            cell_name="IDC_" + cell_ID,
        )
        elbow_coupler_cell = self.elbow_coupler.make_cell(
            readout_line_layer=readout_layer,
            idc_layer=idc_layer,
            cell_name="Elbow_Coupler_" + cell_ID,
        )

        # Create reference cells of each sub-structure.
        inductor_ref_cell = gdspy.CellReference(inductor_cell, (0, 0))
        idc_ref_cell = gdspy.CellReference(idc_cell, (0, 0))
        elbow_coupler_ref_cell = gdspy.CellReference(elbow_coupler_cell, (0, 0))

        # Add pull back in ground plane over the indcutor to idc and feedline to coupler connections:
        pull_back1 = gdspy.Rectangle(
            (
                self.elbow_coupler.get_readout_connection_x() - 50,
                self.elbow_coupler.get_readout_connection_y() - 58,
            ),
            (
                self.elbow_coupler.get_readout_connection_x() + 50,
                self.elbow_coupler.get_readout_connection_y() - 48,
            ),
            layer=ground_layer,
        )
        pull_back2 = gdspy.Rectangle(
            (
                self.inductor.get_idc_connection_x() - 50,
                self.inductor.get_idc_connection_y() - 24,
            ),
            (
                self.inductor.get_idc_connection_x() + 50,
                self.inductor.get_idc_connection_y() - 34,
            ),
            layer=ground_layer,
        )

        # Add reference cells to single_filter_cell.
        lekid_cell.add(
            [
                inductor_ref_cell,
                idc_ref_cell,
                elbow_coupler_ref_cell,
                pull_back1,
                pull_back2,
            ]
        )

        return lekid_cell

    def get_readout_connection_x(self) -> float:
        """
        Function to return the x coordinate of the lekid_a connection to the readout line.
        """

        return self.elbow_coupler.get_readout_connection_x()

    def get_readout_connection_y(self) -> float:
        """
        Function to return the y coordinate of the lekid_a connection to the readout line.
        """

        return self.elbow_coupler.get_readout_connection_y()
