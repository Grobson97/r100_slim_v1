import gdspy
import numpy as np
from models.omt import OMT
from models.microstrip_step_over import MicrostripStepOver
from models.hybrid_180 import Hybrid180


class DualPolSimpleOMT:
    def __init__(
        self,
        diameter: float,
        choke_width: float,
        outer_path_radius: float,
        dual_polarisation=True,
        centre_x=0.0,
        centre_y=0.0,
    ) -> None:
        """
        Creates a new instance of an orthomode transducer antenna. NB: All
        dimensions must be in micrometers.

        :param: diameter: Diameter of the OMT. The same diameter as the end of
        the feed horn.
        :param: choke_width: Distance between the inner and outer diameter of
        the choke.
        :param outer_path_radius: Value to use to move hybrid and step over origins further or closer to centre of OMT.
        It should be a little bigger than the diameter.
        :param dual_polarisation: Boolean to make two orthogonal polarisation probe pairs when True.
        :param: centre_x: x-coordinate of the centre of the OMT.
        :param: centre_y: x-coordinate of the centre of the OMT.
        """

        self.diameter = diameter
        self.choke_width = choke_width
        self.centre_x = centre_x
        self.centre_y = centre_y
        self.outer_path_radius = outer_path_radius
        self.dual_polarisation = dual_polarisation
        self.microstrip_width = 2.0

        self.omt = OMT(
            diameter=self.diameter,
            choke_width=self.choke_width,
            dual_polarisation=dual_polarisation,
            output_bend_angle=0.0,
            centre_x=0.0,
            centre_y=0.0,
        )

        self.omt_connections = self.omt.get_feedline_connections()

    def make_cell(
        self,
        microstrip_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        back_etch_layer: int,
        cell_name="Dual Pol Simple OMT",
        tolerance=0.01,
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Dual pol OMT instance.

        :param microstrip_layer: GDSII layer for the antenna paddles.
        :param ground_layer: GDSII layer for the ground plane.
        :param dielectric_layer: GDSII layer for the dielectric.
        :param back_etch_layer: GDSII layer for the etching of the bulk wafer.
        :param cell_name: Name to be used to reference the cell.
        :param tolerance: Approximate curvature resolution. The number of
        points is automatically calculated.
        """

        radius = self.diameter / 2
        center_x = self.centre_x
        center_y = self.centre_y

        microstrip_width = 2.0

        full_omt_cell = gdspy.Cell(cell_name)

        # Add omt component.
        omt_cell = self.omt.make_cell(
            antenna_layer=microstrip_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            back_etch_layer=back_etch_layer,
            cell_name=cell_name + " - OMT",
        )

        ########################################################################################################################

        # Section to add feedlines from OMT to step over:

        north_path_points_1 = [
            (self.omt_connections[0][0], self.omt_connections[0][1]),
            (
                self.omt_connections[0][0]
                + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
                self.omt_connections[0][1]
                + 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
            ),
        ]
        north_path_1 = gdspy.FlexPath(
            points=north_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        if self.dual_polarisation:
            east_path_points_1 = [
                (self.omt_connections[2][0], self.omt_connections[2][1]),
                (
                    self.omt_connections[2][0]
                    + 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
                    self.omt_connections[2][1]
                    + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
                ),
            ]
            east_path_1 = gdspy.FlexPath(
                points=east_path_points_1,
                width=[microstrip_width, 100.0],
                corners="circular bend",
                bend_radius=100,
                layer=[microstrip_layer, 100],
            )

        south_path_points_1 = [
            (self.omt_connections[1][0], self.omt_connections[1][1]),
            (
                self.omt_connections[1][0]
                + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
                self.omt_connections[1][1]
                - 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
            ),
        ]
        south_path_1 = gdspy.FlexPath(
            points=south_path_points_1,
            width=[microstrip_width, 100.0],
            corners="circular bend",
            bend_radius=100,
            layer=[microstrip_layer, 100],
        )

        if self.dual_polarisation:
            west_path_points_1 = [
                (self.omt_connections[3][0], self.omt_connections[3][1]),
                (
                    self.omt_connections[3][0]
                    - 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
                    self.omt_connections[3][1]
                    + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
                ),
            ]
            west_path_1 = gdspy.FlexPath(
                points=west_path_points_1,
                width=[microstrip_width, 100.0],
                corners="circular bend",
                bend_radius=100,
                layer=[microstrip_layer, 100],
            )

        full_omt_cell.add(
            [
                gdspy.CellReference(omt_cell, origin=(center_x, center_y), rotation=0),
                north_path_1,
                south_path_1,
            ]
        )
        if self.dual_polarisation:
            full_omt_cell.add(
                [
                    east_path_1,
                    west_path_1,
                ]
            )

        dielectric_path_polygons = full_omt_cell.get_polygons(by_spec=(100, 0))
        dielectric_membrane_negative = full_omt_cell.get_polygons(by_spec=(99, 0))

        dielectric_membrane_negative = gdspy.boolean(
            dielectric_membrane_negative,
            dielectric_path_polygons,
            "not",
            layer=dielectric_layer,
        )

        full_omt_cell.add(dielectric_membrane_negative)
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 99)
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 100)

        return full_omt_cell

    def make_smiley_cell(
        self,
        microstrip_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        back_etch_layer: int,
        cell_name="Dual Pol Simple Smiley OMT",
        tolerance=0.01,
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Dual pol OMT instance.

        :param microstrip_layer: GDSII layer for the antenna paddles.
        :param ground_layer: GDSII layer for the ground plane.
        :param dielectric_layer: GDSII layer for the dielectric.
        :param back_etch_layer: GDSII layer for the etching of the bulk wafer.
        :param cell_name: Name to be used to reference the cell.
        :param tolerance: Approximate curvature resolution. The number of
        points is automatically calculated.
        """

        full_omt_cell = gdspy.Cell(cell_name)

        center_x = self.centre_x
        center_y = self.centre_y

        # Add omt component.
        omt_cell = self.omt.make_smiley_cell(
            antenna_layer=microstrip_layer,
            ground_layer=ground_layer,
            dielectric_layer=dielectric_layer,
            back_etch_layer=back_etch_layer,
            cell_name=cell_name + " - Smiley OMT",
        )

        full_omt_cell.add(
            gdspy.CellReference(omt_cell, origin=(center_x, center_y), rotation=0),
        )
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 99)
        full_omt_cell.remove_polygons(lambda pts, layer, datatype: layer == 100)

        return full_omt_cell

    def get_connections(self) -> list:
        """
        Function to return the x and y coordinates of the connection points to the difference ports of the north and
        eastern hybrid 180's. Outputs a list containing two tuples [(x1, y1), (x2, y2)] where x1 and y1 are the north
        connection and x2 and y2 are the coordinates for the eastern connection.
        """

        north_connection = (
            self.omt_connections[0][0]
            + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
            self.omt_connections[0][1]
            + 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
        )

        if self.dual_polarisation:
            east_connection = (
                self.omt_connections[2][0]
                + 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
                self.omt_connections[2][1]
                + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
            )

        south_connection = (
            self.omt_connections[1][0]
            + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
            self.omt_connections[1][1]
            - 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
        )

        if self.dual_polarisation:
            west_connection = (
                self.omt_connections[3][0]
                - 400 * np.cos((self.omt.output_bend_angle / 360) * 2 * np.pi),
                self.omt_connections[3][1]
                + 400 * np.sin((self.omt.output_bend_angle / 360) * 2 * np.pi),
            )
            return [
                north_connection,
                east_connection,
                south_connection,
                west_connection,
            ]

        else:
            return [north_connection, south_connection]
