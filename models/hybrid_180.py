import gdspy
from gdspy.library import Cell
import numpy as np


class Hybrid180:
    def __init__(
        self,
        resonator_length: float,
        loop_width: float,
        input_width: float,
        difference_port_width: float,
        sum_port_connection_x=0.0,
        sum_port_connection_y=0.0,
    ) -> None:
        """
        Creates new instance of a 180 degree hybrid power coupler. NB: All dimensions
        must be in micrometres.

        :param float resonator_length: Length of the resonant wavelength.
        :param float loop_width: Width of the microstrip making the loop/ rat race.
        :param input_width: Width of the input feedline being connected.
        :param difference_port_width: Width of the output feedline being connected.
        :param float sum_port_connection_x: X coordinate of the centre of the summation
        port connection. Also defines the smallest x coordinate of the loop. Default= 0.0.
        :param float sum_port_connection_y: Y coordinate of the centre of the summation
        port connection. Also defines the centre point of the loop between in two input
        ports. Default= 0.0.
        """

        self.resonator_length = resonator_length
        self.loop_width = loop_width
        self.input_width = input_width
        self.difference_port_width = difference_port_width
        self.sum_port_connection_x = sum_port_connection_x
        self.sum_port_connection_y = sum_port_connection_y

    def make_cell(self, microstrip_layer: int, cell_name="180 Hybrid") -> gdspy.Cell:
        """
        Returns the gdspy Cell for a Hybrid180 instance. The geometry is almost a
        square-like loop oriented such that the the summation port is located in
        the middle of the left hand side length, and the two input ports are on
        the top and bottom sides.

        :param int idc_layer: GDSII layer for the microstrip.
        :param str cell_name: Name to be used to reference the cell.
        """

        required_port_width = 3.0

        origin_x = self.sum_port_connection_x
        origin_y = self.sum_port_connection_y
        l = self.resonator_length
        w = self.loop_width
        bend_radius = self.loop_width * 3
        bend_length = (0.5 * np.pi * bend_radius) - (np.pi * w / 4)

        # Create cell to add coupler geometry to
        hybrid_cell = gdspy.Cell(cell_name)

        hybrid_path = gdspy.Path(
            width=self.loop_width,
            initial_point=(origin_x + w / 2, origin_y),
            number_of_paths=1,
        )

        vertical_length_sum_to_input = l / 4 - 20 - bend_length
        horizontal_length_input_to_difference = (
            l - 2 * vertical_length_sum_to_input - 2 * bend_length
        ) / 2

        # using fixed length of 20um in from the corner as the location of an input port.
        hybrid_path.segment(
            length=vertical_length_sum_to_input, direction="+y", layer=microstrip_layer
        )
        hybrid_path.turn(radius=bend_radius, angle="r", layer=microstrip_layer)
        hybrid_path.segment(length=20.0, direction="+x", layer=microstrip_layer)
        hybrid_path.segment(
            length=horizontal_length_input_to_difference,
            direction="+x",
            layer=microstrip_layer,
        )
        hybrid_path.turn(radius=bend_radius, angle="r", layer=microstrip_layer)
        hybrid_path.segment(
            length=2 * vertical_length_sum_to_input,
            direction="-y",
            layer=microstrip_layer,
        )
        hybrid_path.turn(radius=bend_radius, angle="r", layer=microstrip_layer)
        hybrid_path.segment(
            length=horizontal_length_input_to_difference,
            direction="-x",
            layer=microstrip_layer,
        )
        hybrid_path.segment(length=20.0, direction="-x", layer=microstrip_layer)
        hybrid_path.turn(radius=bend_radius, angle="r", layer=microstrip_layer)
        hybrid_path.segment(
            length=vertical_length_sum_to_input, direction="+y", layer=microstrip_layer
        )

        input_transition_1 = gdspy.Path(
            width=required_port_width,
            initial_point=(
                self.sum_port_connection_x + w / 2 + bend_radius + 20,
                (
                    self.sum_port_connection_y
                    + vertical_length_sum_to_input
                    + bend_radius
                    + w / 2
                ),
            ),
        )
        input_transition_1.segment(
            length=300,
            direction="+y",
            final_width=self.input_width,
            layer=microstrip_layer,
        )

        input_transition_2 = gdspy.Path(
            width=required_port_width,
            initial_point=(
                self.sum_port_connection_x + w / 2 + bend_radius + 20,
                (
                    self.sum_port_connection_y
                    - vertical_length_sum_to_input
                    - bend_radius
                    - w / 2
                ),
            ),
        )
        input_transition_2.segment(
            length=300,
            direction="-y",
            final_width=self.input_width,
            layer=microstrip_layer,
        )

        difference_transition = gdspy.Path(
            width=required_port_width,
            initial_point=(
                self.sum_port_connection_x + w / 2 + bend_radius + 20 + l / 4,
                (
                    self.sum_port_connection_y
                    - vertical_length_sum_to_input
                    - bend_radius
                    - w / 2
                ),
            ),
        )
        difference_transition.segment(
            length=300,
            direction="-y",
            final_width=self.difference_port_width,
            layer=microstrip_layer,
        )

        sum_port_overlap = gdspy.Polygon(
            [
                (0.0, -required_port_width / 2),
                (-15, -3 * required_port_width / 2),
                (-20.0, -3 * required_port_width / 2),
                (-20.0, 3 * required_port_width / 2),
                (-15, 3 * required_port_width / 2),
                (0.0, required_port_width / 2),
            ],
            layer=microstrip_layer,
        )

        # Add geometry to coupler cell.
        hybrid_cell.add(
            [
                hybrid_path,
                sum_port_overlap,
                input_transition_1,
                input_transition_2,
                difference_transition,
            ]
        )

        return hybrid_cell

    def get_input_connection_A(self) -> tuple:
        """
        Function to return the x and y coordinates of the input connection
        conventionally labelled as A. located on the top side l/4 from the sum
        port and 3l/4 from the difference port.
        Returns tuple as (x,y).
        """

        l = self.resonator_length
        w = self.loop_width
        bend_radius = self.loop_width * 3
        bend_length = (0.5 * np.pi * bend_radius) - (np.pi * w / 4)
        vertical_length_sum_to_input = l / 4 - 20 - bend_length

        x = self.sum_port_connection_x + w / 2 + bend_radius + 20
        y = (
            self.sum_port_connection_y
            + vertical_length_sum_to_input
            + bend_radius
            + w / 2
            + 300
        )

        return (x, y)

    def get_input_connection_B(self) -> tuple:
        """
        Function to return the x and y coordinates of the input connection
        conventionally labelled as B. located on the bottom side l/4 from the sum
        port and l/4 from the difference port.
        Returns tuple as (x,y).
        """

        l = self.resonator_length
        w = self.loop_width
        bend_radius = self.loop_width * 3
        bend_length = (0.5 * np.pi * bend_radius) - (np.pi * w / 4)
        vertical_length_sum_to_input = l / 4 - 20 - bend_length

        x = self.sum_port_connection_x + w / 2 + bend_radius + 20
        y = (
            self.sum_port_connection_y
            - vertical_length_sum_to_input
            - bend_radius
            - w / 2
            - 300
        )

        return (x, y)

    def get_difference_port_connection(self) -> tuple:
        """
        Function to return the x and y coordinates of the difference port
        connection. Located on the bottom side l/4 from input B and 3/4l from
        input A.
        Returns tuple as (x,y).
        """

        l = self.resonator_length
        w = self.loop_width
        bend_radius = self.loop_width * 3
        bend_length = (0.5 * np.pi * bend_radius) - (np.pi * w / 4)
        vertical_length_sum_to_input = l / 4 - 20 - bend_length
        horizontal_length_input_to_difference = (
            l - 2 * vertical_length_sum_to_input - 2 * bend_length
        ) / 2

        if horizontal_length_input_to_difference < l / 4:
            return print(
                "Different design required as difference port cannot go on the bottom edge."
            )

        x = self.sum_port_connection_x + w / 2 + bend_radius + 20 + l / 4
        y = (
            self.sum_port_connection_y
            - vertical_length_sum_to_input
            - bend_radius
            - w / 2
            - 300
        )

        return (x, y)
