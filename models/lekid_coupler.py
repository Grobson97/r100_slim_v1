import gdspy
from gdspy.library import Cell
import numpy as np


def is_even(number: int):
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


class LekidCoupler:
    def __init__(
        self,
        coupler_length: float,
        finger_width: float,
        finger_gap: float,
        readout_track_width: float,
        idc_track_width: float,
        length_to_idc: float,
        length_to_readout: float,
        idc_connection_x=0.0,
        idc_connection_y=0.0,
    ) -> None:
        """
        Creates new instance of 2 pair elbow coupler with 4 fingers (4F). NB: All dimensions
        must be in micrometres.

        :param float coupler_length: Length of the coupler fingers.
        :param float finger_width: Width of the coupler fingers.
        :param float finger_gap: Gap with between coupler fingers.
        :param float readout_track_width: Width of path to and terminal on readout side.
        :param float idc_track_width: Width of path and terminal on idc side.
        :param float length_to_idc: Distance from fingers section to start of IDC.
        :param float length_to_readout: Distance from fingers section to middle
        of feedline.
        :param float idc_connection_x: X coordinate of center of IDC join. Default= 0.0.
        :param float idc_connection_y: Y coordinate of center of IDC join. Default= 0.0.
        """

        self.coupler_length = coupler_length
        self.finger_width = finger_width
        self.finger_gap = finger_gap
        self.readout_track_width = readout_track_width
        self.idc_track_width = idc_track_width
        self.length_to_idc = length_to_idc
        self.length_to_readout = length_to_readout
        self.idc_connection_x = idc_connection_x
        self.idc_connection_y = idc_connection_y

    def make_cell(
        self,
        readout_line_layer: int,
        idc_layer: int,
        ground_layer: int,
        dielectric_layer: int,
        nitride_step_down_layer: int,
        oxide_step_down_layer: int,
        e_beam_boundary_layer: int,
        include_step_down=False,
        cell_name="Lekid Coupler",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Coupler instance. The geometry is
        oriented such that the coupler fingers are perpendicular to the x axis
        and the coupler connects to the idc on the bottom and feedline on the
        top.

        :param int readout_line_layer: GDSII layer for the readout feedline.
        :param idc_layer: GDSII layer for IDC geometry.
        :param ground_layer: GDSII layer for ground plane.
        :param dielectric_layer: GDSII layer for dielectric layer.
        :param nitride_step_down_layer: GDSII layer for the nitride step down layer.
        :param oxide_step_down_layer: GDSII layer for the oxide step down layer.
        :param e_beam_boundary_layer: GDSII layer for e-beam boundary layer.
        :param include_step_down: Boolean to include stepdown holes in geometry. Default is no.
        :param cell_name: Name to be given to the cell.
        """

        coupler_length = self.coupler_length
        finger_width = self.finger_width
        finger_gap = self.finger_gap
        readout_track_width = self.readout_track_width
        idc_track_width = self.idc_track_width
        length_to_idc = self.length_to_idc
        length_to_readout = self.length_to_readout
        idc_connection_x = self.idc_connection_x
        idc_connection_y = self.idc_connection_y

        # Create cell to add coupler geometry to
        coupler_cell = gdspy.Cell(cell_name)

        ################################################################################################################
        # Section to build coupler fingers and rails.

        idc_coupler_finger_cell = gdspy.Cell(cell_name + "IDC Coupler Finger")
        idc_coupler_finger = gdspy.Rectangle(
            (-finger_width / 2, 0.0),
            (finger_width / 2, coupler_length),
            layer=idc_layer,
        )
        idc_coupler_finger_cell.add(idc_coupler_finger)

        readout_coupler_finger_cell = gdspy.Cell(cell_name + "Readout Coupler Finger")
        readout_coupler_finger = gdspy.Rectangle(
            (-finger_width / 2, 0.0),
            (finger_width / 2, coupler_length),
            layer=readout_line_layer,
        )
        readout_coupler_finger_cell.add(readout_coupler_finger)

        for n in range(4):
            coupler_finger_reference = gdspy.CellReference(
                idc_coupler_finger_cell,
                origin=(
                    (
                        idc_connection_x
                        - idc_track_width / 2
                        + finger_width / 2
                        + n * (finger_width + finger_gap)
                    ),
                    (idc_connection_y + (length_to_idc + idc_track_width)),
                ),
            )
            if is_even(n):
                coupler_finger_reference = gdspy.CellReference(
                    readout_coupler_finger_cell,
                    origin=(
                        (
                            idc_connection_x
                            - idc_track_width / 2
                            + finger_width / 2
                            + n * (finger_width + finger_gap)
                        ),
                        (
                            idc_connection_y
                            + (length_to_idc + idc_track_width)
                            + finger_gap
                        ),
                    ),
                )

            coupler_cell.add(coupler_finger_reference)

        idc_coupler_rail = gdspy.FlexPath(
            points=[
                (idc_connection_x, idc_connection_y),
                (
                    idc_connection_x,
                    idc_connection_y + length_to_idc + idc_track_width / 2,
                ),
                (
                    (
                        idc_connection_x
                        - (idc_track_width / 2)
                        + 4 * finger_width
                        + 3 * finger_gap
                    ),
                    (idc_connection_y + length_to_idc + idc_track_width / 2),
                ),
            ],
            width=idc_track_width,
            ends="flush",
            corners="natural",
            layer=idc_layer,
        )

        readout_coupler_rail = gdspy.FlexPath(
            points=[
                (
                    (idc_connection_x - (idc_track_width / 2)),
                    (
                        idc_connection_y
                        + (length_to_idc + idc_track_width)
                        + finger_gap
                        + coupler_length
                        + readout_track_width / 2
                    ),
                ),
                (
                    (
                        idc_connection_x
                        - (idc_track_width / 2)
                        + 4 * finger_width
                        + 3 * finger_gap
                    ),
                    (
                        idc_connection_y
                        + (length_to_idc + idc_track_width)
                        + finger_gap
                        + coupler_length
                        + readout_track_width / 2
                    ),
                ),
                (
                    (
                        idc_connection_x
                        - (idc_track_width / 2)
                        + 4 * finger_width
                        + 3 * finger_gap
                    ),
                    (
                        idc_connection_y
                        + (length_to_idc + idc_track_width)
                        + finger_gap
                        + coupler_length
                        + readout_track_width
                        + length_to_readout
                    ),
                ),
            ],
            width=idc_track_width,
            ends="flush",
            corners="natural",
            layer=readout_line_layer,
        )

        ################################################################################################################
        # Section to build ground, dielectric and back_etch holes

        # Define vertices of the ground, dielectric and back_etch holes
        hole_vertices = [
            (
                (idc_connection_x - idc_track_width / 2 - 30),
                idc_connection_y,
            ),
            (
                (idc_connection_x - idc_track_width / 2 + 191),
                idc_connection_y,
            ),
            (
                (idc_connection_x - idc_track_width / 2 + 191),
                (idc_connection_y + length_to_idc),
            ),
            (
                (
                    idc_connection_x
                    + idc_track_width / 2
                    + 4 * finger_width
                    + 3 * finger_gap
                    + 30
                ),
                (idc_connection_y + length_to_idc),
            ),
            (
                (
                    idc_connection_x
                    + idc_track_width / 2
                    + 4 * finger_width
                    + 3 * finger_gap
                    + 30
                ),
                (
                    idc_connection_y
                    + length_to_idc
                    + idc_track_width
                    + coupler_length
                    + finger_gap
                    + 34
                ),
            ),
            (
                (idc_connection_x - idc_track_width / 2 - 30),
                (
                    idc_connection_y
                    + length_to_idc
                    + idc_track_width
                    + coupler_length
                    + finger_gap
                    + 34
                ),
            ),
        ]

        # logic to bring dielectric hole in by 5um form gnd hole.
        dielectric_hole_vertices = []
        for vertex in hole_vertices:
            dx = 5
            dy = -5
            if vertex[0] > idc_connection_x:
                dx = -5
            if vertex[1] == idc_connection_y:
                dy = 0
            dielectric_hole_vertices.append((vertex[0] + dx, vertex[1] + dy))

        # logic to bring step down hole in by 15um form gnd hole.
        nitride_step_down_hole_vertices = []
        for vertex in hole_vertices:
            dx = 15
            dy = -15
            if vertex[0] > idc_connection_x:
                dx = -15
            if vertex[1] == idc_connection_y:
                dy = 0
            nitride_step_down_hole_vertices.append((vertex[0] + dx, vertex[1] + dy))

        # logic to bring step down hole in by 15um form gnd hole.
        oxide_step_down_hole_vertices = []
        for vertex in hole_vertices:
            dx = 24
            dy = -24
            if vertex[0] > idc_connection_x:
                dx = -24
            if vertex[1] == idc_connection_y:
                dy = 0
            oxide_step_down_hole_vertices.append((vertex[0] + dx, vertex[1] + dy))

        # Add negative hole in ground layer:
        coupler_gnd_hole = gdspy.Polygon(
            points=hole_vertices,
            layer=ground_layer,
        )
        # Add negative hole in dielectric layer:
        coupler_dielectric_hole = gdspy.Polygon(
            points=dielectric_hole_vertices,
            layer=dielectric_layer,
        )

        # Add negative hole in nitride step down layer:
        nitride_step_down_hole = gdspy.Polygon(
            points=nitride_step_down_hole_vertices,
            layer=nitride_step_down_layer,
        )

        # Add negative hole in oxide step down layer:
        oxide_step_down_hole = gdspy.Polygon(
            points=oxide_step_down_hole_vertices,
            layer=oxide_step_down_layer,
        )

        # Add negative hole for e-beam boundary:
        e_beam_boundary_1 = gdspy.Rectangle(
            (idc_connection_x - idc_track_width / 2 - 4, idc_connection_y + 5),
            (
                idc_connection_x
                - idc_track_width / 2
                + finger_width * 4
                + finger_gap * 3
                + (finger_gap / 2),
                idc_connection_y + length_to_idc + idc_track_width + (finger_gap / 2),
            ),
            layer=e_beam_boundary_layer,
        )
        e_beam_boundary_2 = gdspy.Rectangle(
            (
                idc_connection_x
                - idc_track_width / 2
                + finger_width
                + (finger_gap / 2),
                idc_connection_y + length_to_idc + idc_track_width + (finger_gap / 2),
            ),
            (
                idc_connection_x - idc_track_width / 2 + 2 * finger_width + 12,
                idc_connection_y
                + length_to_idc
                + idc_track_width
                + coupler_length
                + (finger_gap / 2),
            ),
            layer=e_beam_boundary_layer,
        )
        e_beam_boundary_3 = gdspy.Rectangle(
            (
                idc_connection_x
                - idc_track_width / 2
                + finger_width * 3
                + 5 * (finger_gap / 2),
                idc_connection_y + length_to_idc + idc_track_width + (finger_gap / 2),
            ),
            (
                idc_connection_x
                - idc_track_width / 2
                + finger_width * 4
                + 7 * (finger_gap / 2),
                idc_connection_y
                + length_to_idc
                + idc_track_width
                + coupler_length
                + (finger_gap / 2),
            ),
            layer=e_beam_boundary_layer,
        )

        coupler_cell.add(
            [
                idc_coupler_rail,
                readout_coupler_rail,
                coupler_gnd_hole,
                coupler_dielectric_hole,
                e_beam_boundary_1,
                e_beam_boundary_2,
                e_beam_boundary_3,
            ]
        )

        if include_step_down:
            coupler_cell.add([nitride_step_down_hole, oxide_step_down_hole])

        return coupler_cell

    def get_readout_connection_x(self):
        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        readout_connection_x = (
            self.idc_connection_x
            - (self.idc_track_width / 2)
            + 4 * self.finger_width
            + 3 * self.finger_gap
        )

        return readout_connection_x

    def get_readout_connection_y(self):
        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        readout_connection_y = (
            self.idc_connection_y
            + (self.length_to_idc + self.idc_track_width)
            + self.finger_gap
            + self.coupler_length
            + self.readout_track_width
            + self.length_to_readout
        )

        return readout_connection_y
