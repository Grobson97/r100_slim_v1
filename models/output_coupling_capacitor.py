import gdspy
import numpy as np


class OutputCouplingCapacitor:
    def __init__(
        self,
        cap_out: float,
        resonator_connection_x=0.0,
        resonator_connection_y=0.0,
    ) -> None:
        """
        Creates a new instance of an output coupling capacitor. NB: All dimensions
        must be in micrometres.

        param: cap_out: Width of the variable plate capacitor in the coupler.
        param: resonator_connection_x: x-coordinate of the connection to the
        resonator.
        param: resonator_connection_y: y-coordinate of the connection to the
        resonator.
        """

        self.cap_out = cap_out
        self.resonator_connection_x = resonator_connection_x
        self.resonator_connection_y = resonator_connection_y

    def make_cell(
        self, microstrip_layer: int, ground_layer: int, cell_name="Output Coupler"
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given output coupling capacitor instance.
        The geometry is oriented such that coordinates of the connection to the
        resonator has the largest y value and the connection to the output
        the lowest.

        param: cpw_centre_layer: GDSII layer for microstirp geometry.
        param: ground_layer: GDSII layer for ground plane capacitor islands
        geometry.
        param: cell_name: Name to be used to reference the cell.

        """
        cap_out = self.cap_out
        origin_x = self.resonator_connection_x
        origin_y = self.resonator_connection_y

        # Define width of output microstrip.
        width = 3.0

        # Create resonator cell to add module cells to
        output_coupler_cell = gdspy.Cell(cell_name)

        # Build capacitor paddle geometries.
        fixed_cap_geometry = gdspy.Rectangle(
            (origin_x - cap_out / 2.0, origin_y),
            (origin_x + cap_out / 2.0, origin_y - 2.0),
            layer=microstrip_layer,
        )
        cap_out_geometry = gdspy.Rectangle(
            (origin_x - (cap_out / 2.0), origin_y - 6.0),
            (origin_x + cap_out / 2.0, origin_y - 8.0),
            layer=microstrip_layer,
        )

        ground_capacitor_negative1 = gdspy.Rectangle(
            (origin_x - (cap_out / 2.0) - 1.0, origin_y + 4.0),
            (origin_x + (cap_out / 2.0) + 3.0, origin_y + 2.0),
            layer=ground_layer,
        )
        ground_capacitor_negative2 = gdspy.Rectangle(
            (origin_x + (cap_out / 2.0) + 1.0, origin_y + 2.0),
            (origin_x + (cap_out / 2.0) + 3.0, origin_y - 12.0),
            layer=ground_layer,
        )
        ground_capacitor_negative3 = gdspy.Rectangle(
            (origin_x - (cap_out / 2.0) - 3.0, origin_y - 10.0),
            (origin_x + (cap_out / 2.0) + 1.0, origin_y - 12.0),
            layer=ground_layer,
        )
        ground_capacitor_negative4 = gdspy.Rectangle(
            (origin_x - (cap_out / 2.0) - 3.0, origin_y + 4.0),
            (origin_x - (cap_out / 2.0) - 1.0, origin_y - 10.0),
            layer=ground_layer,
        )

        output_path = gdspy.Path(width=width, initial_point=(origin_x, origin_y - 8.0))
        # Add a segment to the path going in the '-y' direction.
        output_path.segment(length=19.25, direction="-y", layer=microstrip_layer)

        # Add the all the geometries to the cell.
        output_coupler_cell.add(output_path)
        output_coupler_cell.add(cap_out_geometry)
        output_coupler_cell.add(fixed_cap_geometry)
        output_coupler_cell.add(ground_capacitor_negative1)
        output_coupler_cell.add(ground_capacitor_negative2)
        output_coupler_cell.add(ground_capacitor_negative3)
        output_coupler_cell.add(ground_capacitor_negative4)

        return output_coupler_cell

    def get_LEKID_connection_x(self) -> float:
        """
        Function to return the x-coordinate of the connection point to the
        inductor section of the Lekid. NB: this will always be the same as the
        resonator connecion x-coordinate.
        """
        return self.resonator_connection_x

    def get_LEKID_connection_y(self) -> float:
        """
        Function to return the y-coordinate of the connection point to the
        inductor section of the Lekid..
        """
        return self.resonator_connection_y - 27.25
