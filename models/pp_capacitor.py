import gdspy


class PPCapacitor:
    def __init__(
        self,
        capacitor_length: float,
        inductor_connection_x=0.0,
        inductor_connection_y=0.0,
    ) -> None:
        """
        Creates New instance of an IDC object.

        :param capacitor_length: Length of plate capacitors.
        :param inductor_connection_x: x coordinate of the IDC-inductor connection.
        :param inductor_connection_y: x coordinate of the IDC-inductor connection.
        """

        self.capacitor_length = capacitor_length
        self.inductor_connection_x = inductor_connection_x
        self.inductor_connection_y = inductor_connection_y

    def make_cell(
        self,
        plate_layer: int,
        ground_layer: int,
        cell_name="PP Capacitor",
    ):
        """
        Returns the gdspy Cell for a given Parallel plate capacitor instance. The geometry is
        oriented such that the coupler is at the top and inductor at the bottom. To add a
        rotation this must be done with the .CellReference() method after calling this method.

        :param plate_layer: GDSII layer for the two parallel plates.
        :param ground_layer: GDSII layer for ground plane plate.
        :param dielectric_layer: GDSII layer for dielectric layer.
        :param cell_name: Name to be given to the cell.
        """

        origin_x = self.inductor_connection_x
        origin_y = self.inductor_connection_y
        plate_width = 78.0
        plate_gap = 5.0
        ground_gap = 30.0

        # Create cell for complete IDC geometries to be added to.
        pp_capacitor_cell = gdspy.Cell(cell_name)

        ################################################################################################################
        # Section to build device layer plates:

        plate_1 = gdspy.Rectangle(
            (origin_x - plate_gap / 2, origin_y),
            (origin_x - plate_gap / 2 - plate_width, origin_y + self.capacitor_length),
            layer=plate_layer,
        )
        plate_2 = gdspy.Rectangle(
            (origin_x + plate_gap / 2, origin_y),
            (origin_x + plate_gap / 2 + plate_width, origin_y + self.capacitor_length),
            layer=plate_layer,
        )

        ground_hole = gdspy.Rectangle(
            (
                origin_x - plate_gap / 2 - plate_width - ground_gap,
                origin_y + self.capacitor_length + 3,
            ),
            (
                origin_x + plate_gap / 2 + plate_width + ground_gap,
                origin_y - ground_gap,
            ),
            layer=ground_layer,
        )
        ground_plate = gdspy.Rectangle(
            (
                origin_x - plate_gap / 2 - plate_width - 3,
                origin_y + self.capacitor_length + 3,
            ),
            (origin_x + plate_gap / 2 + plate_width + 3, origin_y - 3),
            layer=ground_layer,
        )

        ground_hole = gdspy.boolean(
            ground_hole, ground_plate, "not", layer=ground_layer
        )

        # Add remaining geometries to cell:
        pp_capacitor_cell.add([plate_1, plate_2, ground_hole])

        return pp_capacitor_cell

    def get_coupler_connection_x(self):
        """
        Function to return the x-coordinate of Capacitor-Coupler join, located at the top right corner of the left hand
        device plate.
        """
        plate_gap = 5.0

        return self.inductor_connection_x - plate_gap / 2

    def get_coupler_connection_y(self):
        """
        Function to return the y-coordinate of Capacitor-Coupler join, located at the top right corner of the left hand
        device plate.
        """

        return self.inductor_connection_y + self.capacitor_length
