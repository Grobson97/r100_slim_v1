import gdspy
from gdspy.library import Cell
import numpy as np


def is_even(number: int):
    if (number % 2) == 0:
        return True
    if (number % 2) != 0:
        return False


class PPLekidCoupler:
    def __init__(
        self,
        plate_width: float,
        plate_height: float,
        plate_gap: float,
        ground_hole_width: float,
        coupler_track_width: float,
        length_to_capacitor: float,
        length_to_readout: float,
        lekid_capacitor_connection_x=0.0,
        lekid_capacitor_connection_y=0.0,
    ) -> None:
        """
        Creates new instance parallel plate LEKID coupling capacitor

        :param float plate_width: Width of coupling capacitor plates.
        :param float plate_height: Height of coupling capacitor plates.
        :param float plate_gap: Gap between the two device layer coupling capacitors.
        :param ground_hole_width: Width of ground hole. Should match the width of the LEKID capacitor ground hole width.
        :param float coupler_track_width: Width of path and terminal on idc side.
        :param float length_to_capacitor: Distance from coupling plate capacitor to LEKID Capacitor.
        :param float length_to_readout: Distance from fingers section to middle
        of feedline.
        :param float lekid_capacitor_connection_x: X coordinate of center of IDC join. Default= 0.0.
        :param float lekid_capacitor_connection_y: Y coordinate of center of IDC join. Default= 0.0.
        """

        self.plate_width = plate_width
        self.plate_height = plate_height
        self.plate_gap = plate_gap
        self.ground_hole_width = ground_hole_width
        self.coupler_track_width = coupler_track_width
        self.length_to_capacitor = length_to_capacitor
        self.length_to_readout = length_to_readout
        self.lekid_capacitor_connection_x = lekid_capacitor_connection_x
        self.lekid_capacitor_connection_y = lekid_capacitor_connection_y

    def make_cell(
        self,
        plate_layer: int,
        ground_layer: int,
        cell_name="Lekid Coupler",
    ) -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given Coupler instance. The geometry is
        oriented such that the coupler fingers are perpendicular to the x axis
        and the coupler connects to the idc on the bottom and feedline on the
        top.

        :param int plate_layer: GDSII layer for the device layer parallel plates.
        :param ground_layer: GDSII layer for ground plane.
        :param cell_name: Name to be given to the cell.
        """

        plate_width = self.plate_width
        plate_height = self.plate_height
        plate_gap = self.plate_gap
        coupler_track_width = self.coupler_track_width
        length_to_capacitor = self.length_to_capacitor
        length_to_readout = self.length_to_readout
        origin_x = self.lekid_capacitor_connection_x
        origin_y = self.lekid_capacitor_connection_y

        lekid_capacitor_gap = 5.0

        # Create cell to add coupler geometry to
        pp_coupler_cell = gdspy.Cell(cell_name)

        capacitor_to_coupler_path = gdspy.Rectangle(
            (origin_x, origin_y),
            (origin_x - coupler_track_width, origin_y + length_to_capacitor),
            layer=plate_layer,
        )

        output_plate = gdspy.Rectangle(
            (
                origin_x - coupler_track_width / 2 - plate_width / 2,
                origin_y + length_to_capacitor,
            ),
            (
                origin_x - coupler_track_width / 2 + plate_width / 2,
                origin_y + length_to_capacitor + plate_height,
            ),
            layer=plate_layer,
        )

        input_plate = gdspy.Rectangle(
            (
                origin_x - coupler_track_width / 2 - plate_width / 2,
                origin_y + length_to_capacitor + plate_gap + plate_height,
            ),
            (
                origin_x - coupler_track_width / 2 + plate_width / 2,
                origin_y + length_to_capacitor + plate_gap + 2 * plate_height,
            ),
            layer=plate_layer,
        )

        coupler_to_readout_path = gdspy.Rectangle(
            (origin_x, origin_y + length_to_capacitor + plate_gap + 2 * plate_height),
            (
                origin_x - coupler_track_width,
                origin_y
                + length_to_capacitor
                + plate_gap
                + 2 * plate_height
                + length_to_readout,
            ),
            layer=plate_layer,
        )

        ground_hole = gdspy.Rectangle(
            (
                origin_x + lekid_capacitor_gap / 2 - self.ground_hole_width / 2,
                origin_y + length_to_capacitor + plate_gap + 2 * plate_height + 25,
            ),
            (
                origin_x + lekid_capacitor_gap / 2 + self.ground_hole_width / 2,
                origin_y + 3,
            ),
            layer=ground_layer,
        )

        ground_plate = gdspy.Rectangle(
            (
                origin_x - coupler_track_width / 2 + plate_width / 2 + 3,
                origin_y + length_to_capacitor + plate_gap + 2 * plate_height + 3,
            ),
            (
                origin_x - coupler_track_width / 2 - plate_width / 2 - 3,
                origin_y + length_to_capacitor - 3,
            ),
            layer=ground_layer,
        )

        ground_hole = gdspy.boolean(
            ground_hole, ground_plate, "not", layer=ground_layer
        )

        pp_coupler_cell.add(
            [
                capacitor_to_coupler_path,
                output_plate,
                input_plate,
                coupler_to_readout_path,
                ground_hole,
            ]
        )

        return pp_coupler_cell

    def get_readout_connection_x(self):
        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        return self.lekid_capacitor_connection_x - self.coupler_track_width / 2

    def get_readout_connection_y(self):
        """
        Function to return the x-coordinate of Coupler-Feedline join, located at
        the end of a track orthogonal to the coupler fingers in the centre of
        the track.
        """

        return (
            self.lekid_capacitor_connection_y
            + self.length_to_capacitor
            + self.plate_gap
            + 2 * self.plate_height
            + self.length_to_readout
        )
