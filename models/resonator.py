import gdspy
import numpy as np
from numpy import pi


class Resonator:
    def __init__(
        self,
        res_length: float,
        width=2.0,
        input_connection_x=0.0,
        input_connection_y=0.0,
    ) -> None:
        """
        Creates a new instance of a microstrip resonator. NB: All dimensions
        must be in micrometres.

        param: res_length: Length of resonator.
        param: width: Width of resonator microstrip.
        param: input_connection_x: x-coordinate of the connection to the input
        coupler.
        param: input_connection_y: y-coordinate of the connection to the input
        coupler.
        """

        self.res_length = res_length
        self.width = width
        self.input_connection_x = input_connection_x
        self.input_connection_y = input_connection_y

    def make_cell(self, layer: int, cell_name="Resonator") -> gdspy.Cell:
        """
        Returns the gdspy Cell for a given resonator instance. The geometry is
        oriented such that the connection to the input coupler has the highest
        y value and the connecction to the output coupler has the lowest value.

        param: layer: GDSII layer for resonator geometry.
        param: cell_name: Name to be used to reference the cell.
        """

        res_length = self.res_length
        width = self.width
        input_connection_x = self.input_connection_x
        input_connection_y = self.input_connection_y

        # Define bend radius as 3 times width of microstrip to be almost
        # indistinguishable from straigth line segment.
        bend_radius = 3 * width

        # Calculate the effective length of the bend.
        effective_bend_length = (bend_radius * pi / 2) - (pi * width / 4)

        # Define length of initial segment after coupler:
        a = 2.0

        # Calculate length of unit straight line section b.
        b = 0.5 * (res_length / 2 - a - bend_radius - 3 * effective_bend_length)

        # Create resonator cell to add module cells to
        resonator_cell = gdspy.Cell(cell_name)

        resonator_path = gdspy.Path(
            width=width, initial_point=(input_connection_x, input_connection_y)
        )

        # Add a segment to the path going in the '-y' direction
        resonator_path.segment(length=a, direction="-y", layer=layer)
        resonator_path.turn(radius=bend_radius, angle="l", layer=layer)
        resonator_path.segment(length=b, direction="+x", layer=layer)
        resonator_path.turn(radius=bend_radius, angle="rr", layer=layer)
        resonator_path.segment(
            length=2 * b + 2 * bend_radius, direction="-x", layer=layer
        )
        resonator_path.turn(radius=bend_radius, angle="ll", layer=layer)
        resonator_path.segment(length=b, direction="+x", layer=layer)
        resonator_path.turn(radius=bend_radius, angle="r", layer=layer)
        resonator_path.segment(length=a, direction="-y", layer=layer)

        # Add resonator path geometry to the resonator cell.
        resonator_cell.add(resonator_path)

        return resonator_cell

    def get_output_connection_x(self) -> float:
        """
        Function to return the x-coordinate of the connection point to the
        output coupler. NB: this will always be the same as the input
        x-coordinate.
        """
        return self.input_connection_x

    def get_output_connection_y(self) -> float:
        """
        Function to return the y-coordinate of the connection point to the
        output coupler.
        """
        width = self.width
        input_connection_y = self.input_connection_y
        bend_radius = 3 * width

        # Define length of initial segment after coupler:
        a = 2.0

        return input_connection_y - 2 * a - 6 * bend_radius
