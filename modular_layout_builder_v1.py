import gdspy
import numpy as np
import matplotlib.pyplot as plt
from math import radians
from math import degrees
import submodules.dice_lines
import util.filter_bank_builder_tools as tools
import util.spectrometer_builders
import util.general_gds_tools as general_gds_tools
from models.alignment_cross import AlignmentCross
from models.dual_pol_simple_omt import DualPolSimpleOMT
from models.edge_bond_pad import EdgeBondPad
from models.filter_bank import FilterBank
from models.dual_pol_omt import DualPolOMT
from models.alignment_caliper import AlignmentCaliper


########################################################################################################################

resolution = 100
filter_spacing = 0.35
number_of_pixels = 12
show_plots = True
design = "slim_r100"
version = "_v1"
extra_id = "_500MHz"
chip_unique_id = design + version + extra_id
fab_facility = "ANL"
library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = gdspy.Cell("Main")

# Create cell to add all devices to:
all_devices_cell = gdspy.Cell("All Devices")

# Define layer dictionary:
layers = {
    "e_beam_boundary": 0,
    "inductors": 1,
    "idc": 2,
    "readout_centre": 3,
    "dielectric": 4,
    "ground": 5,
    "wafer": 6,
    "dice": 7,
    "antennas": 8,
    "mm-feedline": 9,
    "back_etch": 10,
    "reduced_e-beam_boundary": 11,
    "nitride_step_down": 12,
    "oxide_step_down": 13,
    "capacitor_plates": 14,
    "aluminium": 15,
    "focal plane": 16,
    "pixel": 17,
    "dowel": 18,
    "pogo_pins": 19,
    "tabs": 20,
    "alignment": 21,
    "titanium_aluminium": 22,
    "fab_1_nitride_membrane_etch": 101,
    "fab_2_oxide_membrane_etch": 102,
    "fab_3_nb_wiring": 103,
    "fab_4_al_meanders": 104,
    "fab_5_microstrip_dielectric": 105,
    "fab_6_nb_ground": 106,
    "fab_7_drie_etch": 107,
    "fab_8_titanium_aluminium": 108,
}

# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0
readout_bridge_separation = 500.0
optimal_bridge_thickness = 18.864
dice_width = 300

six_inch_wafer = gdspy.Round(center=(0, 0), radius=75000, layer=layers["wafer"])

main_cell.add(six_inch_wafer)

slim_device_cell = gdspy.Cell("Slim Module")

########################################################################################################################

# Section to add OMT's

full_omts_cell = gdspy.Cell("OMT's")
omt_rotated_cell = gdspy.Cell("Single OMT")

omt_diameter = 6000
pixel_centre_to_centre = 9690

full_omt = DualPolOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    centre_x=0.0,
    centre_y=0.0,
)

omt_cell = full_omt.make_cell(
    microstrip_layer=layers["antennas"],
    ground_layer=layers["ground"],
    step_over_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
    lossy_metal_layer=layers["inductors"],
    cell_name="Full Dual Pol OMT",
)
omt_cell.add(
    gdspy.Round(center=(0, 0), radius=pixel_centre_to_centre / 2, layer=layers["pixel"])
)

smiley_omt_instance = DualPolSimpleOMT(
    diameter=1610,
    choke_width=1195,
    outer_path_radius=1610 + 800,
    centre_x=0.0,
    centre_y=0.0,
)

smiley_omt_cell = smiley_omt_instance.make_smiley_cell(
    microstrip_layer=layers["mm-feedline"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    back_etch_layer=layers["back_etch"],
)

omt_rotated_cell.add(gdspy.CellReference(omt_cell, rotation=0))
omt_x_spacing = np.sqrt(pixel_centre_to_centre**2 - (pixel_centre_to_centre / 2) ** 2)

full_omts_cell.add(
    [
        gdspy.CellReference(omt_cell, rotation=-15),
        gdspy.CellReference(omt_cell, origin=(0, pixel_centre_to_centre), rotation=30),
        gdspy.CellReference(
            omt_cell,
            origin=(omt_x_spacing, -pixel_centre_to_centre / 2),
            rotation=-60,
        ),
        gdspy.CellReference(
            smiley_omt_cell,
            origin=(omt_x_spacing, +pixel_centre_to_centre / 2),
            rotation=-60,
        ),
    ]
)

omt_x_displacement = 3100
omt_y_displacement = np.tan(radians(60)) * omt_x_displacement
slim_device_cell.add(
    gdspy.CellReference(
        full_omts_cell, origin=(omt_x_displacement, omt_y_displacement), rotation=30
    )
)

########################################################################################################################

# Define target frequencies for filter bank filters:
# NB: Final filter is not included in the mask.
# Define readout frequency band:
frequency_min = 120
frequency_max = 180
oversampling = 1.6

number_of_channels = round(
    oversampling * resolution * np.log(frequency_max / frequency_min)
)
print(f"Number of channels = {number_of_channels}")
target_f0_array = tools.create_target_f0_array(
    frequency_min, frequency_max, number_of_channels
)
# Create instance of filter bank using filter_bank module:
filter_bank = FilterBank(
    target_f0_array=target_f0_array,
    feedline_width=feedline_width,
    filter_separation=filter_spacing,
    input_connection_x=0,
    input_connection_y=0,
)

########################################################################################################################

# Section to make duo spectrometer Cell:

filter_bank_radius = 28500

duo_lekid_f0_array = np.linspace(2.010e9, 2.49e9, 140)
duo_dark_f0s = [1.795e9, 1.8e9, 1.805e9]
duo_full_band_lekid_f0s = [1.895e9, 1.89e9]
print(f"Duo F0 spacing: {(duo_lekid_f0_array[1]-duo_lekid_f0_array[0]) * 1e-6} MHz")

spectrometer_f0_arrays = util.spectrometer_builders.split_and_group_f0_array(
    duo_lekid_f0_array, number_of_groups=7, number_of_spectrometers=2
)

duo_f0_array_1 = spectrometer_f0_arrays[0]
duo_f0_array_2 = spectrometer_f0_arrays[1]

if show_plots:
    plt.figure(figsize=(8, 6))
    plt.plot(
        duo_f0_array_1.flatten() * 1e-9,
        linestyle="none",
        marker="o",
        markersize=4,
        label="Spectrometer 1 Detectors",
    )
    plt.plot(
        duo_f0_array_2.flatten() * 1e-9,
        linestyle="none",
        marker="o",
        markersize=4,
        label="Spectrometer 2 Detectors",
    )
    plt.title(
        "Plot showing all the f0 sub groups that go into each spectrometers f0 array in a duo"
    )
    plt.ylabel("F0 (GHz)")
    plt.xlabel("Premixed Detector Index")
    plt.legend()
    plt.show()

# Mix arrays:
duo_f0_array_1 = util.spectrometer_builders.mix_spectrometer_f0_array(
    duo_f0_array_1, [0, 3, 6, 2, 5, 1, 4]
)
duo_f0_array_2 = util.spectrometer_builders.mix_spectrometer_f0_array(
    duo_f0_array_2, [0, 3, 6, 2, 5, 1, 4]
)

# Clip f0s at end of arrays to match the number of channels in FBS:
duo_f0_array_1 = duo_f0_array_1[:number_of_channels]
duo_f0_array_2 = duo_f0_array_2[:number_of_channels]


if show_plots:
    plt.figure(figsize=(8, 6))
    plt.plot(
        duo_f0_array_1[::2] * 1e-9,
        linestyle=":",
        linewidth=1,
        markerfacecolor="none",
        marker="o",
        markersize=5,
        label="FBS 1 Bottom",
    )
    plt.plot(
        duo_f0_array_1[1::2] * 1e-9,
        linestyle=":",
        linewidth=1,
        markerfacecolor="none",
        marker="o",
        markersize=5,
        label="FBS 1 Top",
    )
    plt.plot(
        duo_f0_array_2[::2] * 1e-9,
        linestyle=":",
        linewidth=1,
        markerfacecolor="none",
        marker="o",
        markersize=5,
        label="FBS 2 Bottom",
    )
    plt.plot(
        duo_f0_array_2[1::2] * 1e-9,
        linestyle=":",
        linewidth=1,
        markerfacecolor="none",
        marker="o",
        markersize=5,
        label="FBS 2 Top",
    )
    plt.title("F0 placements of spectrometers along each side of the FBS in a Duo")
    plt.xlabel("Detector index along side of FBS")
    plt.ylabel("F0 (GHz)")
    plt.legend()
    plt.show()

    plt.figure(figsize=(5, 3))
    plt.plot()
    # plt.hlines(0.0, xmin=np.min(all_trio_f0s), xmax=np.max(all_trio_f0s))
    plt.vlines(
        duo_f0_array_1, ymax=0.0, ymin=-10, label="Duo A", color="r", linewidth=2
    )
    plt.vlines(
        duo_f0_array_2, ymax=0.0, ymin=-10, label="Duo B", color="g", linewidth=2
    )
    plt.vlines(
        duo_dark_f0s,
        ymax=0.0,
        ymin=-10,
        label=f"Dark x {len(duo_dark_f0s)}",
        color="k",
        linewidth=2,
    )
    plt.vlines(
        duo_full_band_lekid_f0s,
        ymax=0.0,
        ymin=-10,
        label=f"Full Band x {len(duo_full_band_lekid_f0s)}",
        color="c",
        linewidth=2,
    )
    plt.xlabel("Resonator F0 Schedule (GHz)")
    plt.ylabel("Arbitrary Amplitude")
    plt.legend()
    plt.show()

# Save f0's to text file.
np.savetxt(
    fname=chip_unique_id + "_duo_f0_array.txt",
    X=np.array([duo_f0_array_1, duo_f0_array_2]),
    delimiter=",",
)

print(np.array([duo_f0_array_1, duo_f0_array_2]).shape)

duo_angle = (
    360 / 18
)  # Rotation of each spectrometer relative to the line of symmetry between the two.
# Make radial spectrometer cell:
(
    duo_spectrometer_group_cell,
    duo_spectrometer_origin_a,
    duo_spectrometer_origin_b,
    duo_readout_input,
    duo_readout_output,
) = util.spectrometer_builders.make_radial_duo_spectrometer_cell(
    filter_bank_radius=filter_bank_radius,
    filter_bank_rotation_angle=duo_angle,
    lekid_f0_array_a=duo_f0_array_1,
    lekid_f0_array_b=duo_f0_array_2,
    full_band_lekid_f0s=duo_full_band_lekid_f0s,
    dark_f0s=duo_dark_f0s,
    filter_bank=filter_bank,
    cell_name="Spectrometer Group Cell",
    readout_width=readout_width,
    readout_gap=readout_gap,
    bridge_width=optimal_bridge_thickness,
    layers=layers,
    step_down=True,
)

duo_group_1_rotation = (360 / 18) * 5
duo_group_2_rotation = (360 / 18) * 3
duo_group_3_rotation = 360 / 18
slim_device_cell.add(
    [
        gdspy.CellReference(duo_spectrometer_group_cell, rotation=duo_group_1_rotation),
        gdspy.CellReference(duo_spectrometer_group_cell, rotation=duo_group_2_rotation),
        gdspy.CellReference(duo_spectrometer_group_cell, rotation=duo_group_3_rotation),
    ]
)

# Define angles of rotation for each spectrometer relative to the origin of the slim module device.
spectrometer_1_angle = duo_group_1_rotation + duo_angle / 2
spectrometer_2_angle = duo_group_1_rotation - duo_angle / 2
spectrometer_3_angle = duo_group_2_rotation + duo_angle / 2
spectrometer_4_angle = duo_group_2_rotation - duo_angle / 2
spectrometer_5_angle = duo_group_3_rotation + duo_angle / 2
spectrometer_6_angle = duo_group_3_rotation - duo_angle / 2

########################################################################################################################

# Bond pads:

# Section to add bond pads to slim device

single_bond_pad = EdgeBondPad(
    pad_width=400,
    pad_gap=250,
    cpw_width=readout_width,
    cpw_gap=readout_gap,
    pad_length=600,
    transition_length=700,
    origin_x=0.0,
    origin_y=0.0,
)
single_bond_pad_cell = single_bond_pad.make_cell(
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    cell_name="Bond Pad",
)


def reflect_in_midsection(coordinates: tuple) -> tuple:
    # intersection points with y=root(3)x and normal line:
    xa, ya = coordinates

    # y intercept of line perpendicular to the 60 degree midpoint line passing through the point to be mirrored:
    c = ya + (1 / np.sqrt(3)) * xa

    # find x coordinate of intersection between midpoint line and perpendicular line:
    xi = (3 * c) / (4 * np.sqrt(3))
    yi = xi * np.sqrt(3)

    # find difference between original point and intersection:
    dx = xi - xa
    dy = yi - ya

    return dx + xi, dy + yi


top_edge_height = 75000 + dice_width / 2.0
bond_pads_edge_cell = gdspy.Cell("Bond Pads Edge")

bond_pad_origin_1 = (-20000, top_edge_height - dice_width / 2 - 210)
bond_pad_origin_2 = (13500, top_edge_height - dice_width / 2 - 210)
bond_pad_origin_3 = (27000, top_edge_height - dice_width / 2 - 210)
bond_pad_origin_4 = reflect_in_midsection(bond_pad_origin_3)
bond_pad_origin_5 = reflect_in_midsection(bond_pad_origin_2)
bond_pad_origin_6 = reflect_in_midsection(bond_pad_origin_1)

slim_device_cell.add(
    [
        gdspy.CellReference(single_bond_pad_cell, origin=bond_pad_origin_1, rotation=0),
        gdspy.CellReference(single_bond_pad_cell, origin=bond_pad_origin_2, rotation=0),
        gdspy.CellReference(single_bond_pad_cell, origin=bond_pad_origin_3, rotation=0),
        gdspy.CellReference(
            single_bond_pad_cell, origin=bond_pad_origin_4, rotation=-360 * 1 / 6
        ),
        gdspy.CellReference(
            single_bond_pad_cell, origin=bond_pad_origin_5, rotation=-360 * 1 / 6
        ),
        gdspy.CellReference(
            single_bond_pad_cell, origin=bond_pad_origin_6, rotation=-360 * 1 / 6
        ),
    ]
)


########################################################################################################################

dl = (dice_width / 2.0) * np.tan(radians(30))

inner_edge_length = 75000
outer_point_x = top_edge_height / np.tan(radians(60))

dice_x_1, dice_y_1 = reflect_in_midsection((inner_edge_length + dl, -dice_width / 2.0))
# X coordinate at intersection between 60 degree line from point 1 and y=top_edge_height:
dice_x_2 = (top_edge_height - dice_y_1 + np.sqrt(3) * dice_x_1) / np.sqrt(3)

dice_points = [
    reflect_in_midsection((0.0, -dice_width / 2.0)),
    (dice_x_1, dice_y_1),
    (dice_x_2, top_edge_height),
    (outer_point_x, top_edge_height),
    reflect_in_midsection((dice_x_2, top_edge_height)),
    (inner_edge_length + dl, -dice_width / 2.0),
    (0.0, -dice_width / 2.0),
]

dice_path = gdspy.FlexPath(points=dice_points, width=dice_width, layer=layers["dice"])
slim_device_cell.add(dice_path)

########################################################################################################################

# Section to add tabs on top edge:

tabs_cell = gdspy.Cell("Tabs")
tab_cell = gdspy.Cell("Tab")
single_tab = gdspy.Polygon(
    points=[
        (0.0, -dice_width / 2),
        (4800.0, -dice_width / 2),
        (4800.0, dice_width / 2),
        (0.0, dice_width / 2),
        (0.0, -dice_width / 2),
    ],
    layer=layers["tabs"],
)
single_tab.fillet(150)
tab_cell.add(single_tab)

tabs_cell.add(gdspy.CellArray(tab_cell, columns=15, rows=1, spacing=(5100, 0.0)))

slim_device_cell.add(
    gdspy.CellReference(tabs_cell, origin=(dice_x_2 - 650, top_edge_height))
)

tabs_points = [
    (outer_point_x, top_edge_height),
    reflect_in_midsection((dice_x_2, top_edge_height)),
    (inner_edge_length + dl, -dice_width / 2.0),
    (0.0, -dice_width / 2.0),
    reflect_in_midsection((0.0, -dice_width / 2.0)),
    (dice_x_1, dice_y_1),
    (dice_x_2, top_edge_height),
]
tabs_path = gdspy.FlexPath(points=tabs_points, width=dice_width, layer=layers["tabs"])
slim_device_cell.add(tabs_path)

########################################################################################################################

# Connect Readouts of spectrometer group 1:

# **********************************************************************************************************************

# Input:

bond_pad_connections = (
    single_bond_pad.get_feedline_connection_x(),
    single_bond_pad.get_feedline_connection_y(),
)
group_1_input = tools.rotate_coordinates(
    duo_readout_input[0], duo_readout_input[1], angle=radians(duo_group_1_rotation)
)

group_1_readout_input_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_1_angle,
        input=True,
        connection_coordinates=group_1_input,
    )
)

group_1_readout_input_points = [
    (
        bond_pad_connections[0] + bond_pad_origin_1[0] - 2000,
        bond_pad_connections[1] + bond_pad_origin_1[1] - 30000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_1[0],
        bond_pad_connections[1] + bond_pad_origin_1[1] - 10000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_1[0],
        bond_pad_connections[1] + bond_pad_origin_1[1] - 9500,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_1[0],
        bond_pad_connections[1] + bond_pad_origin_1[1],
    ),
]

group_1_readout_input_points = (
    group_1_readout_input_bend_points + group_1_readout_input_points
)

group_1_input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_1_readout_input_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout input transition",
)

group_1_readout_input = general_gds_tools.build_cpw_with_bridges(
    path_points=group_1_readout_input_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout input",
)

slim_device_cell.add(group_1_input_transition)
slim_device_cell.add(group_1_readout_input)

# Add step down geometry around microwave path:
group_1_readout_input_points.append(bond_pad_origin_1)
group_1_input_step_down_1 = gdspy.FlexPath(
    points=group_1_readout_input_points[:-1], width=560, layer=layers["oxide_step_down"]
)
group_1_input_step_down_2 = gdspy.FlexPath(
    points=group_1_readout_input_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_1_input_step_down_1, group_1_input_step_down_2])
#
# # **********************************************************************************************************************
#
# # Output:

# Connect Readouts of Trios:
group_1_output = tools.rotate_coordinates(
    duo_readout_output[0], duo_readout_output[1], angle=radians(duo_group_1_rotation)
)
print(spectrometer_2_angle)
group_1_readout_output_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_2_angle,
        input=False,
        connection_coordinates=group_1_output,
    )
)
group_1_readout_output_points = [
    (
        bond_pad_connections[0] + bond_pad_origin_2[0] - 8000,
        bond_pad_connections[1] + bond_pad_origin_2[1] - 30000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_2[0],
        bond_pad_connections[1] + bond_pad_origin_2[1] - 10000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_2[0],
        bond_pad_connections[1] + bond_pad_origin_2[1] - 9500,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_2[0],
        bond_pad_connections[1] + bond_pad_origin_2[1],
    ),
]
group_1_readout_output_points = (
    group_1_readout_output_bend_points + group_1_readout_output_points
)

group_1_output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_1_readout_output_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout output transition",
)

group_1_readout_output = general_gds_tools.build_cpw_with_bridges(
    path_points=group_1_readout_output_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 1 Readout output",
)

slim_device_cell.add(group_1_output_transition)
slim_device_cell.add(group_1_readout_output)

# Add step down geometry around microwave path:
group_1_readout_output_points.append(bond_pad_origin_2)
group_1_output_step_down_1 = gdspy.FlexPath(
    points=group_1_readout_output_points[:-1],
    width=560,
    layer=layers["oxide_step_down"],
)
group_1_output_step_down_2 = gdspy.FlexPath(
    points=group_1_readout_output_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_1_output_step_down_1, group_1_output_step_down_2]),

# ########################################################################################################################

# Connect Readouts of spectrometer group 3:

# **********************************************************************************************************************

# Output:

bond_pad_connections = (
    single_bond_pad.get_feedline_connection_x(),
    single_bond_pad.get_feedline_connection_y(),
)
bond_pad_connections = tools.rotate_coordinates(
    bond_pad_connections[0], bond_pad_connections[1], -np.pi * 1 / 3
)
group_3_output = tools.rotate_coordinates(
    duo_readout_output[0], duo_readout_output[1], angle=radians(duo_group_3_rotation)
)

group_3_readout_output_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_6_angle,
        input=False,
        connection_coordinates=group_3_output,
    )
)

group_3_readout_output_points = [
    (
        bond_pad_connections[0]
        + bond_pad_origin_6[0]
        + 2000 * np.sin(radians(30))
        - 30000 * np.cos(radians(30)),
        bond_pad_connections[1]
        + bond_pad_origin_6[1]
        - 30000 * np.sin(radians(30))
        - 2000 * np.cos(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_6[0] - 10000 * np.cos(radians(30)),
        bond_pad_connections[1] + bond_pad_origin_6[1] - 10000 * np.sin(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_6[0] - 9500 * np.cos(radians(30)),
        bond_pad_connections[1] + bond_pad_origin_6[1] - 9500 * np.sin(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_6[0],
        bond_pad_connections[1] + bond_pad_origin_6[1],
    ),
]

group_3_readout_output_points = (
    group_3_readout_output_bend_points + group_3_readout_output_points
)

group_3_output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_3_readout_output_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 3 Readout output transition",
)

group_3_readout_output = general_gds_tools.build_cpw_with_bridges(
    path_points=group_3_readout_output_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 3 Readout output",
)

slim_device_cell.add(group_3_output_transition)
slim_device_cell.add(group_3_readout_output)

# Add step down geometry around microwave path:
group_3_readout_output_points.append(bond_pad_origin_6)
group_3_output_step_down_1 = gdspy.FlexPath(
    points=group_3_readout_output_points[:-1],
    width=560,
    layer=layers["oxide_step_down"],
)
group_3_output_step_down_2 = gdspy.FlexPath(
    points=group_3_readout_output_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_3_output_step_down_1, group_3_output_step_down_2]),

# **********************************************************************************************************************

# Input:

group_3_input = tools.rotate_coordinates(
    duo_readout_input[0], duo_readout_input[1], angle=radians(duo_group_3_rotation)
)

group_3_readout_input_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_5_angle,
        input=True,
        connection_coordinates=group_3_input,
    )
)

x1, y1 = tools.rotate_coordinates(x=-2000, y=-30000, angle=radians(-60))
group_3_readout_input_points = [
    (
        bond_pad_connections[0]
        + bond_pad_origin_5[0]
        + 8000 * np.sin(radians(30))
        - 30000 * np.cos(radians(30)),
        bond_pad_connections[1]
        + bond_pad_origin_5[1]
        - 30000 * np.sin(radians(30))
        - 8000 * np.cos(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_5[0] - 10000 * np.cos(radians(30)),
        bond_pad_connections[1] + bond_pad_origin_5[1] - 10000 * np.sin(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_5[0] - 9500 * np.cos(radians(30)),
        bond_pad_connections[1] + bond_pad_origin_5[1] - 9500 * np.sin(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_5[0],
        bond_pad_connections[1] + bond_pad_origin_5[1],
    ),
]

group_3_readout_input_points = (
    group_3_readout_input_bend_points + group_3_readout_input_points
)

group_3_input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_3_readout_input_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 3 Readout input transition",
)

group_3_readout_input = general_gds_tools.build_cpw_with_bridges(
    path_points=group_3_readout_input_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 3 Readout input",
)

slim_device_cell.add(group_3_input_transition)
slim_device_cell.add(group_3_readout_input)

# Add step down geometry around microwave path:
group_3_readout_input_points.append(bond_pad_origin_5)
group_3_input_step_down_1 = gdspy.FlexPath(
    points=group_3_readout_input_points[:-1], width=560, layer=layers["oxide_step_down"]
)
group_3_input_step_down_2 = gdspy.FlexPath(
    points=group_3_readout_input_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_3_input_step_down_1, group_3_input_step_down_2]),


########################################################################################################################

# Connect Readouts of spectrometer group 2:

# **********************************************************************************************************************

# Input:

bond_pad_connections = (
    single_bond_pad.get_feedline_connection_x(),
    single_bond_pad.get_feedline_connection_y(),
)
group_2_input = tools.rotate_coordinates(
    duo_readout_input[0], duo_readout_input[1], angle=radians(duo_group_2_rotation)
)

group_2_readout_input_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_3_angle,
        input=True,
        connection_coordinates=group_2_input,
    )
)

group_2_readout_input_points = [
    (
        bond_pad_connections[0] + bond_pad_origin_3[0] - 17000,
        bond_pad_connections[1] + bond_pad_origin_3[1] - 30000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_3[0],
        bond_pad_connections[1] + bond_pad_origin_3[1] - 10000,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_3[0],
        bond_pad_connections[1] + bond_pad_origin_3[1] - 9500,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_3[0],
        bond_pad_connections[1] + bond_pad_origin_3[1],
    ),
]

group_2_readout_input_points = (
    group_2_readout_input_bend_points + group_2_readout_input_points
)

group_2_input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_2_readout_input_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 2 Readout input transition",
)

group_2_readout_input = general_gds_tools.build_cpw_with_bridges(
    path_points=group_2_readout_input_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 2 Readout input",
)

slim_device_cell.add(group_2_input_transition)
slim_device_cell.add(group_2_readout_input)

# Add step down geometry around microwave path:
group_2_readout_input_points.append(bond_pad_origin_3)
group_2_input_step_down_1 = gdspy.FlexPath(
    points=group_2_readout_input_points[:-1], width=560, layer=layers["oxide_step_down"]
)
group_2_input_step_down_2 = gdspy.FlexPath(
    points=group_2_readout_input_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_2_input_step_down_1, group_2_input_step_down_2]),

# **********************************************************************************************************************

# Output:

bond_pad_connections = (
    single_bond_pad.get_feedline_connection_x(),
    single_bond_pad.get_feedline_connection_y(),
)
bond_pad_connections = tools.rotate_coordinates(
    bond_pad_connections[0], bond_pad_connections[1], -np.pi * 1 / 3
)
group_2_output = tools.rotate_coordinates(
    duo_readout_output[0], duo_readout_output[1], angle=radians(duo_group_2_rotation)
)

group_2_readout_output_bend_points = (
    util.spectrometer_builders.get_group_readout_bend_points(
        spectrometer_angle=spectrometer_4_angle,
        input=False,
        connection_coordinates=group_2_output,
    )
)

x1, y1 = tools.rotate_coordinates(x=17000, y=-30000, angle=radians(-60))
x2, y2 = tools.rotate_coordinates(x=0, y=-10000, angle=radians(-60))
group_2_readout_output_points = [
    (
        bond_pad_connections[0] + bond_pad_origin_4[0] + x1,
        bond_pad_connections[1] + bond_pad_origin_4[1] + y1,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_4[0] + x2,
        bond_pad_connections[1] + bond_pad_origin_4[1] + y2,
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_4[0] - 9500 * np.cos(radians(30)),
        bond_pad_connections[1] + bond_pad_origin_4[1] - 9500 * np.sin(radians(30)),
    ),
    (
        bond_pad_connections[0] + bond_pad_origin_4[0],
        bond_pad_connections[1] + bond_pad_origin_4[1],
    ),
]

group_2_readout_output_points = (
    group_2_readout_output_bend_points + group_2_readout_output_points
)

group_2_output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=group_2_readout_output_points[-2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=optimal_bridge_thickness,
    final_bridge_thickness=0.1,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 2 Readout output transition",
)

group_2_readout_output = general_gds_tools.build_cpw_with_bridges(
    path_points=group_2_readout_output_points[:-1],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Group 2 Readout output",
)

slim_device_cell.add(group_2_output_transition)
slim_device_cell.add(group_2_readout_output)

# Add step down geometry around microwave path:
group_2_readout_output_points.append(bond_pad_origin_4)
group_2_output_step_down_1 = gdspy.FlexPath(
    points=group_2_readout_output_points[:-1],
    width=560,
    layer=layers["oxide_step_down"],
)
group_2_output_step_down_2 = gdspy.FlexPath(
    points=group_2_readout_output_points[-2:],
    width=1000,
    layer=layers["oxide_step_down"],
)

slim_device_cell.add([group_2_output_step_down_1, group_2_output_step_down_2]),


########################################################################################################################

# Connect hybrids to filterbanks:

# **********************************************************************************************************************

# # Pixel 1:
# # A:
#
# # Rotate and translate connection coordinates:
hybrid_connections = full_omt.get_connections()
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connections[0][0], hybrid_connections[0][1], angle=radians(30)
)
hybrid_connection_1 = (
    hybrid_connection_1[0],
    hybrid_connection_1[1] + pixel_centre_to_centre,
)
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connection_1[0], hybrid_connection_1[1], angle=radians(30)
)
hybrid_connection_1 = (
    hybrid_connection_1[0] + omt_x_displacement,
    hybrid_connection_1[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_1_angle))
delta_y = -1000 * np.sin(radians(spectrometer_1_angle))

duo_spectrometer_origin_1_a = tools.rotate_coordinates(
    duo_spectrometer_origin_a[0],
    duo_spectrometer_origin_a[1],
    angle=radians(duo_group_1_rotation),
)
pixel_path_points_1 = [
    hybrid_connection_1,
    (
        hybrid_connection_1[0] + 600 * np.cos(radians(60)),
        hybrid_connection_1[1] + 600 * np.sin(radians(60)),
    ),
    (
        duo_spectrometer_origin_1_a[0] + delta_x,
        duo_spectrometer_origin_1_a[1] + delta_y,
    ),
    duo_spectrometer_origin_1_a,
]

pixel_path_1 = gdspy.FlexPath(
    points=pixel_path_points_1,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_1)

# B:

# Rotate and translate connection coordinates:
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connections[1][0], hybrid_connections[1][1], angle=radians(30)
)
hybrid_connection_2 = (
    hybrid_connection_2[0],
    hybrid_connection_2[1] + pixel_centre_to_centre,
)
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connection_2[0], hybrid_connection_2[1], angle=radians(30)
)
hybrid_connection_2 = (
    hybrid_connection_2[0] + omt_x_displacement,
    hybrid_connection_2[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_2_angle))
delta_y = -1000 * np.sin(radians(spectrometer_2_angle))

duo_spectrometer_origin_1_b = tools.rotate_coordinates(
    duo_spectrometer_origin_b[0],
    duo_spectrometer_origin_b[1],
    angle=radians(duo_group_1_rotation),
)
pixel_path_points_2 = [
    hybrid_connection_2,
    (
        hybrid_connection_2[0] + 600 * np.cos(radians(30)),
        hybrid_connection_2[1] - 600 * np.sin(radians(30)),
    ),
    (
        duo_spectrometer_origin_1_b[0] + delta_x,
        duo_spectrometer_origin_1_b[1] + delta_y,
    ),
    duo_spectrometer_origin_1_b,
]

pixel_path_2 = gdspy.FlexPath(
    points=pixel_path_points_2,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_2)

# **********************************************************************************************************************

# Pixel 2:
# A:

# Rotate and translate connection coordinates:
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connections[0][0], hybrid_connections[0][1], angle=radians(-15)
)
hybrid_connection_1 = (hybrid_connection_1[0], hybrid_connection_1[1])
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connection_1[0], hybrid_connection_1[1], angle=radians(30)
)
hybrid_connection_1 = (
    hybrid_connection_1[0] + omt_x_displacement,
    hybrid_connection_1[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_3_angle))
delta_y = -1000 * np.sin(radians(spectrometer_3_angle))

duo_spectrometer_origin_2_a = tools.rotate_coordinates(
    duo_spectrometer_origin_a[0],
    duo_spectrometer_origin_a[1],
    angle=radians(duo_group_2_rotation),
)
pixel_path_points_1 = [
    hybrid_connection_1,
    (
        hybrid_connection_1[0] + 600 * np.cos(radians(15)),
        hybrid_connection_1[1] + 600 * np.sin(radians(15)),
    ),
    (
        duo_spectrometer_origin_2_a[0] + delta_x,
        duo_spectrometer_origin_2_a[1] + delta_y,
    ),
    duo_spectrometer_origin_2_a,
]

pixel_path_1 = gdspy.FlexPath(
    points=pixel_path_points_1,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_1)

# B:

# Rotate and translate connection coordinates:
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connections[1][0], hybrid_connections[1][1], angle=radians(-15)
)
hybrid_connection_2 = (hybrid_connection_2[0], hybrid_connection_2[1])
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connection_2[0], hybrid_connection_2[1], angle=radians(30)
)
hybrid_connection_2 = (
    hybrid_connection_2[0] + omt_x_displacement,
    hybrid_connection_2[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_4_angle))
delta_y = -1000 * np.sin(radians(spectrometer_4_angle))

duo_spectrometer_origin_2_b = tools.rotate_coordinates(
    duo_spectrometer_origin_b[0],
    duo_spectrometer_origin_b[1],
    angle=radians(duo_group_2_rotation),
)
pixel_path_points_2 = [
    hybrid_connection_2,
    (
        hybrid_connection_2[0] + 600 * np.sin(radians(15)),
        hybrid_connection_2[1] - 600 * np.cos(radians(15)),
    ),
    (
        duo_spectrometer_origin_2_b[0] + delta_x,
        duo_spectrometer_origin_2_b[1] + delta_y,
    ),
    duo_spectrometer_origin_2_b,
]

pixel_path_2 = gdspy.FlexPath(
    points=pixel_path_points_2,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_2)

# # **********************************************************************************************************************

# Pixel 3:
# A:

# Rotate and translate connection coordinates:
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connections[0][0], hybrid_connections[0][1], angle=radians(-60)
)
hybrid_connection_1 = (
    hybrid_connection_1[0] + omt_x_spacing,
    hybrid_connection_1[1] - pixel_centre_to_centre / 2,
)
hybrid_connection_1 = tools.rotate_coordinates(
    hybrid_connection_1[0], hybrid_connection_1[1], angle=radians(30)
)
hybrid_connection_1 = (
    hybrid_connection_1[0] + omt_x_displacement,
    hybrid_connection_1[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_5_angle))
delta_y = -1000 * np.sin(radians(spectrometer_5_angle))

duo_spectrometer_origin_3_a = tools.rotate_coordinates(
    duo_spectrometer_origin_a[0],
    duo_spectrometer_origin_a[1],
    angle=radians(duo_group_3_rotation),
)
pixel_path_points_1 = [
    hybrid_connection_1,
    (
        hybrid_connection_1[0] + 600 * np.cos(radians(30)),
        hybrid_connection_1[1] - 600 * np.sin(radians(30)),
    ),
    (
        duo_spectrometer_origin_3_a[0] + delta_x,
        duo_spectrometer_origin_3_a[1] + delta_y,
    ),
    duo_spectrometer_origin_3_a,
]

pixel_path_1 = gdspy.FlexPath(
    points=pixel_path_points_1,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_1)

# B:

# Rotate and translate connection coordinates:
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connections[1][0], hybrid_connections[1][1], angle=radians(-60)
)
hybrid_connection_2 = (
    hybrid_connection_2[0] + omt_x_spacing,
    hybrid_connection_2[1] - pixel_centre_to_centre / 2,
)
hybrid_connection_2 = tools.rotate_coordinates(
    hybrid_connection_2[0], hybrid_connection_2[1], angle=radians(30)
)
hybrid_connection_2 = (
    hybrid_connection_2[0] + omt_x_displacement,
    hybrid_connection_2[1] + omt_y_displacement,
)

delta_x = -1000 * np.cos(radians(spectrometer_6_angle))
delta_y = -1000 * np.sin(radians(spectrometer_6_angle))

duo_spectrometer_origin_3_b = tools.rotate_coordinates(
    duo_spectrometer_origin_b[0],
    duo_spectrometer_origin_b[1],
    angle=radians(duo_group_3_rotation),
)
pixel_path_points_2 = [
    hybrid_connection_2,
    (
        hybrid_connection_2[0] - 600 * np.cos(radians(60)),
        hybrid_connection_2[1] - 600 * np.sin(radians(60)),
    ),
    (
        duo_spectrometer_origin_3_b[0] + delta_x,
        duo_spectrometer_origin_3_b[1] + delta_y,
    ),
    duo_spectrometer_origin_3_b,
]

pixel_path_2 = gdspy.FlexPath(
    points=pixel_path_points_2,
    width=feedline_width,
    corners="circular bend",
    bend_radius=100,
    layer=layers["mm-feedline"],
)
slim_device_cell.add(pixel_path_2)


########################################################################################################################
# Section to copy and expand oxide step down layer to form nitride stepdown layer.

slim_module_nitride_step_down_polygon_set = (
    util.general_gds_tools.offset_polygons_on_layer(
        layer_to_reduce=layers["oxide_step_down"],
        output_layer=layers["nitride_step_down"],
        cell=slim_device_cell,
        offset=12.0,
    )
)
slim_device_cell.add(slim_module_nitride_step_down_polygon_set)

########################################################################################################################

# Section to add alignment marks:

alignment_centre_1 = (60000, 0)
alignment_centre_2 = (-60000, 0)

alignment_cross = AlignmentCross()
alignment_cell = alignment_cross.make_cell(
    layer=layers["alignment"], cell_name="Alignment cross"
)
main_cell.add(
    [
        gdspy.CellReference(alignment_cell, alignment_centre_1),
        gdspy.CellReference(alignment_cell, alignment_centre_2),
    ]
)

if fab_facility == "ANL" or fab_facility == "CDF":
    alignment_cell_101 = alignment_cross.make_cell(
        layer=layers["fab_1_nitride_membrane_etch"],
        cell_name="Nitride membrane alignment cross",
    )

    alignment_cell_103 = alignment_cross.make_cell(
        layer=layers["fab_3_nb_wiring"], cell_name="Nb wiring alignment cross"
    )
    main_cell.add(
        [
            gdspy.CellReference(alignment_cell_101, (58500, 0)),
            gdspy.CellReference(alignment_cell_101, (-58500, 0)),
            gdspy.CellReference(alignment_cell_103, (57000, 0)),
            gdspy.CellReference(alignment_cell_103, (-57000, 0)),
        ]
    )


########################################################################################################################

# Section to add unique id and logos

length_per_line = len(chip_unique_id)
chip_unique_id_new = chip_unique_id
if len(chip_unique_id_new) > 20:
    index = 15
    while chip_unique_id_new[index] != "_":
        index += 1
    chip_unique_id_new = chip_unique_id_new[:index] + "\n" + chip_unique_id_new[index:]

    length_per_line = index


chip_unique_id_text = gdspy.Text(
    text=chip_unique_id_new,
    size=1000,
    position=(
        33000 - 1000 * (len(chip_unique_id) * ((5 / 9) + (11 / 9))) / 8,
        np.sqrt(3) * 38000,
    ),
    angle=-radians(30),
    layer=layers["ground"],
)

fab_facility_text = gdspy.Text(
    text=fab_facility,
    size=1000,
    position=(
        37000 - 1000 * (len(fab_facility) * ((5 / 9) + (11 / 9))) / 8,
        np.sqrt(3) * 37000,
    ),
    angle=-radians(30),
    layer=layers["ground"],
)

slim_device_cell.add([chip_unique_id_text, fab_facility_text])

########################################################################################################################

# Section to add logos:
logos_cell = gdspy.Cell("Logos")

# ANL
anl_logo_cell = gdspy.Cell("ANL Logo")
anl_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\anl_logo.gds", layer=0
)
anl_logo_polygon_set = gdspy.PolygonSet(anl_logo_polygons, layer=layers["ground"])
anl_logo_cell.add(anl_logo_polygon_set)

# UC
uc_logo_cell = gdspy.Cell("UC Logo")
uc_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\uc_logo.gds", layer=0
)
uc_logo_polygon_set = gdspy.PolygonSet(uc_logo_polygons, layer=layers["ground"])
uc_logo_cell.add(uc_logo_polygon_set)

# Cardiff
cardiff_logo_cell = gdspy.Cell("Cardiff Logo")
cardiff_logo_polygons = general_gds_tools.extract_polygons_from_gds_file(
    filename="logos\\cardiff_logo.gds", layer=0
)
cardiff_logo_polygon_set = gdspy.PolygonSet(
    cardiff_logo_polygons, layer=layers["ground"]
)
cardiff_logo_cell.add(cardiff_logo_polygon_set)

logos_cell.add(
    [
        gdspy.CellReference(cardiff_logo_cell, origin=(0.0, 0.0)),
        gdspy.CellReference(uc_logo_cell, origin=(5500, 0.0)),
        gdspy.CellReference(anl_logo_cell, origin=(-5500, 0.0)),
    ]
)

slim_device_cell.add(
    gdspy.CellReference(logos_cell, origin=(39000, np.sqrt(3) * 39000), rotation=-30)
)

########################################################################################################################


bounding_box = slim_device_cell.get_bounding_box()

x = (bounding_box[1][0] + bounding_box[0][0]) / 2
y = (bounding_box[1][1] + bounding_box[0][1]) / 2

all_devices_cell.add(
    gdspy.CellReference(
        slim_device_cell, origin=(-x + 6000, -y + 6000 * np.tan(radians(60)))
    )
)

########################################################################################################################

# Section to add sub devices from other files:

device_copy_cell_1 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\mm_loss_2x2_v2p6.gds", cell_name="Device copy 1"
)

dev_1_origin_x = 16000
dev_1_origin_y = -43000

all_devices_cell.add(
    gdspy.CellReference(
        device_copy_cell_1, origin=(dev_1_origin_x, dev_1_origin_y), rotation=90
    )
)

device_copy_cell_2 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\mini_fbs_test_v1_mask.gds",
    cell_name="Device copy 2",
)

dev_2_origin = reflect_in_midsection((15000, dev_1_origin_y))
all_devices_cell.add(
    gdspy.CellReference(device_copy_cell_2, origin=dev_2_origin, rotation=30)
)

device_copy_cell_3 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\SO_UK_6inch_reduced_test_mask_v7_single_device.gds",
    cell_name="Device copy 3",
)
all_devices_cell.add(
    gdspy.CellReference(
        device_copy_cell_3, origin=(-dev_1_origin_x, dev_1_origin_y), rotation=90
    )
)

########################################################################################################################

# Section to add small sub devices from other files:

small_device_copy_cell_1 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\slim_10x10_all_Al_resonator_ANL_mask.gds",
    cell_name="10x10 Al Device",
)
small_dev_1_origin_x = 37750
small_dev_1_origin_y = -34495
all_devices_cell.add(
    gdspy.CellReference(
        small_device_copy_cell_1,
        origin=(small_dev_1_origin_x, small_dev_1_origin_y),
        rotation=180,
    )
)

small_device_copy_cell_2 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\slim_10x10_all_Nb_resonator_ANL_mask.gds",
    cell_name="10x10 Nb Device",
)
small_dev_2_origin = reflect_in_midsection((small_dev_1_origin_x, small_dev_1_origin_y))
all_devices_cell.add(
    gdspy.CellReference(
        small_device_copy_cell_2, origin=small_dev_2_origin, rotation=300
    )
)

small_device_copy_cell_3 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\slim_10x10_all_TiAL_resonator_ANL_mask.gds",
    cell_name="10x10 TiAl Device",
)
all_devices_cell.add(
    gdspy.CellReference(
        small_device_copy_cell_3,
        origin=(small_dev_1_origin_x, small_dev_1_origin_y - 12000),
        rotation=180,
    )
)

small_device_copy_cell_4 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\slim_10x10_all_TiAL_resonator_ANL_mask.gds",
    cell_name="10x10 Hybrid Device",
)
small_dev_4_origin = reflect_in_midsection(
    (small_dev_1_origin_x, small_dev_1_origin_y - 12000)
)
all_devices_cell.add(
    gdspy.CellReference(
        small_device_copy_cell_4, origin=small_dev_4_origin, rotation=300
    )
)

########################################################################################################################

# Section to add DC structures
dc_group_1_cell = gdspy.Cell("DC Group 1")

dc_device_1 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctest_intermetal_contact.gds",
    cell_name="DC Test 1",
)
dc_device_2 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctest_ustrip.gds",
    cell_name="DC Test 2",
)
dc_device_3 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctest_xunder.gds",
    cell_name="DC Test 3",
)

dc_group_1_cell.add([
    gdspy.CellReference(
        dc_device_1,
        origin=(0, 0),
        rotation=0,
    ),
    gdspy.CellReference(
        dc_device_2,
        origin=(10000, 2150),
        rotation=-90,
    ),
    gdspy.CellReference(
        dc_device_3,
        origin=(-7100, 0),
        rotation=0,
    ),
])

# Section to add DC structures
dc_group_2_cell = gdspy.Cell("DC Group 2")

dc_device_4 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctests_tc_rn_al.gds",
    cell_name="DC Test 4",
)
dc_device_5 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctests_tc_rn_nb.gds",
    cell_name="DC Test 5",
)
dc_device_6 = general_gds_tools.get_top_cell_from_gds_file(
    "sub_device_masks\dctests_tc_rn_ti_al.gds",
    cell_name="DC Test 6",
)

dc_group_2_cell.add([
    gdspy.CellReference(
        dc_device_4,
        origin=(0, 0),
        rotation=0,
    ),
    gdspy.CellReference(
        dc_device_5,
        origin=(11400, 0),
        rotation=0,
    ),
    gdspy.CellReference(
        dc_device_6,
        origin=(-11400, 0),
        rotation=0,
    )
])

dc_group_1_origin_x = -8000
dc_group_1_origin_y = 53500
dc_group_2_origin = reflect_in_midsection((dc_group_1_origin_x, dc_group_1_origin_y + 2150))

all_devices_cell.add(gdspy.CellReference(dc_group_1_cell, origin=(-10000, 53500), rotation=0))
all_devices_cell.add(gdspy.CellReference(dc_group_2_cell, origin=dc_group_2_origin, rotation=-60))
#

########################################################################################################################

# Section to add dowel hole and slot:

dowel_centre = (
    omt_x_displacement + (pixel_centre_to_centre / 2 * np.cos(radians(60))),
    (omt_x_displacement + (pixel_centre_to_centre / 2 * np.cos(radians(60))))
    * np.tan(radians(60)),
)
dowel_hole = gdspy.Round(center=dowel_centre, radius=407, layer=layers["dowel"])
dowel_ground_hole = gdspy.Round(center=dowel_centre, radius=407, layer=layers["ground"])
dowel_dielectric_hole = gdspy.Round(
    center=dowel_centre, radius=407, layer=layers["dielectric"]
)
slim_device_cell.add([dowel_hole, dowel_ground_hole, dowel_dielectric_hole])

dowel_slot_cell = gdspy.Cell("Dowel Slot")
dowel_slot_points = [
    (0.0, -407.0),
    (1813.0, -407.0),
    (1813.0, 407.0),
    (0.0, 407.0),
    (0.0, -407.0),
]
dowel_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["dowel"])
dowel_ground_slot = gdspy.Polygon(points=dowel_slot_points, layer=layers["ground"])
dowel_dielectric_slot = gdspy.Polygon(
    points=dowel_slot_points, layer=layers["dielectric"]
)
dowel_slot.fillet(407)
dowel_ground_slot.fillet(407)
dowel_dielectric_slot.fillet(407)
dowel_slot_cell.add([dowel_slot, dowel_ground_slot, dowel_dielectric_slot])

slim_device_cell.add(
    gdspy.CellReference(
        dowel_slot_cell, origin=(40546.75, np.sqrt(3) * 40546.75), rotation=60
    )
)

########################################################################################################################

if fab_facility == "ANL" or fab_facility == "CDF":
    # Add end point detection pads and e-beam alignment

    end_point_squares_cell = gdspy.Cell("End point square")
    nitride_step_down_end_point_square = gdspy.Rectangle(
        (-1600, -750), (-100, 750), layer=layers["nitride_step_down"]
    )
    niobium_end_point_square = gdspy.Rectangle(
        (100, 750), (1600, -750), layer=layers["idc"]
    )
    dielectric_end_point_square = gdspy.Rectangle(
        (100, 750), (1600, -750), layer=layers["dielectric"]
    )

    end_point_squares_cell.add(
        [
            nitride_step_down_end_point_square,
            dielectric_end_point_square,
            niobium_end_point_square,
        ]
    )
    all_devices_cell.add(
        gdspy.CellReference(end_point_squares_cell, origin=(2000, 3400), rotation=60)
    )

    e_beam_alignment_cross = gdspy.Cell("E-Beam Cross")
    e_beam_cross_1 = gdspy.Rectangle((-500, 1), (500, -1), layer=layers["alignment"])
    e_beam_cross_2 = gdspy.Rectangle((-1, 500), (1, -500), layer=layers["alignment"])
    e_beam_alignment_cross.add([e_beam_cross_1, e_beam_cross_2])

    all_devices_cell.add(
        [
            gdspy.CellReference(e_beam_alignment_cross, (0, 62000)),
            gdspy.CellReference(e_beam_alignment_cross, (62000, 0)),
            gdspy.CellReference(e_beam_alignment_cross, (0, -62000)),
            gdspy.CellReference(e_beam_alignment_cross, (-62000, 0)),
        ]
    )

########################################################################################################################

# Section to add dielectric / aluminium etch test for UC masks:

if fab_facility == "UC":

    etch_test_cell = gdspy.Cell("Etch Test Cell")
    etch_al_cross = gdspy.Rectangle(
        (-1000, 500), (1000, -500), layer=layers["aluminium"]
    )
    etch_dielectric_cross = gdspy.Rectangle(
        (-500, 1000), (500, -1000), layer=layers["dielectric"]
    )
    etch_test_cell.add([etch_al_cross, etch_dielectric_cross])

    etch_test_cell_array = gdspy.CellArray(
        etch_test_cell,
        columns=4,
        rows=1,
        spacing=(20000, 0.0),
        origin=(-10000, -7000),
        rotation=215,
    )
    all_devices_cell.add(etch_test_cell_array)

########################################################################################################################

# Section to add Pogo Pins and slot:

pogo_1 = gdspy.Round(
    center=(15000, 15000 * np.tan(radians(60))), radius=1200, layer=layers["pogo_pins"]
)
pogo_2 = gdspy.Round(center=(50000, 29000), radius=1200, layer=layers["pogo_pins"])
pogo_3 = gdspy.Round(center=(0, 57500), radius=1200, layer=layers["pogo_pins"])

slim_device_cell.add([pogo_1, pogo_2, pogo_3])

########################################################################################################################

# Section to copy component layers into fab process layers:

# "": 103,
# "": 104,
# "": 105,
# "": 106,
# "": 107

general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["nitride_step_down"]],
    output_layer=layers["fab_1_nitride_membrane_etch"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["oxide_step_down"]],
    output_layer=layers["fab_2_oxide_membrane_etch"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[
        layers["idc"],
        layers["readout_centre"],
        layers["antennas"],
        layers["mm-feedline"],
        layers["capacitor_plates"],
    ],
    output_layer=layers["fab_3_nb_wiring"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["inductors"], layers["aluminium"]],
    output_layer=layers["fab_4_al_meanders"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["dielectric"]],
    output_layer=layers["fab_5_microstrip_dielectric"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["ground"], layers["dowel"], layers["tabs"]],
    output_layer=layers["fab_6_nb_ground"],
    cell=all_devices_cell,
)
general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["back_etch"], layers["dowel"], layers["tabs"]],
    output_layer=layers["fab_7_drie_etch"],
    cell=all_devices_cell,
)

general_gds_tools.copy_polygons_to_layer(
    layers_to_copy=[layers["titanium_aluminium"]],
    output_layer=layers["fab_8_titanium_aluminium"],
    cell=all_devices_cell,
)

########################################################################################################################

# Section to add calipers to fab layers:

alignment_caliper = AlignmentCaliper(
    notch_spacing=2.0,
    notch_height=8,
    notch_width=3,
    notch_count=10,
    off_set_per_notch=0.2,
    row_spacing=0.0,
    centre=(0.0, 0.0),
)

caliper_1 = alignment_caliper.make_cell(
    bottom_layer=layers["fab_1_nitride_membrane_etch"],
    top_layer=layers["fab_2_oxide_membrane_etch"],
    cell_name="Alignment Caliper 1",
)

caliper_2 = alignment_caliper.make_cell(
    bottom_layer=layers["fab_1_nitride_membrane_etch"],
    top_layer=layers["fab_3_nb_wiring"],
    cell_name="Alignment Caliper 2",
)

caliper_3 = alignment_caliper.make_cell(
    bottom_layer=layers["fab_3_nb_wiring"],
    top_layer=layers["fab_4_al_meanders"],
    cell_name="Alignment Caliper 3",
)

caliper_4 = alignment_caliper.make_cell(
    bottom_layer=layers["fab_3_nb_wiring"],
    top_layer=layers["fab_5_microstrip_dielectric"],
    cell_name="Alignment Caliper 4",
)

caliper_5 = alignment_caliper.make_cell(
    bottom_layer=layers["fab_3_nb_wiring"],
    top_layer=layers["fab_6_nb_ground"],
    cell_name="Alignment Caliper 5",
)

calipers_group_cell = gdspy.Cell("Caliper Group")

calipers_group_cell.add(
    [
        gdspy.CellReference(caliper_1, origin=(-400, 0.0), rotation=0.0),
        gdspy.CellReference(caliper_2, origin=(-200, 0.0), rotation=0.0),
        gdspy.CellReference(caliper_3, origin=(0.0, 0.0), rotation=0.0),
        gdspy.CellReference(caliper_4, origin=(200, 0.0), rotation=0.0),
        gdspy.CellReference(caliper_5, origin=(400, 0.0), rotation=0.0),
    ]
)

main_cell.add(
    [
        gdspy.CellReference(calipers_group_cell, origin=(-57750, -1000)),
        gdspy.CellReference(calipers_group_cell, origin=(57750, -1000)),
        gdspy.CellReference(calipers_group_cell, origin=(-57750, -2000), rotation=90),
        gdspy.CellReference(calipers_group_cell, origin=(57750, -2000), rotation=90),
    ]
)

########################################################################################################################

main_cell.add(gdspy.CellReference(all_devices_cell, origin=(0.0, 0.0), rotation=30))

# main_cell.flatten()
library.add(main_cell)
library.write_gds("mask_files\\" + chip_unique_id + "_" + fab_facility + "_mask.gds")

# pogo pin diameter 2.4mm
