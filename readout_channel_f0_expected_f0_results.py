import numpy as np
import matplotlib.pyplot as plt
import util.filter_bank_builder_tools as tools



# file_path = r"slim_r100_v1_500MHz_duo_f0_array.txt"
#
# full_f0_array = np.loadtxt(file_path, skiprows=0, dtype="float", delimiter=",")
# f0_array_a = np.sort(full_f0_array[0])
# f0_array_b = np.sort(full_f0_array[1])
#
# full_f0_array = full_f0_array.flatten()
# full_f0_array = np.sort(full_f0_array)
# full_index = np.linspace(0, full_f0_array.size, full_f0_array.size)
#
# f0_a_index = np.where(np.in1d(full_f0_array, f0_array_a))[0]
# f0_b_index = np.where(np.in1d(full_f0_array, f0_array_b))[0]
#
# plt.figure(figsize=(8, 6))
# plt.plot(full_index * 1e-9, full_f0_array, linestyle=":", marker="o")
# plt.plot(f0_a_index * 1e-9, f0_array_a, linestyle="none", marker="o")
# plt.plot(f0_b_index * 1e-9, f0_array_b, linestyle="none", marker="o")
# plt.xlabel("F0 index")
# plt.ylabel("F0 (GHz)")
# plt.show()
#
#
# frequency_spacing = np.diff(full_f0_array)
#
# plt.figure(figsize=(8, 6))
# plt.plot(frequency_spacing * 1e-6, linestyle="none", marker="o")
# plt.xlabel("F0 index")
# plt.ylabel("F0 spacing (MHz)")
# plt.show()

########################################################################################################################

# Add mm_wave f0's to data file:

resolution = 100
frequency_min = 120
frequency_max = 180
oversampling = 1.6

number_of_channels = round(
    oversampling * resolution * np.log(frequency_max / frequency_min)
)
print(f"Number of channels = {number_of_channels}")
target_f0_array = tools.create_target_f0_array(
    frequency_min, frequency_max, number_of_channels
)

file_path = r"Spectrometer Group Cell Spectrometer #A_f0_scheduling.txt"
data = np.loadtxt(file_path, skiprows=1, dtype="str", delimiter=",")
detector_tags = data[:, 0]
microwave_f0 = data[:, 1].astype(float)
mm_f0 = data[:, 2].astype(float)

plt.figure(figsize=(8, 6))
plt.plot(mm_f0, linestyle="none", marker="o", markersize=1)
plt.show()
