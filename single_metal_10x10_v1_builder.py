import gdspy
import numpy as np
import matplotlib.pyplot as plt
from math import radians
from math import degrees
import submodules.dice_lines
import util.filter_bank_builder_tools as tools
import util.spectrometer_builders
import util.general_gds_tools as general_gds_tools
from models.alignment_cross import AlignmentCross
from models.edge_bond_pad import EdgeBondPad
from models.filter_bank import FilterBank
from models.dual_pol_omt import DualPolOMT
from models.alignment_caliper import AlignmentCaliper
from models.lekid import Lekid


########################################################################################################################

resolution = 200
filter_spacing = 0.375
number_of_pixels = 12
show_plots = False
chip_unique_id = f"slim_10x10_all_Hybrid_resonator"
fab_facility = "ANL"
library = gdspy.GdsLibrary("Chip Library", unit=1e-06, precision=1e-09)
# Create Main cell to add module cells to
main_cell = gdspy.Cell(f"{chip_unique_id} Main")

# Define layer dictionary:
layers = {
    "e_beam_boundary": 0,
    "inductors": 1,
    "idc": 2,
    "readout_centre": 3,
    "dielectric": 4,
    "ground": 5,
    "wafer": 6,
    "dice": 7,
    "antennas": 8,
    "mm-feedline": 9,
    "back_etch": 10,
    "reduced_e-beam_boundary": 11,
    "nitride_step_down": 12,
    "oxide_step_down": 13,
    "capacitor_plates": 14,
    "aluminium": 15,
    "focal plane": 16,
    "pixel": 17,
    "dowel": 18,
    "pogo_pins": 19,
    "tabs": 20,
    "alignment": 21,
    "titanium_aluminium": 22,
    "fab_1_nitride_membrane_etch": 101,
    "fab_2_oxide_membrane_etch": 102,
    "fab_3_nb_wiring": 103,
    "fab_4_al_meanders": 104,
    "fab_5_microstrip_dielectric": 105,
    "fab_6_nb_ground": 106,
    "fab_7_drie_etch": 107,
}

# define Antenna feedline and readout path widths:
feedline_width = 2.5
readout_width = 16.0
readout_gap = 10.0
readout_bridge_separation = 500.0
optimal_bridge_thickness = 18.864
dice_width = 300

########################################################################################################################

# Add 10x10mm boundary for back etch

edge_line_1 = gdspy.Rectangle((-5300, 5000), (5300, 5300), layer=layers["back_etch"])
edge_line_2 = gdspy.Rectangle((5000, 5300), (5300, -5300), layer=layers["back_etch"])
edge_line_3 = gdspy.Rectangle((-5300, -5000), (5300, -5300), layer=layers["back_etch"])

edge_line_4a = gdspy.Rectangle(
    (-5300, 5000), (-5000, 3062.5), layer=layers["back_etch"]
)
edge_line_4b = gdspy.Rectangle((-5300, 2312.5), (-5000, 375), layer=layers["back_etch"])
edge_line_4c = gdspy.Rectangle(
    (-5300, -375), (-5000, -2312.5), layer=layers["back_etch"]
)
edge_line_4d = gdspy.Rectangle(
    (-5300, -3062.5), (-5000, -5000), layer=layers["back_etch"]
)

main_cell.add(
    [
        edge_line_1,
        edge_line_2,
        edge_line_3,
        edge_line_4a,
        edge_line_4b,
        edge_line_4c,
        edge_line_4d,
    ]
)

########################################################################################################################

# Add lekids:

lekids_cell = gdspy.Cell(f"{chip_unique_id} LEKIDs")

target_f0_array = np.array([1.8, 1.9, 2.0, 2.1]) * 1e9
inductor_positions = np.array([[-2250, 2000], [-750, 2000], [750, 2000], [2250, 2000]])
full_readout_connections = []

for count, f0 in enumerate(target_f0_array):

    lekid = Lekid(
        target_f0=f0,
        step_down=True,
        signal_input_x=0,
        signal_input_y=0,
    )

    lekid_cell = lekid.make_cell(
        inductor_layer=layers["inductors"],
        idc_layer=layers["idc"],
        readout_layer=layers["readout_centre"],
        ground_layer=layers["ground"],
        dielectric_layer=layers["dielectric"],
        nitride_step_down_layer=layers["nitride_step_down"],
        oxide_step_down_layer=layers["oxide_step_down"],
        e_beam_boundary_layer=layers["e_beam_boundary"],
        readout_width=readout_width,
        readout_gap=readout_gap,
        include_step_down=True,
        cell_name=f"{chip_unique_id} LEKID {count}",
    )

    readout_positions = lekid.get_readout_connections(readout_width=readout_width)
    full_readout_connections.append(readout_positions)
    height_displacement = readout_positions[0][1]
    lekids_cell.add(
        gdspy.CellReference(
            lekid_cell,
            origin=(
                inductor_positions[count][0],
                inductor_positions[count][1] - height_displacement,
            ),
            rotation=0,
        )
    )

# Connect readouts of detectors
for count, connection in enumerate(full_readout_connections):
    if connection != full_readout_connections[-1]:
        tools.cpw_connect_parallel_lines(
            x1=connection[1][0] + inductor_positions[count][0],
            y1=inductor_positions[count][1],
            x2=full_readout_connections[count + 1][0][0]
            + inductor_positions[count + 1][0],
            y2=inductor_positions[count + 1][1],
            centre_width=readout_width,
            ground_gap=readout_gap,
            bend_radius=200,
            centre_layer=layers["readout_centre"],
            ground_layer=layers["ground"],
            target_cell=lekids_cell,
            parallel_in_x=True,
        )

main_cell.add(gdspy.CellReference(lekids_cell, origin=(0.0, 0.0), rotation=-90))

########################################################################################################################

# Section to add bond pads
device_width = 10000

bond_pad_top_origin = (0.0, device_width / 2 - 20)
bond_pad_bottom_origin = (0.0, -device_width / 2 + 20)


bond_pad = EdgeBondPad(
    pad_width=400,
    pad_gap=250,
    cpw_width=readout_width,
    cpw_gap=readout_gap,
    pad_length=600,
    transition_length=700,
    origin_x=0.0,
    origin_y=0.0,
)

bond_pad_cell = bond_pad.make_cell(
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    dielectric_layer=layers["dielectric"],
    cell_name="Test Device Bond Pad",
)

main_cell.add(
    [
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_top_origin, rotation=0.0),
        gdspy.CellReference(bond_pad_cell, origin=bond_pad_bottom_origin, rotation=180),
    ]
)

########################################################################################################################
# Create input readout line:

bond_pad_connection = (
    bond_pad.get_feedline_connection_x(),
    bond_pad.get_feedline_connection_y(),
)
bond_pad_top_connection = (
    bond_pad_top_origin[0] + bond_pad_connection[0],
    bond_pad_top_origin[1] + bond_pad_connection[1],
)

input_readout_points = [
    bond_pad_top_connection,
    (bond_pad_top_connection[0], bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] + 1000, bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] + 2000, bond_pad_top_connection[1] - 600.0),
    (bond_pad_top_connection[0] + 2000, 2365),
]

input_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=input_readout_points[:3],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=0.0,
    final_bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout input transition",
)

input_readout = general_gds_tools.build_cpw_with_bridges(
    path_points=input_readout_points[2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout input",
)

main_cell.add(input_transition)
main_cell.add(input_readout)

########################################################################################################################

# Create output readout line:

rotated_bond_pad_connection_x, rotated_bond_pad_connection_y = tools.rotate_coordinates(
    x=bond_pad_connection[0], y=bond_pad_connection[1], angle=radians(180)
)

bond_pad_bottom_connection = (
    bond_pad_bottom_origin[0] + rotated_bond_pad_connection_x,
    bond_pad_bottom_origin[1] + rotated_bond_pad_connection_y,
)

output_readout_points = [
    bond_pad_bottom_connection,
    (bond_pad_bottom_connection[0], bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] + 1000, bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] + 2000, bond_pad_bottom_connection[1] + 600.0),
    (bond_pad_bottom_connection[0] + 2000, -2275),
]

output_transition = general_gds_tools.build_bridge_transition_cpw(
    path_points=output_readout_points[:3],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    initial_bridge_thickness=0.0,
    final_bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout output transition",
)

output_readout = general_gds_tools.build_cpw_with_bridges(
    path_points=output_readout_points[2:],
    cpw_centre_width=readout_width,
    cpw_gap=readout_gap,
    bend_radius=200,
    bridge_thickness=optimal_bridge_thickness,
    bridge_separation=readout_bridge_separation,
    cpw_centre_layer=layers["readout_centre"],
    ground_layer=layers["ground"],
    cell_name="Readout output",
)

main_cell.add(output_transition)
main_cell.add(output_readout)

########################################################################################################################

# Add ID text and logo:

device_id_text = gdspy.Text(
    text=chip_unique_id,
    size=300,
    position=(3500, 4500),
    angle=-np.pi / 2,
    layer=layers["ground"],
)

main_cell.add(device_id_text)


########################################################################################################################

# main_cell.flatten()
library.add(main_cell)
library.write_gds("mask_files\\" + chip_unique_id + "_" + fab_facility + "_mask.gds")

# pogo pin diameter 2.4mm
