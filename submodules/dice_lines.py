import gdspy
import numpy as np


def make_dice_line(layers: dict) -> gdspy.Cell:
    slim_dice_lines_cell = gdspy.Cell("Slim Module Dice Lines")

    slim_dice_path_points = [
        (0.0, 0.0),
        (0.0, 7850),
        (25000, 7850 + 25000 * np.tan(np.pi / 6)),
        (25000, 42000),
        (80000, 42000),
        (80000, -42000),
        (25000, -42000),
        (25000, -7850 - 25000 * np.tan(np.pi / 6)),
        (0.0, -7850),
        (0.0, 0.0),
    ]

    dice_path = gdspy.FlexPath(
        points=slim_dice_path_points,
        width=50,
        corners="natural",
        bend_radius=50,
        layer=layers["dice"],
    )

    # Section to add dicing trace:
    slim_dice_lines_cell.add([dice_path])

    # Copy dice layer polygons into niobium ground layer as well:
    slim_dice_polygons = slim_dice_lines_cell.get_polygons()
    slim_niobium_dice_line = gdspy.PolygonSet(
        slim_dice_polygons, layer=layers["ground"]
    )
    slim_dice_lines_cell.add(slim_niobium_dice_line)

    return slim_dice_lines_cell
