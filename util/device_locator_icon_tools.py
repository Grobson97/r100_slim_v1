import gdspy


def make_full_icon(icon_layer: int, icon_reference: str) -> gdspy.Cell:
    """
    Function to make the full icon before subtraction occurs.

    :param icon_layer: Layer for the location icon
    :param icon_reference: Unique reference to give to icon cell name.
    :return:
    """

    full_icon_cell = gdspy.Cell(f"{icon_reference} Full Icon")

    device_a = gdspy.Rectangle((-255, 255), (-5, 5), layer=icon_layer)
    device_b = gdspy.Rectangle((255, 255), (5, 5), layer=icon_layer)
    device_c = gdspy.Rectangle((-255, -255), (-5, -5), layer=icon_layer)
    device_d = gdspy.Rectangle((255, -255), (5, -5), layer=icon_layer)

    device_w1 = gdspy.Rectangle((-405, 255), (-305, 155), layer=icon_layer)
    device_w2 = gdspy.Rectangle((-255, 405), (-155, 305), layer=icon_layer)
    device_x1 = gdspy.Rectangle((255, 405), (155, 305), layer=icon_layer)
    device_x2 = gdspy.Rectangle((405, 255), (305, 155), layer=icon_layer)
    device_y1 = gdspy.Rectangle((405, -255), (305, -155), layer=icon_layer)
    device_y2 = gdspy.Rectangle((255, -405), (155, -305), layer=icon_layer)
    device_z1 = gdspy.Rectangle((-255, -405), (-155, -305), layer=icon_layer)
    device_z2 = gdspy.Rectangle((-405, -255), (-305, -155), layer=icon_layer)

    device_al_res_1 = gdspy.Rectangle((-405, 5), (-305, 105), layer=icon_layer)
    device_al_res_2 = gdspy.Rectangle((-5, 405), (-105, 305), layer=icon_layer)
    device_nb_res_1 = gdspy.Rectangle((5, 405), (105, 305), layer=icon_layer)
    device_nb_res_2 = gdspy.Rectangle((405, 5), (305, 105), layer=icon_layer)
    device_ppcap_res_1 = gdspy.Rectangle((405, -5), (305, -105), layer=icon_layer)
    device_ppcap_res_2 = gdspy.Rectangle((-405, -5), (-305, -105), layer=icon_layer)

    flat = gdspy.Rectangle((-255, -500), (255, -520), layer=icon_layer)

    full_icon_cell.add(
        [
            device_a,
            device_b,
            device_c,
            device_d,
            device_w1,
            device_w2,
            device_x1,
            device_x2,
            device_y1,
            device_y2,
            device_z1,
            device_z2,
            device_al_res_1,
            device_al_res_2,
            device_nb_res_1,
            device_nb_res_2,
            device_ppcap_res_1,
            device_ppcap_res_2,
            flat,
        ]
    )

    return full_icon_cell


def make_device_icon(
    device_id: str, icon_layer: int, icon_reference: str
) -> gdspy.Cell:
    """
    Function to create the device icon that will overlap with the desired icon in the full icon.

    :param device_id: Letter corresponding to the specific device. e.g. "a", "b", "c" etc.
    :param icon_layer: Layer for the location icon
    :param icon_reference: Unique reference to give to icon cell name.
    :return:
    """

    device_icon_cell = gdspy.Cell(f"{icon_reference} Device Icon")

    # If device_id a, use default points:
    point_1 = (-235, 235)
    point_2 = (-25, 25)
    if device_id == "b":
        point_1 = (235, 235)
        point_2 = (25, 25)
    if device_id == "c":
        point_1 = (-235, -235)
        point_2 = (-25, -25)
    if device_id == "d":
        point_1 = (235, -235)
        point_2 = (25, -25)
    if device_id == "w1":
        point_1 = (-385, 235)
        point_2 = (-325, 175)
    if device_id == "w2":
        point_1 = (-235, 385)
        point_2 = (-100, 325)
    if device_id == "x1":
        point_1 = (235, 385)
        point_2 = (175, 325)
    if device_id == "x2":
        point_1 = (385, 235)
        point_2 = (325, 175)
    if device_id == "y1":
        point_1 = (385, -235)
        point_2 = (325, -175)
    if device_id == "y2":
        point_1 = (235, -385)
        point_2 = (175, -325)
    if device_id == "z1":
        point_1 = (-235, -385)
        point_2 = (-175, -325)
    if device_id == "z2":
        point_1 = (-385, -235)
        point_2 = (-325, -175)
    if device_id == "al_res_1":
        point_1 = (-385, 65)
        point_2 = (-325, 85)
    if device_id == "al_res_2":
        point_1 = (-85, 385)
        point_2 = (-25, 325)
    if device_id == "nb_res_1":
        point_1 = (25, 385)
        point_2 = (85, 325)
    if device_id == "nb_res_2":
        point_1 = (325, 85)
        point_2 = (385, 25)
    if device_id == "ppcap_res_1":
        point_1 = (325, -25)
        point_2 = (385, -85)
    if device_id == "ppcap_res_2":
        point_1 = (-325, -25)
        point_2 = (-385, -85)

    device_icon_cell.add(
        gdspy.Rectangle(point1=point_1, point2=point_2, layer=icon_layer)
    )

    return device_icon_cell


def make_locator_icon(
    device_id: str, icon_layer: int, icon_reference: str
) -> gdspy.Cell:
    full_icon_cell = make_full_icon(
        icon_layer=icon_layer, icon_reference=icon_reference
    )
    device_icon = make_device_icon(
        device_id=device_id, icon_layer=icon_layer, icon_reference=icon_reference
    )

    locator_icon = gdspy.boolean(
        full_icon_cell, device_icon, operation="not", layer=icon_layer
    )

    device_a_locator_icon_cell = gdspy.Cell(f"{icon_reference} Locator Icon")
    device_a_locator_icon_cell.add(locator_icon)

    return device_a_locator_icon_cell
